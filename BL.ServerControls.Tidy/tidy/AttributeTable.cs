/*
* @(#)AttributeTable.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// HTML attribute hash table
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <version>  1.0, 1999/05/22
	/// </version>
	/// <version>  1.0.1, 1999/05/29
	/// </version>
	/// <version>  1.1, 1999/06/18 Java Bean
	/// </version>
	/// <version>  1.2, 1999/07/10 Tidy Release 7 Jul 1999
	/// </version>
	/// <version>  1.3, 1999/07/30 Tidy Release 26 Jul 1999
	/// </version>
	/// <version>  1.4, 1999/09/04 DOM support
	/// </version>
	/// <version>  1.5, 1999/10/23 Tidy Release 27 Sep 1999
	/// </version>
	/// <version>  1.6, 1999/11/01 Tidy Release 22 Oct 1999
	/// </version>
	/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
	/// </version>
	/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
	/// </version>
	/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
	/// </version>
	/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
	/// </version>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	public class AttributeTable
	{
		private void  InitBlock()
		{
			attributeHashtable = new System.Collections.Hashtable();
		}
		public static AttributeTable DefaultAttributeTable
		{
			get
			{
				if (defaultAttributeTable == null)
				{
					defaultAttributeTable = new AttributeTable();
					for (int i = 0; i < attrs.Length; i++)
					{
						defaultAttributeTable.install(attrs[i]);
					}
					attrHref = defaultAttributeTable.lookup("href");
					attrSrc = defaultAttributeTable.lookup("src");
					attrId = defaultAttributeTable.lookup("id");
					attrName = defaultAttributeTable.lookup("name");
					attrSummary = defaultAttributeTable.lookup("summary");
					attrAlt = defaultAttributeTable.lookup("alt");
					attrLongdesc = defaultAttributeTable.lookup("longdesc");
					attrUsemap = defaultAttributeTable.lookup("usemap");
					attrIsmap = defaultAttributeTable.lookup("ismap");
					attrLanguage = defaultAttributeTable.lookup("language");
					attrType = defaultAttributeTable.lookup("type");
					attrTitle = defaultAttributeTable.lookup("title");
					attrXmlns = defaultAttributeTable.lookup("xmlns");
					attrValue = defaultAttributeTable.lookup("value");
					attrContent = defaultAttributeTable.lookup("content");
					attrDatafld = defaultAttributeTable.lookup("datafld"); ;
					attrWidth = defaultAttributeTable.lookup("width"); ;
					attrHeight = defaultAttributeTable.lookup("height"); ;
					
					attrAlt.nowrap = true;
					attrValue.nowrap = true;
					attrContent.nowrap = true;
				}
				return defaultAttributeTable;
			}
			
		}
		
		public AttributeTable()
		{
			InitBlock();
		}
		
		public virtual Attribute lookup(System.String name)
		{
			return (Attribute) attributeHashtable[name];
		}
		
		public virtual Attribute install(Attribute attr)
		{
			return (Attribute) SupportClass.PutElement(attributeHashtable, attr.name, attr);
		}
		
		/* public method for finding attribute definition by name */
		public virtual Attribute findAttribute(AttVal attval)
		{
			Attribute np;
			
			if ((System.Object) attval.attribute != null)
			{
				np = lookup(attval.attribute);
				return np;
			}
			
			return null;
		}
		
		public virtual bool isUrl(System.String attrname)
		{
			Attribute np;
			
			np = lookup(attrname);
			return (np != null && np.attrchk == AttrCheckImpl.getCheckUrl());
		}
		
		public virtual bool isScript(System.String attrname)
		{
			Attribute np;
			
			np = lookup(attrname);
			return (np != null && np.attrchk == AttrCheckImpl.getCheckScript());
		}
		
		public virtual bool isLiteralAttribute(System.String attrname)
		{
			Attribute np;
			
			np = lookup(attrname);
			return (np != null && np.literal);
		}
		
		/*
		Henry Zrepa reports that some folk are
		using embed with script attributes where
		newlines are signficant. These need to be
		declared and handled specially!
		*/
		public virtual void  declareLiteralAttrib(System.String name)
		{
			Attribute attrib = lookup(name);
			
			if (attrib == null)
				attrib = install(new Attribute(name, Dict.VERS_PROPRIETARY, null));
			
			attrib.literal = true;
		}
		
		//UPGRADE_NOTE: The initialization of  'attributeHashtable' was moved to method 'InitBlock'. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1005"'
		private System.Collections.Hashtable attributeHashtable;
		
		private static AttributeTable defaultAttributeTable = null;
		
		//UPGRADE_NOTE: The initialization of  'attrs' was moved to static method 'org.w3c.tidy.AttributeTable'. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1005"'
		private static Attribute[] attrs;
		
		public static Attribute attrHref = null;
		public static Attribute attrSrc = null;
		public static Attribute attrId = null;
		public static Attribute attrName = null;
		public static Attribute attrSummary = null;
		public static Attribute attrAlt = null;
		public static Attribute attrLongdesc = null;
		public static Attribute attrUsemap = null;
		public static Attribute attrIsmap = null;
		public static Attribute attrLanguage = null;
		public static Attribute attrType = null;
		public static Attribute attrTitle = null;
		public static Attribute attrXmlns = null;
		public static Attribute attrValue = null;
		public static Attribute attrContent = null;
		public static Attribute attrDatafld = null;
		public static Attribute attrWidth = null;
		public static Attribute attrHeight = null;
		static AttributeTable()
		{
			attrs = new Attribute[]{
				new Attribute("abbr", Dict.VERS_HTML40, null), 
				new Attribute("accept-charset", Dict.VERS_HTML40, null), 
				new Attribute("accept", Dict.VERS_ALL, null), 
				new Attribute("accesskey", Dict.VERS_HTML40, null), 
				new Attribute("action", Dict.VERS_ALL, AttrCheckImpl.getCheckUrl()), 
				new Attribute("add_date", Dict.VERS_NETSCAPE, null), 
				new Attribute("align", Dict.VERS_ALL, AttrCheckImpl.getCheckAlign()), 
				new Attribute("alink", Dict.VERS_LOOSE, null), 
				new Attribute("alt", Dict.VERS_ALL, null), 
				new Attribute("archive", Dict.VERS_HTML40, null), 
				new Attribute("axis", Dict.VERS_HTML40, null), 
				new Attribute("background", Dict.VERS_LOOSE, AttrCheckImpl.getCheckUrl()), 
				new Attribute("bgcolor", Dict.VERS_LOOSE, null), 
				new Attribute("bgproperties", Dict.VERS_PROPRIETARY, null), 
				new Attribute("border", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("bordercolor", Dict.VERS_MICROSOFT, null), 
				new Attribute("bottommargin", Dict.VERS_MICROSOFT, null), 
				new Attribute("cellpadding", Dict.VERS_FROM32, null), 
				new Attribute("cellspacing", Dict.VERS_FROM32, null), 
				new Attribute("char", Dict.VERS_HTML40, null), 
				new Attribute("charoff", Dict.VERS_HTML40, null), 
				new Attribute("charset", Dict.VERS_HTML40, null), 
				new Attribute("checked", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("cite", Dict.VERS_HTML40, AttrCheckImpl.getCheckUrl()), 
				new Attribute("class", Dict.VERS_HTML40, null), 
				new Attribute("classid", Dict.VERS_HTML40, AttrCheckImpl.getCheckUrl()), 
				new Attribute("clear", Dict.VERS_LOOSE, null), 
				new Attribute("code", Dict.VERS_LOOSE, null), 
				new Attribute("codebase", Dict.VERS_HTML40, AttrCheckImpl.getCheckUrl()), 
				new Attribute("codetype", Dict.VERS_HTML40, null), 
				new Attribute("color", Dict.VERS_LOOSE, null), 
				new Attribute("cols", Dict.VERS_IFRAMES, null), 
				new Attribute("colspan", Dict.VERS_FROM32, null), 
				new Attribute("compact", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("content", Dict.VERS_ALL, null), 
				new Attribute("coords", Dict.VERS_FROM32, null), 
				new Attribute("data", Dict.VERS_HTML40, AttrCheckImpl.getCheckUrl()), 
				new Attribute("datafld", Dict.VERS_MICROSOFT, null), 
				new Attribute("dataformatas", Dict.VERS_MICROSOFT, null), 
				new Attribute("datapagesize", Dict.VERS_MICROSOFT, null), 
				new Attribute("datasrc", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckUrl()), 
				new Attribute("datetime", Dict.VERS_HTML40, null), 
				new Attribute("declare", Dict.VERS_HTML40, AttrCheckImpl.getCheckBool()), 
				new Attribute("defer", Dict.VERS_HTML40, AttrCheckImpl.getCheckBool()), 
				new Attribute("dir", Dict.VERS_HTML40, null), 
				new Attribute("disabled", Dict.VERS_HTML40, AttrCheckImpl.getCheckBool()), 
				new Attribute("enctype", Dict.VERS_ALL, null), 
				new Attribute("face", Dict.VERS_LOOSE, null), 
				new Attribute("for", Dict.VERS_HTML40, null), 
				new Attribute("frame", Dict.VERS_HTML40, null), 
				new Attribute("frameborder", Dict.VERS_FRAMES, null), 
				new Attribute("framespacing", Dict.VERS_PROPRIETARY, null), 
				new Attribute("gridx", Dict.VERS_PROPRIETARY, null), 
				new Attribute("gridy", Dict.VERS_PROPRIETARY, null), 
				new Attribute("headers", Dict.VERS_HTML40, null), 
				new Attribute("height", Dict.VERS_ALL, null), 
				new Attribute("href", Dict.VERS_ALL, AttrCheckImpl.getCheckUrl()), 
				new Attribute("hreflang", Dict.VERS_HTML40, null), 
				new Attribute("hspace", Dict.VERS_ALL, null), 
				new Attribute("http-equiv", Dict.VERS_ALL, null), 
				new Attribute("id", Dict.VERS_HTML40, AttrCheckImpl.getCheckId()), 
				new Attribute("ismap", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("label", Dict.VERS_HTML40, null), 
				new Attribute("lang", Dict.VERS_HTML40, null), 
				new Attribute("language", Dict.VERS_LOOSE, null), 
				new Attribute("last_modified", Dict.VERS_NETSCAPE, null), 
				new Attribute("last_visit", Dict.VERS_NETSCAPE, null), 
				new Attribute("leftmargin", Dict.VERS_MICROSOFT, null), 
				new Attribute("link", Dict.VERS_LOOSE, null), 
				new Attribute("longdesc", Dict.VERS_HTML40, AttrCheckImpl.getCheckUrl()), 
				new Attribute("lowsrc", Dict.VERS_PROPRIETARY, AttrCheckImpl.getCheckUrl()), 
				new Attribute("marginheight", Dict.VERS_IFRAMES, null), 
				new Attribute("marginwidth", Dict.VERS_IFRAMES, null), 
				new Attribute("maxlength", Dict.VERS_ALL, null), 
				new Attribute("media", Dict.VERS_HTML40, null), 
				new Attribute("method", Dict.VERS_ALL, null), 
				new Attribute("multiple", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("name", Dict.VERS_ALL, AttrCheckImpl.getCheckName()), 
				new Attribute("nohref", Dict.VERS_FROM32, AttrCheckImpl.getCheckBool()), 
				new Attribute("noresize", Dict.VERS_FRAMES, AttrCheckImpl.getCheckBool()), 
				new Attribute("noshade", Dict.VERS_LOOSE, AttrCheckImpl.getCheckBool()), 
				new Attribute("nowrap", Dict.VERS_LOOSE, AttrCheckImpl.getCheckBool()), 
				new Attribute("object", Dict.VERS_HTML40_LOOSE, null), 
				new Attribute("onblur", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onchange", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onclick", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("ondblclick", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onkeydown", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onkeypress", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onkeyup", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onload", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onmousedown", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onmousemove", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onmouseout", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onmouseover", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onmouseup", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onsubmit", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onreset", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onselect", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onunload", Dict.VERS_HTML40, AttrCheckImpl.getCheckScript()), 
				new Attribute("onafterupdate", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("onbeforeupdate", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("onerrorupdate", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("onrowenter", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("onrowexit", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("onbeforeunload", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("ondatasetchanged", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("ondataavailable", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("ondatasetcomplete", Dict.VERS_MICROSOFT, AttrCheckImpl.getCheckScript()), 
				new Attribute("profile", Dict.VERS_HTML40, AttrCheckImpl.getCheckUrl()), 
				new Attribute("prompt", Dict.VERS_LOOSE, null), 
				new Attribute("readonly", Dict.VERS_HTML40, AttrCheckImpl.getCheckBool()), 
				new Attribute("rel", Dict.VERS_ALL, null), 
				new Attribute("rev", Dict.VERS_ALL, null), 
				new Attribute("rightmargin", Dict.VERS_MICROSOFT, null), 
				new Attribute("rows", Dict.VERS_ALL, null), 
				new Attribute("rowspan", Dict.VERS_ALL, null), 
				new Attribute("rules", Dict.VERS_HTML40, null), 
				new Attribute("scheme", Dict.VERS_HTML40, null), 
				new Attribute("scope", Dict.VERS_HTML40, null), 
				new Attribute("scrolling", Dict.VERS_IFRAMES, null), 
				new Attribute("selected", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("shape", Dict.VERS_FROM32, null), 
				new Attribute("showgrid", Dict.VERS_PROPRIETARY, AttrCheckImpl.getCheckBool()), 
				new Attribute("showgridx", Dict.VERS_PROPRIETARY, AttrCheckImpl.getCheckBool()), 
				new Attribute("showgridy", Dict.VERS_PROPRIETARY, AttrCheckImpl.getCheckBool()), 
				new Attribute("size", Dict.VERS_LOOSE, null), 
				new Attribute("span", Dict.VERS_HTML40, null), 
				new Attribute("src", (ushort) (Dict.VERS_ALL | Dict.VERS_FRAMES), AttrCheckImpl.getCheckUrl()), 
				new Attribute("standby", Dict.VERS_HTML40, null), 
				new Attribute("start", Dict.VERS_ALL, null), 
				new Attribute("style", Dict.VERS_HTML40, null), 
				new Attribute("summary", Dict.VERS_HTML40, null), 
				new Attribute("tabindex", Dict.VERS_HTML40, null), 
				new Attribute("target", Dict.VERS_HTML40, null), 
				new Attribute("text", Dict.VERS_LOOSE, null), 
				new Attribute("title", Dict.VERS_HTML40, null), 
				new Attribute("topmargin", Dict.VERS_MICROSOFT, null), 
				new Attribute("type", Dict.VERS_FROM32, null), 
				new Attribute("usemap", Dict.VERS_ALL, AttrCheckImpl.getCheckBool()), 
				new Attribute("valign", Dict.VERS_FROM32, AttrCheckImpl.getCheckValign()), 
				new Attribute("value", Dict.VERS_ALL, null), 
				new Attribute("valuetype", Dict.VERS_HTML40, null), 
				new Attribute("version", Dict.VERS_ALL, null), 
				new Attribute("vlink", Dict.VERS_LOOSE, null), 
				new Attribute("vspace", Dict.VERS_LOOSE, null), 
				new Attribute("width", Dict.VERS_ALL, null), 
				new Attribute("wrap", Dict.VERS_NETSCAPE, null), 
				new Attribute("xml:lang", Dict.VERS_XML, null), 
				new Attribute("xmlns", Dict.VERS_ALL, null)
			};
		}
	}
}