/*
* @(#)DOMProcessingInstructionImpl.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// DOMProcessingInstructionImpl
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
	/// </version>
	/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
	/// </version>
	/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
	/// </version>
	/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
	/// </version>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	//UPGRADE_TODO: Interface 'org.w3c.dom.ProcessingInstruction' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
	public class DOMProcessingInstructionImpl:DOMNodeImpl, org.w3c.dom.ProcessingInstruction
	{
		/// <seealso cref="org.w3c.dom.Node#getNodeType">
		/// </seealso>
		override public short NodeType
		{
			get
			{
				//UPGRADE_TODO: Field 'org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (short)org.w3c.dom.Node_Fields.PROCESSING_INSTRUCTION_NODE;
			}
			
		}
		/// <seealso cref="org.w3c.dom.ProcessingInstruction#getTarget">
		/// </seealso>
		virtual public System.String Target
		{
			get
			{
				// TODO
				return null;
			}
			
		}
		//UPGRADE_NOTE: Respective javadoc comments were merged.  It should be changed in order to comply with .NET documentation conventions.
		/// <seealso cref="org.w3c.dom.ProcessingInstruction#getData">
		/// </seealso>
		/// <seealso cref="org.w3c.dom.ProcessingInstruction#setData">
		/// </seealso>
		virtual public System.String Data
		{
			get
			{
				//UPGRADE_TODO: Method 'org.w3c.dom.Node.getNodeValue' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				//return getNodeValue();
				return this.NodeValue;
			}
			
			set
			{
				// NOT SUPPORTED
				//UPGRADE_TODO: Field 'org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				throw new DOMExceptionImpl(org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR, "Not supported");
			}
			
		}
		
		protected internal DOMProcessingInstructionImpl(Node adaptee):base(adaptee)
		{
		}
		
		
		/* --------------------- DOM ---------------------------- */
	}
}