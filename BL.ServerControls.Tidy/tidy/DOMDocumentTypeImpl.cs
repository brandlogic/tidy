/*
* @(#)DOMDocumentTypeImpl.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// DOMDocumentTypeImpl
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
	/// </version>
	/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
	/// </version>
	/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
	/// </version>
	/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
	/// </version>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	//UPGRADE_TODO: Interface 'org.w3c.dom.DocumentType' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
	public class DOMDocumentTypeImpl:DOMNodeImpl, org.w3c.dom.DocumentType
	{
		/// <seealso cref="org.w3c.dom.Node#getNodeType">
		/// </seealso>
		override public short NodeType
		{
			get
			{
				//UPGRADE_TODO: Field 'org.w3c.dom.Node.DOCUMENT_TYPE_NODE' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (short)org.w3c.dom.Node_Fields.DOCUMENT_TYPE_NODE;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Node#getNodeName">
		/// </seealso>
		override public System.String NodeName
		{
			get
			{
				//UPGRADE_TODO: Method 'org.w3c.dom.DocumentType.getName' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				//return getName();
				return this.Name;
			}
			
		}
		/// <seealso cref="org.w3c.dom.DocumentType#getName">
		/// </seealso>
		virtual public System.String Name
		{
			get
			{
				System.String value_Renamed = null;
				if (adaptee.type == Node.DocTypeTag)
				{
					
					if (adaptee.textarray != null && adaptee.start < adaptee.end)
					{
						value_Renamed = Lexer.getString(adaptee.textarray, adaptee.start, adaptee.end - adaptee.start);
					}
				}
				return value_Renamed;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.NamedNodeMap' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		virtual public org.w3c.dom.NamedNodeMap Entities
		{
			get
			{
				// NOT SUPPORTED
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.NamedNodeMap' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		virtual public org.w3c.dom.NamedNodeMap Notations
		{
			get
			{
				// NOT SUPPORTED
				return null;
			}
			
		}
		/// <summary> DOM2 - not implemented.</summary>
		virtual public System.String PublicId
		{
			get
			{
				return null;
			}
			
		}
		/// <summary> DOM2 - not implemented.</summary>
		virtual public System.String SystemId
		{
			get
			{
				return null;
			}
			
		}
		/// <summary> DOM2 - not implemented.</summary>
		virtual public System.String InternalSubset
		{
			get
			{
				return null;
			}
			
		}
		
		protected internal DOMDocumentTypeImpl(Node adaptee):base(adaptee)
		{
		}
		
		
		/* --------------------- DOM ---------------------------- */
	}
}