//
// In order to convert some functionality to Visual C#, the Java Language Conversion Assistant
// creates "support classes" that duplicate the original functionality.  
//
// Support classes replicate the functionality of the original code, but in some cases they are 
// substantially different architecturally. Although every effort is made to preserve the 
// original architecture of the application in the converted project, the user should be aware that 
// the primary goal of these support classes is to replicate functionality, and that at times 
// the architecture of the resulting solution may differ somewhat.
//

using System;

/// <summary>
/// Contains conversion support elements such as classes, interfaces and static methods.
/// </summary>
public class SupportClass
{
	/// <summary>
	/// Adds a new key-and-value pair into the hash table
	/// </summary>
	/// <param name="collection">The collection to work with</param>
	/// <param name="key">Key used to obtain the value</param>
	/// <param name="newValue">Value asociated with the key</param>
	/// <returns>The old element associated with the key</returns>
	/// System.Collections.Specialized.NameValueCollection
	//public static System.Object PutElement(System.Collections.IDictionary collection, System.Object key, System.Object newValue)	//-TH
	public static System.Object PutElement(System.Collections.Specialized.NameValueCollection collection, System.Object key, System.Object newValue)
	{
		System.Object element = collection[key.ToString()];
		collection[key.ToString()] = newValue.ToString();
		return element;
	}

	public static System.Object PutElement(System.Collections.IDictionary collection, System.Object key, System.Object newValue)
	{
		System.Object element = collection[key.ToString()];
		collection[key.ToString()] = newValue;
		return element;
	}

	/*******************************/
	/// <summary>
	/// The class performs token processing from strings
	/// </summary>
	public class Tokenizer
	{
		//Element list identified
		private System.Collections.ArrayList elements;
		//Source string to use
		private string source;
		//The tokenizer uses the default delimiter set: the space character, the tab character, the newline character, and the carriage-return character
		private string delimiters = " \t\n\r";		

		/// <summary>
		/// Initializes a new class instance with a specified string to process
		/// </summary>
		/// <param name="source">String to tokenize</param>
		public Tokenizer(string source)
		{			
			this.elements = new System.Collections.ArrayList();
			this.elements.AddRange(source.Split(this.delimiters.ToCharArray()));
			this.RemoveEmptyStrings();
			this.source = source;
		}

		/// <summary>
		/// Initializes a new class instance with a specified string to process
		/// and the specified token delimiters to use
		/// </summary>
		/// <param name="source">String to tokenize</param>
		/// <param name="delimiters">String containing the delimiters</param>
		public Tokenizer(string source, string delimiters)
		{
			this.elements = new System.Collections.ArrayList();
			this.delimiters = delimiters;
			this.elements.AddRange(source.Split(this.delimiters.ToCharArray()));
			this.RemoveEmptyStrings();
			this.source = source;
		}

		/// <summary>
		/// Current token count for the source string
		/// </summary>
		public int Count
		{
			get
			{
				return (this.elements.Count);
			}
		}

		/// <summary>
		/// Determines if there are more tokens to return from the source string
		/// </summary>
		/// <returns>True or false, depending if there are more tokens</returns>
		public bool HasMoreTokens()
		{
			return (this.elements.Count > 0);			
		}

		/// <summary>
		/// Returns the next token from the token list
		/// </summary>
		/// <returns>The string value of the token</returns>
		public string NextToken()
		{			
			string result;
			if (source == "") throw new System.Exception();
			else
			{
				this.elements = new System.Collections.ArrayList();
				this.elements.AddRange(this.source.Split(delimiters.ToCharArray()));
				RemoveEmptyStrings();		
				result = (string) this.elements[0];
				this.elements.RemoveAt(0);				
				this.source = this.source.Remove(this.source.IndexOf(result),result.Length);
				this.source = this.source.TrimStart(this.delimiters.ToCharArray());
				return result;					
			}			
		}

		/// <summary>
		/// Returns the next token from the source string, using the provided
		/// token delimiters
		/// </summary>
		/// <param name="delimiters">String containing the delimiters to use</param>
		/// <returns>The string value of the token</returns>
		public string NextToken(string delimiters)
		{
			this.delimiters = delimiters;
			return NextToken();
		}

		/// <summary>
		/// Removes all empty strings from the token list
		/// </summary>
		private void RemoveEmptyStrings()
		{
			for (int index=0; index < this.elements.Count; index++)
				if ((string)this.elements[index]== "")
				{
					this.elements.RemoveAt(index);
					index--;
				}
		}
	}

	/*******************************/
	/// <summary>
	/// Converts an array of bytes to an array of bytes
	/// </summary>
	/// <param name="sbyteArray">The array of bytes to be converted</param>
	/// <returns>The new array of bytes</returns>
	public static byte[] ToByteArray(byte[] byteArray)
	{
//		byte[] byteArray = new byte[sbyteArray.Length];
//		for(int index=0; index < sbyteArray.Length; index++)
//			byteArray[index] = (byte) sbyteArray[index];
		return byteArray;
	}

	/// <summary>
	/// Converts an array of sbytes to an array of bytes
	/// </summary>
	/// <param name="sbyteArray">The array of sbytes to be converted</param>
	/// <returns>The new array of bytes</returns>
//	public static byte[] ToByteArray(sbyte[] sbyteArray)
//	{
//		byte[] byteArray = new byte[sbyteArray.Length];
//		for(int index=0; index < sbyteArray.Length; index++)
//			byteArray[index] = (byte) sbyteArray[index];
//		return byteArray;
//	}

	/// <summary>
	/// Converts a string to an array of bytes
	/// </summary>
	/// <param name="sourceString">The string to be converted</param>
	/// <returns>The new array of bytes</returns>
	public static byte[] ToByteArray(string sourceString)
	{
//		byte[] byteArray = new byte[sourceString.Length];
//		for (int index=0; index < sourceString.Length; index++)
//			byteArray[index] = (byte) sourceString[index];
//		return byteArray;
		System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding();
		return utf8.GetBytes(sourceString);
	}

	/// <summary>
	/// Converts a array of object-type instances to a byte-type array.
	/// </summary>
	/// <param name="tempObjectArray">Array to convert.</param>
	/// <returns>An array of byte type elements.</returns>
//	public static byte[] ToByteArray(object[] tempObjectArray)
//	{
//		byte[] byteArray = new byte[tempObjectArray.Length];
//		for (int index = 0; index < tempObjectArray.Length; index++)
//			byteArray[index] = (byte)tempObjectArray[index];
//		return byteArray;
//	}


	/*******************************/
	/// <summary>
	/// This method returns the literal value received
	/// </summary>
	/// <param name="literal">The literal to return</param>
	/// <returns>The received value</returns>
//	public static long Identity(long literal)
//	{
//		return literal;
//	}

	/// <summary>
	/// This method returns the literal value received
	/// </summary>
	/// <param name="literal">The literal to return</param>
	/// <returns>The received value</returns>
//	public static ulong Identity(ulong literal)
//	{
//		return literal;
//	}

	/// <summary>
	/// This method returns the literal value received
	/// </summary>
	/// <param name="literal">The literal to return</param>
	/// <returns>The received value</returns>
//	public static float Identity(float literal)
//	{
//		return literal;
//	}

	/// <summary>
	/// This method returns the literal value received
	/// </summary>
	/// <param name="literal">The literal to return</param>
	/// <returns>The received value</returns>
	public static double Identity(double literal)
	{
		return literal;
	}

	/*******************************/
	/// <summary>
	/// Adds an element to the top end of a Stack instance.
	/// </summary>
	/// <param name="stack">The Stack instance</param>
	/// <param name="element">The element to add</param>
	/// <returns>The element added</returns>  
	public static System.Object StackPush(System.Collections.Stack stack, System.Object element)
	{
		stack.Push(element);
		return element;
	}

	/*******************************/
	/// <summary>
	/// Receives a byte array and returns it transformed in an sbyte array
	/// </summary>
	/// <param name="byteArray">Byte array to process</param>
	/// <returns>The transformed array</returns>
//	public static sbyte[] ToSByteArray(byte[] byteArray)
//	{
//		sbyte[] sbyteArray = new sbyte[byteArray.Length];
//		for(int index=0; index < byteArray.Length; index++)
//			sbyteArray[index] = (sbyte) byteArray[index];
//		return sbyteArray;
//	}

	/*******************************/
	/// <summary>
	/// Receives a byte array and returns it transformed in a char array
	/// </summary>
	/// <param name="byteArray">Byte array to process</param>
	/// <returns>The transformed array</returns>
//	public static char[] ToCharArray(byte[] byteArray)
//	{
//		char[] charArray = new char[byteArray.Length];
//		for(int index=0; index < byteArray.Length; index++)
//			charArray[index] = (char) charArray[index];
//		return charArray;
//	}
}
