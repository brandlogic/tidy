/*
* @(#)Report.java   1.11 2000/08/16
*
*/
/// <summary> 
/// Error/informational message reporter.
/// 
/// You should only need to edit the file TidyMessages.properties
/// to localize HTML tidy.
/// 
/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
/// HTML Tidy Release 4 Aug 2000</a>
/// 
/// </summary>
/// <author>   Dave Raggett <dsr@w3.org>
/// </author>
/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
/// </author>
/// <version>  1.0, 1999/05/22
/// </version>
/// <version>  1.0.1, 1999/05/29
/// </version>
/// <version>  1.1, 1999/06/18 Java Bean
/// </version>
/// <version>  1.2, 1999/07/10 Tidy Release 7 Jul 1999
/// </version>
/// <version>  1.3, 1999/07/30 Tidy Release 26 Jul 1999
/// </version>
/// <version>  1.4, 1999/09/04 DOM support
/// </version>
/// <version>  1.5, 1999/10/23 Tidy Release 27 Sep 1999
/// </version>
/// <version>  1.6, 1999/11/01 Tidy Release 22 Oct 1999
/// </version>
/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
/// </version>
/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
/// </version>
/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
/// </version>
/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
/// </version>
/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
/// </version>
using System;
namespace org.w3c.tidy
{
	
	public class Report
	{
		
		/* used to point to Web Accessibility Guidelines */
		public const System.String ACCESS_URL = "http://www.w3.org/WAI/GL";
		
		public const System.String RELEASE_DATE = "4th August 2000";
		
		public static System.String currentFile; /* sasdjb 01May00 for GNU Emacs error parsing */
		
		/* error codes for entities */
		
		public const ushort MISSING_SEMICOLON = 1;
		public const ushort UNKNOWN_ENTITY = 2;
		public const ushort UNESCAPED_AMPERSAND = 3;
		
		/* error codes for element messages */
		
		public const ushort MISSING_ENDTAG_FOR = 1;
		public const ushort MISSING_ENDTAG_BEFORE = 2;
		public const ushort DISCARDING_UNEXPECTED = 3;
		public const ushort NESTED_EMPHASIS = 4;
		public const ushort NON_MATCHING_ENDTAG = 5;
		public const ushort TAG_NOT_ALLOWED_IN = 6;
		public const ushort MISSING_STARTTAG = 7;
		public const ushort UNEXPECTED_ENDTAG = 8;
		public const ushort USING_BR_INPLACE_OF = 9;
		public const ushort INSERTING_TAG = 10;
		public const ushort SUSPECTED_MISSING_QUOTE = 11;
		public const ushort MISSING_TITLE_ELEMENT = 12;
		public const ushort DUPLICATE_FRAMESET = 13;
		public const ushort CANT_BE_NESTED = 14;
		public const ushort OBSOLETE_ELEMENT = 15;
		public const ushort PROPRIETARY_ELEMENT = 16;
		public const ushort UNKNOWN_ELEMENT = 17;
		public const ushort TRIM_EMPTY_ELEMENT = 18;
		public const ushort COERCE_TO_ENDTAG = 19;
		public const ushort ILLEGAL_NESTING = 20;
		public const ushort NOFRAMES_CONTENT = 21;
		public const ushort CONTENT_AFTER_BODY = 22;
		public const ushort INCONSISTENT_VERSION = 23;
		public const ushort MALFORMED_COMMENT = 24;
		public const ushort BAD_COMMENT_CHARS = 25;
		public const ushort BAD_XML_COMMENT = 26;
		public const ushort BAD_CDATA_CONTENT = 27;
		public const ushort INCONSISTENT_NAMESPACE = 28;
		public const ushort DOCTYPE_AFTER_TAGS = 29;
		public const ushort MALFORMED_DOCTYPE = 30;
		public const ushort UNEXPECTED_END_OF_FILE = 31;
		public const ushort DTYPE_NOT_UPPER_CASE = 32;
		public const ushort TOO_MANY_ELEMENTS = 33;
		
		/* error codes used for attribute messages */
		
		public const ushort UNKNOWN_ATTRIBUTE = 1;
		public const ushort MISSING_ATTRIBUTE = 2;
		public const ushort MISSING_ATTR_VALUE = 3;
		public const ushort BAD_ATTRIBUTE_VALUE = 4;
		public const ushort UNEXPECTED_GT = 5;
		public const ushort PROPRIETARY_ATTR_VALUE = 6;
		public const ushort REPEATED_ATTRIBUTE = 7;
		public const ushort MISSING_IMAGEMAP = 8;
		public const ushort XML_ATTRIBUTE_VALUE = 9;
		public const ushort UNEXPECTED_QUOTEMARK = 10;
		public const ushort ID_NAME_MISMATCH = 11;
		
		/* accessibility flaws */
		
		public const ushort MISSING_IMAGE_ALT = 1;
		public const ushort MISSING_LINK_ALT = 2;
		public const ushort MISSING_SUMMARY = 4;
		public const ushort MISSING_IMAGE_MAP = 8;
		public const ushort USING_FRAMES = 16;
		public const ushort USING_NOFRAMES = 32;
		
		/* presentation flaws */
		
		public const ushort USING_SPACER = 1;
		public const ushort USING_LAYER = 2;
		public const ushort USING_NOBR = 4;
		public const ushort USING_FONT = 8;
		public const ushort USING_BODY = 16;
		
		/* character encoding errors */
		public const ushort WINDOWS_CHARS = 1;
		public const ushort NON_ASCII = 2;
		public const ushort FOUND_UTF16 = 4;
		
		private static ushort optionerrors;
		
		private static System.Resources.ResourceManager res = null;
		
		public static void  tidyPrint(System.IO.TextWriter p, System.String msg)
		{
			p.Write(msg);
		}
		
		public static void  tidyPrintln(System.IO.TextWriter p, System.String msg)
		{
			p.WriteLine(msg);
		}
		
		public static void  tidyPrintln(System.IO.TextWriter p)
		{
			p.WriteLine();
		}
		
		public static void  showVersion(System.IO.TextWriter p)
		{
			tidyPrintln(p, "Java HTML Tidy release date: " + RELEASE_DATE);
			tidyPrintln(p, "See http://www.w3.org/People/Raggett for details");
		}
		
		public static void  tag(Lexer lexer, Node tag)
		{
			if (tag != null)
			{
				if (tag.type == Node.StartTag)
					tidyPrint(lexer.errout, "<" + tag.element + ">");
				else if (tag.type == Node.EndTag)
					tidyPrint(lexer.errout, "</" + tag.element + ">");
				else if (tag.type == Node.DocTypeTag)
					tidyPrint(lexer.errout, "<!DOCTYPE>");
				else if (tag.type == Node.TextNode)
					tidyPrint(lexer.errout, "plain text");
				else
					tidyPrint(lexer.errout, tag.element);
			}
		}
		
		/* lexer is not defined when this is called */
		public static void  unknownOption(System.String option)
		{
			optionerrors++;
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//System.Console.Error.WriteLine(String.Format(res.GetString("unknown_option"), new System.Object[]{option}));
				System.Console.Error.WriteLine("unknown option");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				System.Console.Error.WriteLine(e.ToString());
			}
		}
		
		/* lexer is not defined when this is called */
		public static void  badArgument(System.String option)
		{
			optionerrors++;
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//System.Console.Error.WriteLine(String.Format(res.GetString("bad_argument"), new System.Object[]{option}));
				System.Console.Error.WriteLine("bad argument");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				System.Console.Error.WriteLine(e.ToString());
			}
		}
		
		
		public static void  position(Lexer lexer)
		{
			try
			{
				/* Change formatting to be parsable by GNU Emacs */
				if (lexer.configuration.Emacs)
				{
					//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					tidyPrint(lexer.errout, String.Format(res.GetString("emacs_format"), new System.Object[]{currentFile, (System.Object) (lexer.lines), (System.Object) (lexer.columns)}));
					tidyPrint(lexer.errout, "emacs_format");
					tidyPrint(lexer.errout, " ");
				}
				/* traditional format */
				else
				{
					//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, String.Format(res.GetString("line_column"), new System.Object[]{(System.Object) (lexer.lines), (System.Object) (lexer.columns)}));
					tidyPrint(lexer.errout, "line column");
				}
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				lexer.errout.WriteLine(e.ToString());
			}
		}
		
		public static void  encodingError(Lexer lexer, ushort code, int c)
		{
			lexer.warnings++;
			
			if (lexer.configuration.ShowWarnings)
			{
				position(lexer);
				
				if (code == WINDOWS_CHARS)
				{
					lexer.badChars |= WINDOWS_CHARS;
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("illegal_char"), new System.Object[]{(System.Object) (c)}));
						tidyPrint(lexer.errout, "illegal_char");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				tidyPrintln(lexer.errout);
			}
		}
		
		public static void  entityError(Lexer lexer, ushort code, System.String entity, int c)
		{
			lexer.warnings++;
			
			if (lexer.configuration.ShowWarnings)
			{
				position(lexer);
				
				
				if (code == MISSING_SEMICOLON)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("missing_semicolon"), new System.Object[]{entity}));
						tidyPrint(lexer.errout, "missing semicolon");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == UNKNOWN_ENTITY)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("unknown_entity"), new System.Object[]{entity}));
						tidyPrint(lexer.errout, "unknown_entity");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == UNESCAPED_AMPERSAND)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("unescaped_ampersand"));
						tidyPrint(lexer.errout, "unescaped_ampersand");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				tidyPrintln(lexer.errout);
			}
		}
		
		public static void  attrError(Lexer lexer, Node node, System.String attr, ushort code)
		{
			lexer.warnings++;
			
			/* keep quiet after 6 errors */
			if (lexer.errors > 6)
				return ;
			
			if (lexer.configuration.ShowWarnings)
			{
				/* on end of file adjust reported position to end of input */
				if (code == UNEXPECTED_END_OF_FILE)
				{
					lexer.lines = lexer.in_Renamed.curline;
					lexer.columns = lexer.in_Renamed.curcol;
				}
				
				position(lexer);
				
				if (code == UNKNOWN_ATTRIBUTE)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("unknown_attribute"), new System.Object[]{attr}));
						tidyPrint(lexer.errout, "unknown attribute");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MISSING_ATTRIBUTE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("missing_attribute"), new System.Object[]{attr}));
						tidyPrint(lexer.errout, "missing attribute");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MISSING_ATTR_VALUE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("missing_attr_value"), new System.Object[]{attr}));
						tidyPrint(lexer.errout, "missing attr value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MISSING_IMAGEMAP)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("missing_imagemap"));
						tidyPrint(lexer.errout, "missing image map");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					ushort lexerDotBadAccess = (ushort)lexer.badAccess;
					lexerDotBadAccess |= MISSING_IMAGE_MAP;
					lexer.badAccess = (short)lexerDotBadAccess;
				}
				else if (code == BAD_ATTRIBUTE_VALUE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("bad_attribute_value"), new System.Object[]{attr}));
						tidyPrint(lexer.errout, "bad attribute value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == XML_ATTRIBUTE_VALUE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("xml_attribute_value"), new System.Object[]{attr}));
						tidyPrint(lexer.errout, "xml attribute value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == UNEXPECTED_GT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("error"));
						tidyPrint(lexer.errout, "error");
						tag(lexer, node);
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("unexpected_gt"));
						tidyPrint(lexer.errout, "unexpected gt");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					lexer.errors++; ;
				}
				else if (code == UNEXPECTED_QUOTEMARK)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("unexpected_quotemark"));
						tidyPrint(lexer.errout, "unexpected quotemark");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == REPEATED_ATTRIBUTE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("repeated_attribute"));
						tidyPrint(lexer.errout, "repeated attribute");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == PROPRIETARY_ATTR_VALUE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("proprietary_attr_value"), new System.Object[]{attr}));
						tidyPrint(lexer.errout, "proprietary attr value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == UNEXPECTED_END_OF_FILE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("unexpected_end_of_file"));
						tidyPrint(lexer.errout, "unexpected end of file");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == ID_NAME_MISMATCH)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("id_name_mismatch"));
						tidyPrint(lexer.errout, "id name mismatch");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				tidyPrintln(lexer.errout);
			}
			else if (code == UNEXPECTED_GT)
			{
				position(lexer);
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("error"));
					tidyPrint(lexer.errout, "error");
					tag(lexer, node);
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("unexpected_gt"));
					tidyPrint(lexer.errout, "unexpected gt");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
				tidyPrintln(lexer.errout);
				lexer.errors++; ;
			}
		}
		
		public static void  warning(Lexer lexer, Node element, Node node, ushort code)
		{
			
			TagTable tt = lexer.configuration.tt;
			
			lexer.warnings++;
			
			/* keep quiet after 6 errors */
			if (lexer.errors > 6)
				return ;
			
			if (lexer.configuration.ShowWarnings)
			{
				/* on end of file adjust reported position to end of input */
				if (code == UNEXPECTED_END_OF_FILE)
				{
					lexer.lines = lexer.in_Renamed.curline;
					lexer.columns = lexer.in_Renamed.curcol;
				}
				
				position(lexer);
				
				if (code == MISSING_ENDTAG_FOR)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("missing_endtag_for"), new System.Object[]{element.element}));
						tidyPrint(lexer.errout, "missing endtag for");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MISSING_ENDTAG_BEFORE)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("missing_endtag_before"), new System.Object[]{element.element}));
						tidyPrint(lexer.errout, "missing endtag before");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
				}
				else if (code == DISCARDING_UNEXPECTED)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("discarding_unexpected"));
						tidyPrint(lexer.errout, "discarding unexpected");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
				}
				else if (code == NESTED_EMPHASIS)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("nested_emphasis"));
						tidyPrint(lexer.errout, "nested emphasis");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
				}
				else if (code == COERCE_TO_ENDTAG)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("coerce_to_endtag"), new System.Object[]{element.element}));
						tidyPrint(lexer.errout, "coerce to endtag");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == NON_MATCHING_ENDTAG)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("non_matching_endtag_1"));
						tidyPrint(lexer.errout, "non matching endtag 1");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("non_matching_endtag_2"), new System.Object[]{element.element}));
						tidyPrint(lexer.errout, "non matching endtag 2");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == TAG_NOT_ALLOWED_IN)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("tag_not_allowed_in"), new System.Object[]{element.element}));
						tidyPrint(lexer.errout, "tag not allowed in");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == DOCTYPE_AFTER_TAGS)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("doctype_after_tags"));
						tidyPrint(lexer.errout, "doctype after tags");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MISSING_STARTTAG)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("missing_starttag"), new System.Object[]{node.element}));
						tidyPrint(lexer.errout, "missing starttag");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == UNEXPECTED_ENDTAG)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("unexpected_endtag"), new System.Object[]{node.element}));
						tidyPrint(lexer.errout, "unexpected endtag");
						if (element != null)
						{
							//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
							//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
							//tidyPrint(lexer.errout, String.Format(res.GetString("unexpected_endtag_suffix"), new System.Object[]{element.element}));
							tidyPrint(lexer.errout, "unexpected endtag suffix");
						}
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == TOO_MANY_ELEMENTS)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("too_many_elements"), new System.Object[]{node.element}));
						tidyPrint(lexer.errout, "too many elements");
						if (element != null)
						{
							//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
							//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
							//tidyPrint(lexer.errout, String.Format(res.GetString("too_many_elements_suffix"), new System.Object[]{element.element}));
							tidyPrint(lexer.errout, "too many elements suffix");
						}
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == USING_BR_INPLACE_OF)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("using_br_inplace_of"));
						tidyPrint(lexer.errout, "using br inplace of");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
				}
				else if (code == INSERTING_TAG)
				{
					try
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("inserting_tag"), new System.Object[]{node.element}));
						tidyPrint(lexer.errout, "inserting tag");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == CANT_BE_NESTED)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("cant_be_nested"));
						tidyPrint(lexer.errout, "cant be nested");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == PROPRIETARY_ELEMENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("proprietary_element"));
						tidyPrint(lexer.errout, "proprietarty element");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					
					if (node.tag == tt.tagLayer)
						lexer.badLayout |= USING_LAYER;
					else if (node.tag == tt.tagSpacer)
						lexer.badLayout |= USING_SPACER;
					else if (node.tag == tt.tagNobr)
						lexer.badLayout |= USING_NOBR;
				}
				else if (code == OBSOLETE_ELEMENT)
				{
					try
					{
						if (element.tag != null && (element.tag.model & Dict.CM_OBSOLETE) != 0)
						{
							//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
							//tidyPrint(lexer.errout, res.GetString("obsolete_element"));
							tidyPrint(lexer.errout, "obsolete element");
						}
						else
						{
							//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
							//tidyPrint(lexer.errout, res.GetString("replacing_element"));
							tidyPrint(lexer.errout, "replacing element");
						}
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, element);
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("by"));
						tidyPrint(lexer.errout, "by");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
				}
				else if (code == TRIM_EMPTY_ELEMENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("trim_empty_element"));
						tidyPrint(lexer.errout, "trim empty element");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, element);
				}
				else if (code == MISSING_TITLE_ELEMENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("missing_title_element"));
						tidyPrint(lexer.errout, "missing title element");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == ILLEGAL_NESTING)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, element);
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("illegal_nesting"));
						tidyPrint(lexer.errout, "illegal nesting");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == NOFRAMES_CONTENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("warning"));
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, node);
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("noframes_content"));
						tidyPrint(lexer.errout, "noframes content");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == INCONSISTENT_VERSION)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("inconsistent_version"));
						tidyPrint(lexer.errout, "inconsistent version");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MALFORMED_DOCTYPE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("malformed_doctype"));
						tidyPrint(lexer.errout, "malformed doctype");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == CONTENT_AFTER_BODY)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("content_after_body"));
						tidyPrint(lexer.errout, "content after body");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == MALFORMED_COMMENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("malformed_comment"));
						tidyPrint(lexer.errout, "malformed comment");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == BAD_COMMENT_CHARS)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("bad_comment_chars"));
						tidyPrint(lexer.errout, "bad comment chars");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == BAD_XML_COMMENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("bad_xml_comment"));
						tidyPrint(lexer.errout, "bad xml comment");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == BAD_CDATA_CONTENT)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("bad_cdata_content"));
						tidyPrint(lexer.errout, "bad cdata content");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == INCONSISTENT_NAMESPACE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("inconsistent_namespace"));
						tidyPrint(lexer.errout, "inconsistent namespace");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == DTYPE_NOT_UPPER_CASE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("dtype_not_upper_case"));
						tidyPrint(lexer.errout, "dtype not upper case");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				else if (code == UNEXPECTED_END_OF_FILE)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("unexpected_end_of_file"));
						tidyPrint(lexer.errout, "unexpected end of file");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
					tag(lexer, element);
				}
				
				tidyPrintln(lexer.errout);
			}
		}
		
		public static void  error(Lexer lexer, Node element, Node node, ushort code)
		{
			lexer.warnings++;
			
			/* keep quiet after 6 errors */
			if (lexer.errors > 6)
				return ;
			
			lexer.errors++;
			
			position(lexer);
			
			if (code == SUSPECTED_MISSING_QUOTE)
			{
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("suspected_missing_quote"));
					tidyPrint(lexer.errout, "suspected missing quote");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
			}
			else if (code == DUPLICATE_FRAMESET)
			{
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("duplicate_frameset"));
					tidyPrint(lexer.errout, "duplicate frameset");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
			}
			else if (code == UNKNOWN_ELEMENT)
			{
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("error"));
					tidyPrint(lexer.errout, "error");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
				tag(lexer, node);
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("unknown_element"));
					tidyPrint(lexer.errout, "unknown element");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
			}
			else if (code == UNEXPECTED_ENDTAG)
			{
				try
				{
					//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, String.Format(res.GetString("unexpected_endtag"), new System.Object[]{node.element}));
					tidyPrint(lexer.errout, "unexpected endtag");
					if (element != null)
					{
						//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, String.Format(res.GetString("unexpected_endtag_suffix"), new System.Object[]{element.element}));
						tidyPrint(lexer.errout, "unexpected endtag suffix");
					}
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
			}
			
			tidyPrintln(lexer.errout);
		}
		
		public static void  errorSummary(Lexer lexer)
		{
			/* adjust badAccess to that its null if frames are ok */
			if ((lexer.badAccess & (USING_FRAMES | USING_NOFRAMES)) != 0)
			{
				if (!(((lexer.badAccess & USING_FRAMES) != 0) && ((lexer.badAccess & USING_NOFRAMES) == 0)))
					lexer.badAccess &= ~ (USING_FRAMES | USING_NOFRAMES);
			}
			
			if (lexer.badChars != 0)
			{
				if ((lexer.badChars & WINDOWS_CHARS) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badchars_summary"));
						tidyPrint(lexer.errout, "badchars summary");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
			}
			
			if (lexer.badForm != 0)
			{
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, res.GetString("badform_summary"));
					tidyPrint(lexer.errout, "badform summary");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
			}
			
			if (lexer.badAccess != 0)
			{
				if ((lexer.badAccess & MISSING_SUMMARY) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badaccess_missing_summary"));
						tidyPrint(lexer.errout, "badaccess missing summary");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badAccess & MISSING_IMAGE_ALT) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badaccess_missing_image_alt"));
						tidyPrint(lexer.errout, "The alt attribute should be used to give a short description of an image; longer descriptions should be given with the longdesc attribute which takes a URL linked to the description.\n These measures are needed for people using non-graphical browsers.");

					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badAccess & MISSING_IMAGE_MAP) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badaccess_missing_image_map"));
						tidyPrint(lexer.errout, "Use client-side image maps in preference to server-side image maps as the latter are inaccessible to people using non-graphical browsers. In addition, client-side maps are easier to set up and provide immediate feedback to users.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badAccess & MISSING_LINK_ALT) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badaccess_missing_link_alt"));
						tidyPrint(lexer.errout, "For hypertext links defined using a client-side image map, you need to use the alt attribute to provide a textual description of the link for people using non-graphical browsers.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if (((lexer.badAccess & USING_FRAMES) != 0) && ((lexer.badAccess & USING_NOFRAMES) == 0))
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badaccess_frames"));
						tidyPrint(lexer.errout, "Pages designed using frames presents problems for people who are either blind or using a browser that doesn't support frames. A frames-based page should always include an alternative layout inside a NOFRAMES element.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				try
				{
					//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(lexer.errout, String.Format(res.GetString("badaccess_summary"), new System.Object[]{ACCESS_URL}));
					tidyPrint(lexer.errout, "For further advice on how to make your pages accessible see " + ACCESS_URL + ". You may also want to try \"http://www.cast.org/bobby/\" which is a free Web-based service for checking URLs for accessibility.");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					lexer.errout.WriteLine(e.ToString());
				}
			}
			
			if (lexer.badLayout != 0)
			{
				if ((lexer.badLayout & USING_LAYER) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badlayout_using_layer"));
						tidyPrint(lexer.errout, "The Cascading Style Sheets (CSS) Positioning mechanism is recommended in preference to the proprietary <LAYER> element due to limited vendor support for LAYER.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badLayout & USING_SPACER) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badlayout_using_spacer"));
						tidyPrint(lexer.errout, "You are recommended to use CSS for controlling white space (e.g. for indentation, margins and line spacing). The proprietary <SPACER> element has limited vendor support.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badLayout & USING_FONT) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badlayout_using_font"));
						tidyPrint(lexer.errout, "You are recommended to use CSS to specify the font and properties such as its size and color. This will reduce the size of HTML files and make them easier maintain compared with using <FONT> elements.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badLayout & USING_NOBR) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badlayout_using_nobr"));
						tidyPrint(lexer.errout, "You are recommended to use CSS to control line wrapping.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
				
				if ((lexer.badLayout & USING_BODY) != 0)
				{
					try
					{
						//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
						//tidyPrint(lexer.errout, res.GetString("badlayout_using_body"));
						tidyPrint(lexer.errout, "You are recommended to use CSS to specify page and link colors");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
						lexer.errout.WriteLine(e.ToString());
					}
				}
			}
		}
		
		public static void  unknownOption(System.IO.TextWriter errout, char c)
		{
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, String.Format(res.GetString("unrecognized_option"), new System.Object[]{new System.String(new char[]{c})}));
				tidyPrintln(errout, "unrecognized option - " + c.ToString());
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  unknownFile(System.IO.TextWriter errout, System.String program, System.String file)
		{
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, String.Format(res.GetString("unknown_file"), new System.Object[]{program, file}));
				tidyPrintln(errout, "can't open file " + file);
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  needsAuthorIntervention(System.IO.TextWriter errout)
		{
			try
			{
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, res.GetString("needs_author_intervention"));
				tidyPrintln(errout, "This document has errors that must be fixed before using HTML Tidy to generate a tidied up version.");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  missingBody(System.IO.TextWriter errout)
		{
			try
			{
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, res.GetString("missing_body"));
				tidyPrintln(errout, "Can't create slides - document is missing a body element.");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  reportNumberOfSlides(System.IO.TextWriter errout, int count)
		{
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, String.Format(res.GetString("slides_found"), new System.Object[]{(System.Object) (count)}));
				tidyPrintln(errout, count.ToString() + " Slides found");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  generalInfo(System.IO.TextWriter errout)
		{
			try
			{
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, res.GetString("general_info"));
				tidyPrintln(errout, "General info");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  helloMessage(System.IO.TextWriter errout, System.String date, System.String filename)
		{
			currentFile = filename; /* for use with Gnu Emacs */
			
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, String.Format(res.GetString("hello_message"), new System.Object[]{date, filename}));
				tidyPrintln(errout, "Hello message");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  reportVersion(System.IO.TextWriter errout, Lexer lexer, System.String filename, Node doctype)
		{
			int i, c;
			int state = 0;
			System.String vers = lexer.HTMLVersionName();
			MutableInteger cc = new MutableInteger();
			
			try
			{
				if (doctype != null)
				{
					//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrint(errout, String.Format(res.GetString("doctype_given"), new System.Object[]{filename}));
					tidyPrint(errout, "Doctype given is ");
					
					for (i = doctype.start; i < doctype.end; ++i)
					{
						c = (int) doctype.textarray[i];
						
						/* look for UTF-8 multibyte character */
						if (c < 0)
						{
							i += PPrint.getUTF8(doctype.textarray, i, cc);
							c = cc.value_Renamed;
						}
						
						if (c == (char) '"')
							++state;
						else if (state == 1)
							errout.Write((char) c);
					}
					
					errout.Write('"');
				}
				
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, String.Format(res.GetString("report_version"), new System.Object[]{filename, ((System.Object) vers != null?vers:"HTML proprietary")}));
				tidyPrintln(errout, "Document content looks like " + vers != null ? vers : "HTML proprietary");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		
		public static void  reportNumWarnings(System.IO.TextWriter errout, Lexer lexer)
		{
			if (lexer.warnings > 0)
			{
				try
				{
					//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrintln(errout, String.Format(res.GetString("num_warnings"), new System.Object[]{(System.Object) (lexer.warnings)}));
					tidyPrintln(errout, lexer.warnings.ToString() + " warnings/errors were found!");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					errout.WriteLine(e.ToString());
				}
			}
			else
			{
				try
				{
					//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
					//tidyPrintln(errout, res.GetString("no_warnings"));
					tidyPrintln(errout, "no warnings or errors were found\n");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					errout.WriteLine(e.ToString());
				}
			}
		}
		
		public static void  helpText(System.IO.TextWriter out_Renamed, System.String prog)
		{
			try
			{
				//UPGRADE_ISSUE: Method 'java.text.MessageFormat.format' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1000_javatextMessageFormat"'
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(out_Renamed, String.Format(res.GetString("help_text"), new System.Object[]{prog, RELEASE_DATE}));
				tidyPrintln(out_Renamed, "no help for you");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				out_Renamed.WriteLine(e.ToString());
			}
		}
		
		public static void  badTree(System.IO.TextWriter errout)
		{
			try
			{
				//UPGRADE_TODO: Method 'java.util.ResourceBundle.getString' was converted to 'System.Resources.ResourceManager.GetString()' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				//tidyPrintln(errout, res.GetString("bad_tree"));
				tidyPrintln(errout, "\nPanic - tree has lost its integrity\n");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				errout.WriteLine(e.ToString());
			}
		}
		static Report()
		{
			{
				try
				{
					//UPGRADE_TODO: Make sure that resources used in this class are valid resource files. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1078"'

					res = System.Resources.ResourceManager.CreateFileBasedResourceManager("org/w3c/tidy/TidyMessages", "", null);
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
					throw new System.ApplicationException(e.ToString());
				}
			}
		}
	}
}