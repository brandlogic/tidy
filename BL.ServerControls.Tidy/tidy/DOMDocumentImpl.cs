/*
* @(#)DOMDocumentImpl.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// DOMDocumentImpl
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <version>  1.4, 1999/09/04 DOM Support
	/// </version>
	/// <version>  1.5, 1999/10/23 Tidy Release 27 Sep 1999
	/// </version>
	/// <version>  1.6, 1999/11/01 Tidy Release 22 Oct 1999
	/// </version>
	/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
	/// </version>
	/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
	/// </version>
	/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
	/// </version>
	/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
	/// </version>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	//UPGRADE_TODO: Interface 'org.w3c.dom.Document' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
	public class DOMDocumentImpl:DOMNodeImpl, org.w3c.dom.Document
	{
		virtual public TagTable TagTable
		{
			set
			{
				this.tt = value;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Node#getNodeName">
		/// </seealso>
		override public System.String NodeName
		{
			get
			{
				return "#document";
			}
			
		}
		/// <seealso cref="org.w3c.dom.Node#getNodeType">
		/// </seealso>
		override public short NodeType
		{
			get
			{
				//UPGRADE_TODO: Field 'org.w3c.dom.Node.DOCUMENT_NODE' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (short)org.w3c.dom.Node_Fields.DOCUMENT_NODE;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Document#getDoctype">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.DocumentType' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		virtual public org.w3c.dom.DocumentType Doctype
		{
			get
			{
				Node node = adaptee.content;
				while (node != null)
				{
					if (node.type == Node.DocTypeTag)
						break;
					node = node.next;
				}
				if (node != null)
				{
					//UPGRADE_TODO: Interface 'org.w3c.dom.DocumentType' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
					return (org.w3c.dom.DocumentType) node.Adapter;
				}
				else
					return null;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Document#getImplementation">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.DOMImplementation' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		virtual public org.w3c.dom.DOMImplementation Implementation
		{
			get
			{
				// NOT SUPPORTED
				return null;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Document#getDocumentElement">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		virtual public org.w3c.dom.Element DocumentElement
		{
			get
			{
				Node node = adaptee.content;
				while (node != null)
				{
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
						break;
					node = node.next;
				}
				if (node != null)
				{
					//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
					return (org.w3c.dom.Element) node.Adapter;
				}
				else
					return null;
			}
			
		}
		
		private TagTable tt; // a DOM Document has its own TagTable.
		
		protected internal DOMDocumentImpl(Node adaptee):base(adaptee)
		{
			tt = new TagTable();
		}
		
		/* --------------------- DOM ---------------------------- */
		
		/// <seealso cref="org.w3c.dom.Document#createElement">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Element createElement(System.String tagName)
		{
			Node node = new Node(Node.StartEndTag, null, 0, 0, tagName, tt);
			if (node != null)
			{
				if (node.tag == null)
				// Fix Bug 121206
					node.tag = tt.xmlTags;
				//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (org.w3c.dom.Element) node.Adapter;
			}
			else
				return null;
		}
		
		/// <seealso cref="org.w3c.dom.Document#createDocumentFragment">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.DocumentFragment' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.DocumentFragment createDocumentFragment()
		{
			// NOT SUPPORTED
			return null;
		}
		
		/// <seealso cref="org.w3c.dom.Document#createTextNode">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Text' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Text createTextNode(System.String data)
		{
			byte[] textarray = Lexer.getBytes(data);
			Node node = new Node(Node.TextNode, textarray, 0, textarray.Length);
			if (node != null)
			{
				//UPGRADE_TODO: Interface 'org.w3c.dom.Text' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (org.w3c.dom.Text) node.Adapter;
			}
			else
				return null;
		}
		
		/// <seealso cref="org.w3c.dom.Document#createComment">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Comment' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Comment createComment(System.String data)
		{
			byte[] textarray = Lexer.getBytes(data);
			Node node = new Node(Node.CommentTag, textarray, 0, textarray.Length);
			if (node != null)
			{
				//UPGRADE_TODO: Interface 'org.w3c.dom.Comment' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (org.w3c.dom.Comment) node.Adapter;
			}
			else
				return null;
		}
		
		/// <seealso cref="org.w3c.dom.Document#createCDATASection">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.CDATASection' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.CDATASection createCDATASection(System.String data)
		{
			// NOT SUPPORTED
			return null;
		}
		
		/// <seealso cref="org.w3c.dom.Document#createProcessingInstruction">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.ProcessingInstruction' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.ProcessingInstruction createProcessingInstruction(System.String target, System.String data)
		{
			//UPGRADE_TODO: Field 'org.w3c.dom.DOMException.NOT_SUPPORTED_ERR' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
			throw new DOMExceptionImpl(org.w3c.dom.DOMException.NOT_SUPPORTED_ERR, "HTML document");
		}
		
		/// <seealso cref="org.w3c.dom.Document#createAttribute">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Attr' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Attr createAttribute(System.String name)
		{
			AttVal av = new AttVal(null, null, (int) '"', name, null);
			if (av != null)
			{
				av.dict = AttributeTable.DefaultAttributeTable.findAttribute(av);
				//UPGRADE_TODO: Interface 'org.w3c.dom.Attr' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (org.w3c.dom.Attr) av.Adapter;
			}
			else
			{
				return null;
			}
		}
		
		/// <seealso cref="org.w3c.dom.Document#createEntityReference">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.EntityReference' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.EntityReference createEntityReference(System.String name)
		{
			// NOT SUPPORTED
			return null;
		}
		
		/// <seealso cref="org.w3c.dom.Document#getElementsByTagName">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.NodeList' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.NodeList getElementsByTagName(System.String tagname)
		{
			return new DOMNodeListByTagNameImpl(this.adaptee, tagname);
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		/// <exception cref="">   org.w3c.dom.DOMException
		/// </exception>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node importNode(org.w3c.dom.Node importedNode, bool deep)
		{
			return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		/// <exception cref="">   org.w3c.dom.DOMException
		/// </exception>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Attr' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Attr createAttributeNS(System.String namespaceURI, System.String qualifiedName)
		{
			return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		/// <exception cref="">   org.w3c.dom.DOMException
		/// </exception>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Element createElementNS(System.String namespaceURI, System.String qualifiedName)
		{
			return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		//UPGRADE_TODO: Interface 'org.w3c.dom.NodeList' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.NodeList getElementsByTagNameNS(System.String namespaceURI, System.String localName)
		{
			return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Element getElementById(System.String elementId)
		{
			return null;
		}
	}
}