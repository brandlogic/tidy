/*
* @(#)DOMCDATASectionImpl.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// DOMCDATASectionImpl
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <author>   Gary L Peskin <garyp@firstech.com>
	/// </author>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	//UPGRADE_TODO: Interface 'org.w3c.dom.CDATASection' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
	public class DOMCDATASectionImpl:DOMTextImpl, org.w3c.dom.CDATASection
	{
		
		protected internal DOMCDATASectionImpl(Node adaptee):base(adaptee)
		{
		}
		
		
		/* --------------------- DOM ---------------------------- */
		
		/// <seealso cref="org.w3c.dom.Node#getNodeName">
		/// </seealso>
		public virtual System.String getNodeName()	//override	-TH
		{
			return "#cdata-section";
		}
		
		/// <seealso cref="org.w3c.dom.Node#getNodeType">
		/// </seealso>
		public virtual ushort getNodeType()	//override	-TH
		{
			//UPGRADE_TODO: Field 'org.w3c.dom.Node.CDATA_SECTION_NODE' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
			return org.w3c.dom.Node_Fields.CDATA_SECTION_NODE;
		}
	}
}