/*
* @(#)DOMAttrMapImpl.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// DOMAttrMapImpl
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <version>  1.4, 1999/09/04 DOM support
	/// </version>
	/// <version>  1.5, 1999/10/23 Tidy Release 27 Sep 1999
	/// </version>
	/// <version>  1.6, 1999/11/01 Tidy Release 22 Oct 1999
	/// </version>
	/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
	/// </version>
	/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
	/// </version>
	/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
	/// </version>
	/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
	/// </version>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	//UPGRADE_TODO: Interface 'org.w3c.dom.NamedNodeMap' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
	public class DOMAttrMapImpl : org.w3c.dom.NamedNodeMap
	{
		/// <seealso cref="org.w3c.dom.NamedNodeMap#getLength">
		/// </seealso>
		virtual public int Length
		{
			get
			{
				int len = 0;
				AttVal att = this.first;
				while (att != null)
				{
					len++;
					att = att.next;
				}
				return len;
			}
			
		}
		
		private AttVal first = null;
		
		protected internal DOMAttrMapImpl(AttVal first)
		{
			this.first = first;
		}
		
		/// <seealso cref="org.w3c.dom.NamedNodeMap#getNamedItem">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node getNamedItem(System.String name)
		{
			AttVal att = this.first;
			while (att != null)
			{
				if (att.attribute.Equals(name))
					break;
				att = att.next;
			}
			if (att != null)
				return att.Adapter;
			else
				return null;
		}
		
		/// <seealso cref="org.w3c.dom.NamedNodeMap#setNamedItem">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node setNamedItem(org.w3c.dom.Node arg)
		{
			// NOT SUPPORTED
			return null;
		}
		
		/// <seealso cref="org.w3c.dom.NamedNodeMap#removeNamedItem">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node removeNamedItem(System.String name)
		{
			// NOT SUPPORTED
			return null;
		}
		
		/// <seealso cref="org.w3c.dom.NamedNodeMap#item">
		/// </seealso>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node item(int index)
		{
			int i = 0;
			AttVal att = this.first;
			while (att != null)
			{
				if (i >= index)
					break;
				i++;
				att = att.next;
			}
			if (att != null)
				return att.Adapter;
			else
				return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node getNamedItemNS(System.String namespaceURI, System.String localName)
		{
			return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		/// <exception cref="">   org.w3c.dom.DOMException
		/// </exception>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node setNamedItemNS(org.w3c.dom.Node arg)
		{
			return null;
		}
		
		/// <summary> DOM2 - not implemented.</summary>
		/// <exception cref="">   org.w3c.dom.DOMException
		/// </exception>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public virtual org.w3c.dom.Node removeNamedItemNS(System.String namespaceURI, System.String localName)
		{
			return null;
		}
	}
}