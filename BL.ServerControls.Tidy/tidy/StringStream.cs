using System;

namespace BL.ServerControls.TidyNet.tidy
{
	/// <summary>
	/// Summary description for StringStream.
	/// </summary>
	public class StringStream
	{
		private string _stringValue;

		public bool EndOfStream = false;

		public StringStream()
		{
		}

		public StringStream(string strIn)
		{
			_stringValue = strIn;
		}

		public int Read()
		{
			char c;
			if (_stringValue.Length <= 0)
			{
				this.EndOfStream = true;
				return -1;
			}
			else
			{
				c = Char.Parse(_stringValue.Substring(0,1));
				_stringValue = _stringValue.Remove(0,1);
				return (int)c;
			}
		}

		public string Write(int i)
		{
			_stringValue += ((char) i).ToString();
			return _stringValue;
		}

		public string Write(int[] ia)
		{
			foreach (int i in ia)
				_stringValue += ((char) i).ToString();
			return _stringValue;
		}

		public string Write(byte b)
		{
			_stringValue += ((char) b).ToString();
			return _stringValue;
		}

		public string Write(byte[] ba)
		{
			foreach (byte b in ba)
				_stringValue += ((char) b).ToString();
			return _stringValue;
		}

		public string Write(sbyte sb)
		{
			_stringValue += ((char) sb).ToString();
			return _stringValue;
		}

		public string Write(sbyte[] sba)
		{
			foreach (sbyte sb in sba)
				_stringValue += ((char) sb).ToString();
			return _stringValue;
		}

		public string Write(char c)
		{
			_stringValue += c.ToString();
			return _stringValue;
		}

		public string Write(char[] ca)
		{
			foreach (char c in ca)
				_stringValue += c.ToString();
			return _stringValue;
		}

		public string Write(string s)
		{
			_stringValue += s;
			return _stringValue;
		}

		public string Write(string[] sa)
		{
			foreach (string s in sa)
				_stringValue += s;
			return _stringValue;
		}

		public string Write(Object o)
		{
			if (o != null)
				_stringValue += o.ToString();
			return _stringValue;
		}

		public string Write(Object[] oa)
		{
			foreach (string o in oa)
			{
				if (o != null)
					_stringValue += o.ToString();
			}
			return _stringValue;
		}

		virtual public string StringValue
		{
			get
			{
				return _stringValue;
			}
			
			set
			{
				_stringValue = value;
			}
		}
	}
}
