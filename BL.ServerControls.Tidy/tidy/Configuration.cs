/*
* @(#)Configuration.java   1.11 2000/08/16
*
*/
/// <summary> 
/// Read configuration file and manage configuration properties.
/// 
/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
/// See Tidy.java for the copyright notice.
/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
/// HTML Tidy Release 4 Aug 2000</a>
/// 
/// </summary>
/// <author>   Dave Raggett <dsr@w3.org>
/// </author>
/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
/// </author>
/// <version>  1.0, 1999/05/22
/// </version>
/// <version>  1.0.1, 1999/05/29
/// </version>
/// <version>  1.1, 1999/06/18 Java Bean
/// </version>
/// <version>  1.2, 1999/07/10 Tidy Release 7 Jul 1999
/// </version>
/// <version>  1.3, 1999/07/30 Tidy Release 26 Jul 1999
/// </version>
/// <version>  1.4, 1999/09/04 DOM support
/// </version>
/// <version>  1.5, 1999/10/23 Tidy Release 27 Sep 1999
/// </version>
/// <version>  1.6, 1999/11/01 Tidy Release 22 Oct 1999
/// </version>
/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
/// </version>
/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
/// </version>
/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
/// </version>
/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
/// </version>
/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
/// </version>

/*
Configuration files associate a property name with a value.
The format is that of a Java .properties file.*/
using System;
using System.Runtime.InteropServices;
namespace org.w3c.tidy
{
	//[Serializable]
	public class Configuration	// : System.Runtime.Serialization.ISerializable	-TH
	{
		private void  InitBlock()
		{
			CharEncoding = ASCII;
			docTypeMode = DOCTYPE_AUTO;
			//UPGRADE_TODO: Format of property file may need to be changed. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1089"'
			_properties = new System.Collections.Specialized.NameValueCollection();
			//_properties = new System.Data.PropertyCollection();
		}
		
		/* character encodings */
		public const int RAW = 0;
		public const int ASCII = 1;
		public const int LATIN1 = 2;
		public const int UTF8 = 3;
		public const int ISO2022 = 4;
		public const int MACROMAN = 5;
		
		/* mode controlling treatment of doctype */
		public const int DOCTYPE_OMIT = 0;
		public const int DOCTYPE_AUTO = 1;
		public const int DOCTYPE_STRICT = 2;
		public const int DOCTYPE_LOOSE = 3;
		public const int DOCTYPE_USER = 4;
		
		protected internal int spaces = 2; /* default indentation */
		protected internal int wraplen = 68; /* default wrap margin */
		//UPGRADE_NOTE: The initialization of  'CharEncoding' was moved to method 'InitBlock'. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1005"'
		protected internal int CharEncoding;
		protected internal int tabsize = 4;
		
		//UPGRADE_NOTE: The initialization of  'docTypeMode' was moved to method 'InitBlock'. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1005"'
		protected internal int docTypeMode; /* see doctype property */
		protected internal System.String altText = null; /* default text for alt attribute */
		protected internal System.String slidestyle = null; /* style sheet for slides */
		protected internal System.String docTypeStr = null; /* user specified doctype */
		protected internal System.String errfile = null; /* file name to write errors to */
		protected internal bool writeback = false; /* if true then output tidied markup */
		
		protected internal bool OnlyErrors = false; /* if true normal output is suppressed */
		protected internal bool ShowWarnings = true; /* however errors are always shown */
		protected internal bool Quiet = false; /* no 'Parsing X', guessed DTD or summary */
		protected internal bool IndentContent = false; /* indent content of appropriate tags */
		protected internal bool SmartIndent = false; /* does text/block level content effect indentation */
		protected internal bool HideEndTags = false; /* suppress optional end tags */
		protected internal bool XmlTags = false; /* treat input as XML */
		protected internal bool XmlOut = false; /* create output as XML */
		protected internal bool xHTML = false; /* output extensible HTML */
		protected internal bool XmlPi = false; /* add <?xml?> for XML docs */
		protected internal bool RawOut = false; /* avoid mapping values > 127 to entities */
		protected internal bool UpperCaseTags = false; /* output tags in upper not lower case */
		protected internal bool UpperCaseAttrs = false; /* output attributes in upper not lower case */
		protected internal bool MakeClean = false; /* remove presentational clutter */
		protected internal bool LogicalEmphasis = false; /* replace i by em and b by strong */
		protected internal bool DropFontTags = false; /* discard presentation tags */
		protected internal bool DropEmptyParas = true; /* discard empty p elements */
		protected internal bool FixComments = true; /* fix comments with adjacent hyphens */
		protected internal bool BreakBeforeBR = false; /* o/p newline before <br> or not? */
		protected internal bool BurstSlides = false; /* create slides on each h2 element */
		protected internal bool NumEntities = false; /* use numeric entities */
		protected internal bool QuoteMarks = false; /* output " marks as &quot; */
		protected internal bool QuoteNbsp = true; /* output non-breaking space as entity */
		protected internal bool QuoteAmpersand = true; /* output naked ampersand as &amp; */
		protected internal bool WrapAttVals = false; /* wrap within attribute values */
		protected internal bool WrapScriptlets = false; /* wrap within JavaScript string literals */
		protected internal bool WrapSection = true; /* wrap within <![ ... ]> section tags */
		protected internal bool WrapAsp = true; /* wrap within ASP pseudo elements */
		protected internal bool WrapJste = true; /* wrap within JSTE pseudo elements */
		protected internal bool WrapPhp = true; /* wrap within PHP pseudo elements */
		protected internal bool FixBackslash = true; /* fix URLs by replacing \ with / */
		protected internal bool IndentAttributes = false; /* newline+indent before each attribute */
		protected internal bool XmlPIs = false; /* if set to yes PIs must end with ?> */
		protected internal bool XmlSpace = false; /* if set to yes adds xml:space attr as needed */
		protected internal bool EncloseBodyText = false; /* if yes text at body is wrapped in <p>'s */
		protected internal bool EncloseBlockText = false; /* if yes text in blocks is wrapped in <p>'s */
		protected internal bool KeepFileTimes = true; /* if yes last modied time is preserved */
		protected internal bool Word2000 = false; /* draconian cleaning for Word2000 */
		protected internal bool TidyMark = true; /* add meta element indicating tidied doc */
		protected internal bool Emacs = false; /* if true format error output for GNU Emacs */
		protected internal bool LiteralAttribs = false; /* if true attributes may use newlines */
		
		protected internal TagTable tt; /* TagTable associated with this Configuration */
		
		//UPGRADE_NOTE: The initialization of  '_properties' was moved to method 'InitBlock'. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1005"'
		[NonSerialized()]
		private System.Collections.Specialized.NameValueCollection _properties;	//	-TH
		//private System.Collections.IDictionary _properties;
		//private System.Data.PropertyCollection _properties;
		
		public Configuration()
		{
			InitBlock();
		}
		
		public virtual void  addProps(System.Collections.Specialized.NameValueCollection p)
		{
			System.Collections.IEnumerator enum_Renamed = p.Keys.GetEnumerator();
			//UPGRADE_TODO: Method 'java.util.Enumeration.hasMoreElements' was converted to 'System.Collections.IEnumerator.MoveNext' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
			while (enum_Renamed.MoveNext())
			{
				//UPGRADE_TODO: Method 'java.util.Enumeration.nextElement' was converted to 'System.Collections.IEnumerator.Current' which has a different behavior. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1073"'
				System.String key = (System.String) enum_Renamed.Current;
				System.String value_Renamed = p[key];
				SupportClass.PutElement(_properties, key, value_Renamed);
			}
			parseProps();
		}
		
		public virtual void  parseFile(System.String filename)
		{
			try
			{
				new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
				_properties = new System.Collections.Specialized.NameValueCollection(System.Configuration.ConfigurationSettings.AppSettings);
			}
			catch (System.IO.IOException e)
			{
				//UPGRADE_TODO: The equivalent in .NET for method 'java.lang.Throwable.toString' may return a different value. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1043"'
				System.Console.Error.WriteLine(filename + e.ToString());
				return ;
			}
			parseProps();
		}
		
		private void  parseProps()
		{
			System.String value_Renamed;
			
			value_Renamed = _properties["indent-spaces"].ToString();
			if ((System.Object) value_Renamed != null)
				spaces = parseInt(value_Renamed, "indent-spaces");
			
			value_Renamed = _properties["wrap"].ToString();
			if ((System.Object) value_Renamed != null)
				wraplen = parseInt(value_Renamed, "wrap");
			
			value_Renamed = _properties["wrap-attributes"].ToString();
			if ((System.Object) value_Renamed != null)
				WrapAttVals = parseBool(value_Renamed, "wrap-attributes");
			
			value_Renamed = _properties["wrap-script-literals"].ToString();
			if ((System.Object) value_Renamed != null)
				WrapScriptlets = parseBool(value_Renamed, "wrap-script-literals");
			
			value_Renamed = _properties["wrap-sections"].ToString();
			if ((System.Object) value_Renamed != null)
				WrapSection = parseBool(value_Renamed, "wrap-sections");
			
			value_Renamed = _properties["wrap-asp"].ToString();
			if ((System.Object) value_Renamed != null)
				WrapAsp = parseBool(value_Renamed, "wrap-asp");
			
			value_Renamed = _properties["wrap-jste"].ToString();
			if ((System.Object) value_Renamed != null)
				WrapJste = parseBool(value_Renamed, "wrap-jste");
			
			value_Renamed = _properties["wrap-php"].ToString();
			if ((System.Object) value_Renamed != null)
				WrapPhp = parseBool(value_Renamed, "wrap-php");
			
			value_Renamed = _properties["literal-attributes"].ToString();
			if ((System.Object) value_Renamed != null)
				LiteralAttribs = parseBool(value_Renamed, "literal-attributes");
			
			value_Renamed = _properties["tab-size"].ToString();
			if ((System.Object) value_Renamed != null)
				tabsize = parseInt(value_Renamed, "tab-size");
			
			value_Renamed = _properties["markup"].ToString();
			if ((System.Object) value_Renamed != null)
				OnlyErrors = parseInvBool(value_Renamed, "markup");
			
			value_Renamed = _properties["quiet"].ToString();
			if ((System.Object) value_Renamed != null)
				Quiet = parseBool(value_Renamed, "quiet");
			
			value_Renamed = _properties["tidy-mark"].ToString();
			if ((System.Object) value_Renamed != null)
				TidyMark = parseBool(value_Renamed, "tidy-mark");
			
			value_Renamed = _properties["indent"].ToString();
			if ((System.Object) value_Renamed != null)
				IndentContent = parseIndent(value_Renamed, "indent");
			
			value_Renamed = _properties["indent-attributes"].ToString();
			if ((System.Object) value_Renamed != null)
				IndentAttributes = parseBool(value_Renamed, "ident-attributes");
			
			value_Renamed = _properties["hide-endtags"].ToString();
			if ((System.Object) value_Renamed != null)
				HideEndTags = parseBool(value_Renamed, "hide-endtags");
			
			value_Renamed = _properties["input-xml"].ToString();
			if ((System.Object) value_Renamed != null)
				XmlTags = parseBool(value_Renamed, "input-xml");
			
			value_Renamed = _properties["output-xml"].ToString();
			if ((System.Object) value_Renamed != null)
				XmlOut = parseBool(value_Renamed, "output-xml");
			
			value_Renamed = _properties["output-xhtml"].ToString();
			if ((System.Object) value_Renamed != null)
				xHTML = parseBool(value_Renamed, "output-xhtml");
			
			value_Renamed = _properties["add-xml-pi"].ToString();
			if ((System.Object) value_Renamed != null)
				XmlPi = parseBool(value_Renamed, "add-xml-pi");
			
			value_Renamed = _properties["add-xml-decl"].ToString();
			if ((System.Object) value_Renamed != null)
				XmlPi = parseBool(value_Renamed, "add-xml-decl");
			
			value_Renamed = _properties["assume-xml-procins"].ToString();
			if ((System.Object) value_Renamed != null)
				XmlPIs = parseBool(value_Renamed, "assume-xml-procins");
			
			value_Renamed = _properties["raw"].ToString();
			if ((System.Object) value_Renamed != null)
				RawOut = parseBool(value_Renamed, "raw");
			
			value_Renamed = _properties["uppercase-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				UpperCaseTags = parseBool(value_Renamed, "uppercase-tags");
			
			value_Renamed = _properties["uppercase-attributes"].ToString();
			if ((System.Object) value_Renamed != null)
				UpperCaseAttrs = parseBool(value_Renamed, "uppercase-attributes");
			
			value_Renamed = _properties["clean"].ToString();
			if ((System.Object) value_Renamed != null)
				MakeClean = parseBool(value_Renamed, "clean");
			
			value_Renamed = _properties["logical-emphasis"].ToString();
			if ((System.Object) value_Renamed != null)
				LogicalEmphasis = parseBool(value_Renamed, "logical-emphasis");
			
			value_Renamed = _properties["word-2000"].ToString();
			if ((System.Object) value_Renamed != null)
				Word2000 = parseBool(value_Renamed, "word-2000");
			
			value_Renamed = _properties["drop-empty-paras"].ToString();
			if ((System.Object) value_Renamed != null)
				DropEmptyParas = parseBool(value_Renamed, "drop-empty-paras");
			
			value_Renamed = _properties["drop-font-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				DropFontTags = parseBool(value_Renamed, "drop-font-tags");
			
			value_Renamed = _properties["enclose-text"].ToString();
			if ((System.Object) value_Renamed != null)
				EncloseBodyText = parseBool(value_Renamed, "enclose-text");
			
			value_Renamed = _properties["enclose-block-text"].ToString();
			if ((System.Object) value_Renamed != null)
				EncloseBlockText = parseBool(value_Renamed, "enclose-block-text");
			
			value_Renamed = _properties["alt-text"].ToString();
			if ((System.Object) value_Renamed != null)
				altText = value_Renamed;
			
			value_Renamed = _properties["add-xml-space"].ToString();
			if ((System.Object) value_Renamed != null)
				XmlSpace = parseBool(value_Renamed, "add-xml-space");
			
			value_Renamed = _properties["fix-bad-comments"].ToString();
			if ((System.Object) value_Renamed != null)
				FixComments = parseBool(value_Renamed, "fix-bad-comments");
			
			value_Renamed = _properties["split"].ToString();
			if ((System.Object) value_Renamed != null)
				BurstSlides = parseBool(value_Renamed, "split");
			
			value_Renamed = _properties["break-before-br"].ToString();
			if ((System.Object) value_Renamed != null)
				BreakBeforeBR = parseBool(value_Renamed, "break-before-br");
			
			value_Renamed = _properties["numeric-entities"].ToString();
			if ((System.Object) value_Renamed != null)
				NumEntities = parseBool(value_Renamed, "numeric-entities");
			
			value_Renamed = _properties["quote-marks"].ToString();
			if ((System.Object) value_Renamed != null)
				QuoteMarks = parseBool(value_Renamed, "quote-marks");
			
			value_Renamed = _properties["quote-nbsp"].ToString();
			if ((System.Object) value_Renamed != null)
				QuoteNbsp = parseBool(value_Renamed, "quote-nbsp");
			
			value_Renamed = _properties["quote-ampersand"].ToString();
			if ((System.Object) value_Renamed != null)
				QuoteAmpersand = parseBool(value_Renamed, "quote-ampersand");
			
			value_Renamed = _properties["write-back"].ToString();
			if ((System.Object) value_Renamed != null)
				writeback = parseBool(value_Renamed, "write-back");
			
			value_Renamed = _properties["keep-time"].ToString();
			if ((System.Object) value_Renamed != null)
				KeepFileTimes = parseBool(value_Renamed, "keep-time");
			
			value_Renamed = _properties["show-warnings"].ToString();
			if ((System.Object) value_Renamed != null)
				ShowWarnings = parseBool(value_Renamed, "show-warnings");
			
			value_Renamed = _properties["error-file"].ToString();
			if ((System.Object) value_Renamed != null)
				errfile = parseName(value_Renamed, "error-file");
			
			value_Renamed = _properties["slide-style"].ToString();
			if ((System.Object) value_Renamed != null)
				slidestyle = parseName(value_Renamed, "slide-style");
			
			value_Renamed = _properties["new-inline-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				parseInlineTagNames(value_Renamed, "new-inline-tags");
			
			value_Renamed = _properties["new-blocklevel-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				parseBlockTagNames(value_Renamed, "new-blocklevel-tags");
			
			value_Renamed = _properties["new-empty-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				parseEmptyTagNames(value_Renamed, "new-empty-tags");
			
			value_Renamed = _properties["new-pre-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				parsePreTagNames(value_Renamed, "new-pre-tags");
			
			value_Renamed = _properties["char-encoding"].ToString();
			if ((System.Object) value_Renamed != null)
				CharEncoding = parseCharEncoding(value_Renamed, "char-encoding");
			
			value_Renamed = _properties["doctype"].ToString();
			if ((System.Object) value_Renamed != null)
				docTypeStr = parseDocType(value_Renamed, "doctype");
			
			value_Renamed = _properties["fix-backslash"].ToString();
			if ((System.Object) value_Renamed != null)
				FixBackslash = parseBool(value_Renamed, "fix-backslash");
			
			value_Renamed = _properties["gnu-emacs"].ToString();
			if ((System.Object) value_Renamed != null)
				Emacs = parseBool(value_Renamed, "gnu-emacs");
		}
		
		/* ensure that config is self consistent */
		public virtual void  adjust()
		{
			if (EncloseBlockText)
				EncloseBodyText = true;
			
			/* avoid the need to set IndentContent when SmartIndent is set */
			
			if (SmartIndent)
				IndentContent = true;
			
			/* disable wrapping */
			if (wraplen == 0)
				wraplen = 0x7FFFFFFF;
			
			/* Word 2000 needs o:p to be declared as inline */
			if (Word2000)
			{
				tt.defineInlineTag("o:p");
			}
			
			/* XHTML is written in lower case */
			if (xHTML)
			{
				XmlOut = true;
				UpperCaseTags = false;
				UpperCaseAttrs = false;
			}
			
			/* if XML in, then XML out */
			if (XmlTags)
			{
				XmlOut = true;
				XmlPIs = true;
			}
			
			/* XML requires end tags */
			if (XmlOut)
			{
				QuoteAmpersand = true;
				HideEndTags = false;
			}
		}
		
		private static int parseInt(System.String s, System.String option)
		{
			int i = 0;
			try
			{
				i = System.Int32.Parse(s);
			}
			catch //(System.FormatException e)
			{
				Report.badArgument(option);
				i = - 1;
			}
			return i;
		}
		
		private static bool parseBool(System.String s, System.String option)
		{
			bool b = false;
			if ((System.Object) s != null && s.Length > 0)
			{
				char c = s[0];
				if ((c == 't') || (c == 'T') || (c == 'Y') || (c == 'y') || (c == '1'))
					b = true;
				else if ((c == 'f') || (c == 'F') || (c == 'N') || (c == 'n') || (c == '0'))
					b = false;
				else
					Report.badArgument(option);
			}
			return b;
		}
		
		private static bool parseInvBool(System.String s, System.String option)
		{
			bool b = false;
			if ((System.Object) s != null && s.Length > 0)
			{
				char c = s[0];
				if ((c == 't') || (c == 'T') || (c == 'Y') || (c == 'y'))
					b = true;
				else if ((c == 'f') || (c == 'F') || (c == 'N') || (c == 'n'))
					b = false;
				else
					Report.badArgument(option);
			}
			return !b;
		}
		
		private static System.String parseName(System.String s, System.String option)
		{
			SupportClass.Tokenizer t = new SupportClass.Tokenizer(s);
			System.String rs = null;
			if (t.Count >= 1)
				rs = t.NextToken();
			else
				Report.badArgument(option);
			return rs;
		}
		
		private static int parseCharEncoding(System.String s, System.String option)
		{
			int result = ASCII;
			
			if (Lexer.wstrcasecmp(s, "ascii") == 0)
				result = ASCII;
			else if (Lexer.wstrcasecmp(s, "latin1") == 0)
				result = LATIN1;
			else if (Lexer.wstrcasecmp(s, "raw") == 0)
				result = RAW;
			else if (Lexer.wstrcasecmp(s, "utf8") == 0)
				result = UTF8;
			else if (Lexer.wstrcasecmp(s, "iso2022") == 0)
				result = ISO2022;
			else if (Lexer.wstrcasecmp(s, "mac") == 0)
				result = MACROMAN;
			else
				Report.badArgument(option);
			
			return result;
		}
		
		/* slight hack to avoid changes to pprint.c */
		private bool parseIndent(System.String s, System.String option)
		{
			bool b = IndentContent;
			
			if (Lexer.wstrcasecmp(s, "yes") == 0)
			{
				b = true;
				SmartIndent = false;
			}
			else if (Lexer.wstrcasecmp(s, "true") == 0)
			{
				b = true;
				SmartIndent = false;
			}
			else if (Lexer.wstrcasecmp(s, "no") == 0)
			{
				b = false;
				SmartIndent = false;
			}
			else if (Lexer.wstrcasecmp(s, "false") == 0)
			{
				b = false;
				SmartIndent = false;
			}
			else if (Lexer.wstrcasecmp(s, "auto") == 0)
			{
				b = true;
				SmartIndent = true;
			}
			else
				Report.badArgument(option);
			return b;
		}
		
		private void  parseInlineTagNames(System.String s, System.String option)
		{
			SupportClass.Tokenizer t = new SupportClass.Tokenizer(s, " \t\n\r,");
			while (t.HasMoreTokens())
			{
				tt.defineInlineTag(t.NextToken());
			}
		}
		
		private void  parseBlockTagNames(System.String s, System.String option)
		{
			SupportClass.Tokenizer t = new SupportClass.Tokenizer(s, " \t\n\r,");
			while (t.HasMoreTokens())
			{
				tt.defineBlockTag(t.NextToken());
			}
		}
		
		private void  parseEmptyTagNames(System.String s, System.String option)
		{
			SupportClass.Tokenizer t = new SupportClass.Tokenizer(s, " \t\n\r,");
			while (t.HasMoreTokens())
			{
				tt.defineEmptyTag(t.NextToken());
			}
		}
		
		private void  parsePreTagNames(System.String s, System.String option)
		{
			SupportClass.Tokenizer t = new SupportClass.Tokenizer(s, " \t\n\r,");
			while (t.HasMoreTokens())
			{
				tt.definePreTag(t.NextToken());
			}
		}
		
		/*
		doctype: omit | auto | strict | loose | <fpi>
		
		where the fpi is a string similar to
		
		"-//ACME//DTD HTML 3.14159//EN"
		*/
		protected internal virtual System.String parseDocType(System.String s, System.String option)
		{
			s = s.Trim();
			
			/* "-//ACME//DTD HTML 3.14159//EN" or similar */
			
			if (s.StartsWith("\""))
			{
				docTypeMode = DOCTYPE_USER;
				return s;
			}
			
			/* read first word */
			System.String word = "";
			SupportClass.Tokenizer t = new SupportClass.Tokenizer(s, " \t\n\r,");
			if (t.HasMoreTokens())
				word = t.NextToken();
			
			if (Lexer.wstrcasecmp(word, "omit") == 0)
				docTypeMode = DOCTYPE_OMIT;
			else if (Lexer.wstrcasecmp(word, "strict") == 0)
				docTypeMode = DOCTYPE_STRICT;
			else if (Lexer.wstrcasecmp(word, "loose") == 0 || Lexer.wstrcasecmp(word, "transitional") == 0)
				docTypeMode = DOCTYPE_LOOSE;
			else if (Lexer.wstrcasecmp(word, "auto") == 0)
				docTypeMode = DOCTYPE_AUTO;
			else
			{
				docTypeMode = DOCTYPE_AUTO;
				Report.badArgument(option);
			}
			return null;
		}
	}
}