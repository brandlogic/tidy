/*
* @(#)DOMAttrImpl.java   1.11 2000/08/16
*
*/
using System;
namespace org.w3c.tidy
{
	
	/// <summary> 
	/// DOMAttrImpl
	/// 
	/// (c) 1998-2000 (W3C) MIT, INRIA, Keio University
	/// See Tidy.java for the copyright notice.
	/// Derived from <a href="http://www.w3.org/People/Raggett/tidy">
	/// HTML Tidy Release 4 Aug 2000</a>
	/// 
	/// </summary>
	/// <author>   Dave Raggett <dsr@w3.org>
	/// </author>
	/// <author>   Andy Quick <ac.quick@sympatico.ca> (translation to Java)
	/// </author>
	/// <version>  1.4, 1999/09/04 DOM Support
	/// </version>
	/// <version>  1.5, 1999/10/23 Tidy Release 27 Sep 1999
	/// </version>
	/// <version>  1.6, 1999/11/01 Tidy Release 22 Oct 1999
	/// </version>
	/// <version>  1.7, 1999/12/06 Tidy Release 30 Nov 1999
	/// </version>
	/// <version>  1.8, 2000/01/22 Tidy Release 13 Jan 2000
	/// </version>
	/// <version>  1.9, 2000/06/03 Tidy Release 30 Apr 2000
	/// </version>
	/// <version>  1.10, 2000/07/22 Tidy Release 8 Jul 2000
	/// </version>
	/// <version>  1.11, 2000/08/16 Tidy Release 4 Aug 2000
	/// </version>
	
	//UPGRADE_TODO: Interface 'org.w3c.dom.Attr' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
	public class DOMAttrImpl:DOMNodeImpl, org.w3c.dom.Attr
	{
		override public System.String NodeValue
		{
			/* --------------------- DOM ---------------------------- */
			
			
			get
			{
				//UPGRADE_TODO: Method 'org.w3c.dom.Attr.getValue' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				//return getValue();
				return this.Value;
			}
			
			set
			{
				//UPGRADE_TODO: Method 'org.w3c.dom.Attr.setValue' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				//setValue(value);
				this.Value = value;
			}
			
		}
		override public System.String NodeName
		{
			get
			{
				//UPGRADE_TODO: Method 'org.w3c.dom.Attr.getName' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				//return getName();
				return this.Name;
			}
			
		}
		override public short NodeType
		{
			get
			{
				//UPGRADE_TODO: Field 'org.w3c.dom.Node.ATTRIBUTE_NODE' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
				return (short)org.w3c.dom.Node_Fields.ATTRIBUTE_NODE;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.Node ParentNode
		{
			get
			{
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.NodeList' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.NodeList ChildNodes
		{
			get
			{
				// NOT SUPPORTED
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.Node FirstChild
		{
			get
			{
				// NOT SUPPORTED
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.Node LastChild
		{
			get
			{
				// NOT SUPPORTED
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.Node PreviousSibling
		{
			get
			{
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.Node NextSibling
		{
			get
			{
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.NamedNodeMap' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.NamedNodeMap Attributes
		{
			get
			{
				return null;
			}
			
		}
		//UPGRADE_TODO: Interface 'org.w3c.dom.Document' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		override public org.w3c.dom.Document OwnerDocument
		{
			get
			{
				return null;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Attr#getName">
		/// </seealso>
		virtual public System.String Name
		{
			get
			{
				return avAdaptee.attribute;
			}
			
		}
		/// <seealso cref="org.w3c.dom.Attr#getSpecified">
		/// </seealso>
		virtual public bool Specified
		{
			get
			{
				return true;
			}
			
		}
		//UPGRADE_NOTE: Respective javadoc comments were merged.  It should be changed in order to comply with .NET documentation conventions.
		/// <summary> Returns value of this attribute.  If this attribute has a null value,
		/// then the attribute name is returned instead.
		/// Thanks to Brett Knights <brett@knightsofthenet.com> for this fix.
		/// </summary>
		/// <seealso cref="org.w3c.dom.Attr#getValue">
		/// 
		/// </seealso>
		/// <seealso cref="org.w3c.dom.Attr#setValue">
		/// </seealso>
		virtual public System.String Value
		{
			get
			{
				return ((System.Object) avAdaptee.value_Renamed == null)?avAdaptee.attribute:avAdaptee.value_Renamed;
			}
			
			set
			{
				avAdaptee.value_Renamed = value;
			}
			
		}
		/// <summary> DOM2 - not implemented.</summary>
		//UPGRADE_TODO: Interface 'org.w3c.dom.Element' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		virtual public org.w3c.dom.Element OwnerElement
		{
			get
			{
				return null;
			}
			
		}
		
		protected internal AttVal avAdaptee;
		
		protected internal DOMAttrImpl(AttVal adaptee):base(null)
		{ // must override all methods of DOMNodeImpl
			this.avAdaptee = adaptee;
		}
		
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public override org.w3c.dom.Node insertBefore(org.w3c.dom.Node newChild, org.w3c.dom.Node refChild)
		{
			//UPGRADE_TODO: Field 'org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
			throw new DOMExceptionImpl(org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR, "Not supported");
		}
		
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public override org.w3c.dom.Node replaceChild(org.w3c.dom.Node newChild, org.w3c.dom.Node oldChild)
		{
			//UPGRADE_TODO: Field 'org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
			throw new DOMExceptionImpl(org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR, "Not supported");
		}
		
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public override org.w3c.dom.Node removeChild(org.w3c.dom.Node oldChild)
		{
			//UPGRADE_TODO: Field 'org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
			throw new DOMExceptionImpl(org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR, "Not supported");
		}
		
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public override org.w3c.dom.Node appendChild(org.w3c.dom.Node newChild)
		{
			//UPGRADE_TODO: Field 'org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
			throw new DOMExceptionImpl(org.w3c.dom.DOMException.NO_MODIFICATION_ALLOWED_ERR, "Not supported");
		}
		
		public override bool hasChildNodes()
		{
			return false;
		}
		
		//UPGRADE_TODO: Interface 'org.w3c.dom.Node' was not converted. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1095"'
		public override org.w3c.dom.Node cloneNode(bool deep)
		{
			return null;
		}
	}
}