using System;
using System.Xml.Xsl;

namespace BL.ServerControls.TidyLite
{
	/// <summary>Read configuration file and manage configuration properties.</summary>
	public class Configuration
	{
		private void InitBlock()
		{
			_properties = new System.Collections.Specialized.NameValueCollection();
		}

		protected internal int spaces = 2; /* default indentation */
		protected internal int wraplen = int.MaxValue; /* default wrap margin */
		protected internal int tabsize = 4;
		protected internal bool ShowWarnings = true; /* however errors are always shown */
		protected internal bool Quiet = false; /* no 'Parsing X', guessed DTD or summary */
		protected internal bool IndentContent = false; /* indent content of appropriate tags */
		protected internal bool LogicalEmphasis = false; /* replace i by em and b by strong */
		protected internal bool DropFontTags = false; /* discard presentation tags */
		protected internal bool FixComments = true; /* fix comments with adjacent hyphens */
		protected internal bool QuoteMarks = false; /* output " marks as &quot; */
		protected internal TagTable tt; /* TagTable associated with this Configuration */
		protected internal bool StripBaseHref = false;
		protected internal string Domain = String.Empty;
		protected internal bool StripRedundantURLBeforeHash = false;
		protected internal string CurrentURL = String.Empty;
		protected internal XslCompiledTransform XslTransformer = null;
		private System.Collections.Specialized.NameValueCollection _properties;	//	-TH
		
		public Configuration()
		{
			InitBlock();
		}
		
		public virtual void addProps(System.Collections.Specialized.NameValueCollection p)
		{
			System.Collections.IEnumerator enum_Renamed = p.Keys.GetEnumerator();
			while (enum_Renamed.MoveNext())
			{
				System.String key = (System.String) enum_Renamed.Current;
				System.String value_Renamed = p[key];
				SupportClass.PutElement(_properties, key, value_Renamed);
			}
			parseProps();
		}
		
		public virtual void parseFile(System.String filename)
		{
			try
			{
				new System.IO.FileStream(filename, System.IO.FileMode.Open, System.IO.FileAccess.Read);
				_properties = new System.Collections.Specialized.NameValueCollection(System.Configuration.ConfigurationSettings.AppSettings);
			}
			catch (System.IO.IOException e)
			{
				System.Console.Error.WriteLine(filename + e.ToString());
				return ;
			}
			parseProps();
		}
		
		private void parseProps()
		{
			System.String value_Renamed;

			value_Renamed = _properties["strip-base-href"].ToString();
			if ((System.Object) value_Renamed != null)
				StripBaseHref = parseBool(value_Renamed, "strip-base-href");

			value_Renamed = _properties["domain"].ToString();
			if ((System.Object) value_Renamed != null)
				Domain = value_Renamed;

			value_Renamed = _properties["strip-hash"].ToString();
			if ((System.Object) value_Renamed != null)
				StripRedundantURLBeforeHash = parseBool(value_Renamed, "strip-hash");

			value_Renamed = _properties["current-url"].ToString();
			if ((System.Object) value_Renamed != null)
				CurrentURL = value_Renamed;

			value_Renamed = _properties["indent-spaces"].ToString();
			if ((System.Object) value_Renamed != null)
				spaces = parseInt(value_Renamed, "indent-spaces");
			
			value_Renamed = _properties["wrap"].ToString();
			if ((System.Object) value_Renamed != null)
				wraplen = parseInt(value_Renamed, "wrap");
			
			value_Renamed = _properties["tab-size"].ToString();
			if ((System.Object) value_Renamed != null)
				tabsize = parseInt(value_Renamed, "tab-size");
			
			value_Renamed = _properties["quiet"].ToString();
			if ((System.Object) value_Renamed != null)
				Quiet = parseBool(value_Renamed, "quiet");
			
			value_Renamed = _properties["indent"].ToString();
			if ((System.Object) value_Renamed != null)
				IndentContent = parseIndent(value_Renamed, "indent");
			
			value_Renamed = _properties["logical-emphasis"].ToString();
			if ((System.Object) value_Renamed != null)
				LogicalEmphasis = parseBool(value_Renamed, "logical-emphasis");
			
			value_Renamed = _properties["drop-font-tags"].ToString();
			if ((System.Object) value_Renamed != null)
				DropFontTags = parseBool(value_Renamed, "drop-font-tags");
			
			value_Renamed = _properties["fix-bad-comments"].ToString();
			if ((System.Object) value_Renamed != null)
				FixComments = parseBool(value_Renamed, "fix-bad-comments");
			
			value_Renamed = _properties["quote-marks"].ToString();
			if ((System.Object) value_Renamed != null)
				QuoteMarks = parseBool(value_Renamed, "quote-marks");
			
			value_Renamed = _properties["show-warnings"].ToString();
			if ((System.Object) value_Renamed != null)
				ShowWarnings = parseBool(value_Renamed, "show-warnings");
			
		}
		
		private static int parseInt(string s, string option)
		{
			int i = 0;
			try
			{
				i = System.Int32.Parse(s);
			}
			catch
			{
				Report.badArgument(option);
				i = - 1;
			}
			return i;
		}
		
		private static bool parseBool(System.String s, System.String option)
		{
			bool b = false;
			if ((System.Object) s != null && s.Length > 0)
			{
				char c = s[0];
				if ((c == 't') || (c == 'T') || (c == 'Y') || (c == 'y') || (c == '1'))
					b = true;
				else if ((c == 'f') || (c == 'F') || (c == 'N') || (c == 'n') || (c == '0'))
					b = false;
				else
					Report.badArgument(option);
			}
			return b;
		}
		
		private bool parseIndent(System.String s, System.String option)
		{
			bool b = IndentContent;
			
			if (Lexer.wstrcasecmp(s, "yes") == 0)
			{
				b = true;
			}
			else if (Lexer.wstrcasecmp(s, "true") == 0)
			{
				b = true;
			}
			else if (Lexer.wstrcasecmp(s, "no") == 0)
			{
				b = false;
			}
			else if (Lexer.wstrcasecmp(s, "false") == 0)
			{
				b = false;
			}
			else if (Lexer.wstrcasecmp(s, "auto") == 0)
			{
				b = true;
			}
			else
				Report.badArgument(option);
			return b;
		}		
	}
}