using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>HTML Parser</summary>
	public interface Parser
	{
		void  parse(Lexer lexer, Node node, short mode);
	}
}