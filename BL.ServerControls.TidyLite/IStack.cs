using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Inline stack node</summary>
	public class IStack
	{
		public IStack next;
		public Dict tag; /* tag's dictionary definition */
		public string element; /* name (null for text nodes) */
		public AttVal attributes;
		
		public IStack()
		{
			next = null;
			tag = null;
			element = null;
			attributes = null;
		}
	}
}