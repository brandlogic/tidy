using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Check attribute values</summary>
	public interface AttrCheck
		{
			void  check(Lexer lexer, Node node, AttVal attval);
		}
}