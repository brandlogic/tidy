using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Mutable Object</summary>
	public class MutableObject
	{
		virtual public System.Object Object
		{
			get
			{
				return value_Renamed;
			}
			set
			{
				value = value;
			}
		}
		
		public MutableObject():this(null)
		{
		}
		
		public MutableObject(System.Object o)
		{
			this.value_Renamed = o;
		}
		
		private System.Object value_Renamed;
	}
}