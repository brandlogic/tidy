using System;
namespace BL.ServerControls.TidyLite
{	
	/// <summary>Tag dictionary node hash table</summary>
	public class TagTable
	{
		private void  InitBlock()
		{
			tagHashtable = new System.Collections.Hashtable();
		}
		virtual public Configuration Configuration
		{
			set
			{
				this.configuration = value;
			}
		}
		
		private Configuration configuration = null;
		
		public TagTable()
		{
			InitBlock();
			for (int i = 0; i < tags.Length; i++)
			{
				install(tags[i]);
			}
			tagHtml = lookup("html");
			tagHead = lookup("head");
			tagBody = lookup("body");
			tagFrameset = lookup("frameset");
			tagFrame = lookup("frame");
			tagNoframes = lookup("noframes");
			tagMeta = lookup("meta");
			tagTitle = lookup("title");
			tagBase = lookup("base");
			tagHr = lookup("hr");
			tagPre = lookup("pre");
			tagListing = lookup("listing");
			tagH1 = lookup("h1");
			tagH2 = lookup("h2");
			tagP = lookup("p");
			tagUl = lookup("ul");
			tagOl = lookup("ol");
			tagDir = lookup("dir");
			tagLi = lookup("li");
			tagDt = lookup("dt");
			tagDd = lookup("dd");
			tagDl = lookup("dl");
			tagTd = lookup("td");
			tagTh = lookup("th");
			tagTr = lookup("tr");
			tagCol = lookup("col");
			tagBr = lookup("br");
			tagA = lookup("a");
			tagLink = lookup("link");
			tagB = lookup("b");
			tagI = lookup("i");
			tagStrong = lookup("strong");
			tagEm = lookup("em");
			tagBig = lookup("big");
			tagSmall = lookup("small");
			tagParam = lookup("param");
			tagOption = lookup("option");
			tagOptgroup = lookup("optgroup");
			tagImg = lookup("img");
			tagMap = lookup("map");
			tagArea = lookup("area");
			tagNobr = lookup("nobr");
			tagWbr = lookup("wbr");
			tagFont = lookup("font");
			tagSpacer = lookup("spacer");
			tagLayer = lookup("layer");
			tagCenter = lookup("center");
			tagStyle = lookup("style");
			tagScript = lookup("script");
			tagNoscript = lookup("noscript");
			tagTable = lookup("table");
			tagCaption = lookup("caption");
			tagForm = lookup("form");
			tagTextarea = lookup("textarea");
			tagBlockquote = lookup("blockquote");
			tagApplet = lookup("applet");
			tagObject = lookup("object");
			tagDiv = lookup("div");
			tagSpan = lookup("span");
			tagIframe = lookup("iframe");
		}
		
		public virtual Dict lookup(System.String name)
		{
			return (Dict) tagHashtable[name];
		}
		
		public virtual Dict install(Dict dict)
		{
			Dict d = (Dict) tagHashtable[dict.name];
			if (d != null)
			{
				d.model |= dict.model;
				d.parser = dict.parser;
				d.chkattrs = dict.chkattrs;
				return d;
			}
			else
			{
				SupportClass.PutElement(tagHashtable, dict.name, dict);
				return dict;
			}
		}
		
		public virtual bool findTag(Node node)
		{
			Dict np;

			if ((System.Object) node.element != null)
			{
				np = lookup(node.element);
				if (np != null)
				{
					node.tag = np;
					return true;
				}
			}
			
			return false;
		}
		
		private System.Collections.Hashtable tagHashtable;
		private static Dict[] tags;
		
		public Dict tagHtml = null;
		public Dict tagHead = null;
		public Dict tagBody = null;
		public Dict tagFrameset = null;
		public Dict tagFrame = null;
		public Dict tagNoframes = null;
		public Dict tagMeta = null;
		public Dict tagTitle = null;
		public Dict tagBase = null;
		public Dict tagHr = null;
		public Dict tagPre = null;
		public Dict tagListing = null;
		public Dict tagH1 = null;
		public Dict tagH2 = null;
		public Dict tagP = null;
		public Dict tagUl = null;
		public Dict tagOl = null;
		public Dict tagDir = null;
		public Dict tagLi = null;
		public Dict tagDt = null;
		public Dict tagDd = null;
		public Dict tagDl = null;
		public Dict tagTd = null;
		public Dict tagTh = null;
		public Dict tagTr = null;
		public Dict tagCol = null;
		public Dict tagBr = null;
		public Dict tagA = null;
		public Dict tagLink = null;
		public Dict tagB = null;
		public Dict tagI = null;
		public Dict tagStrong = null;
		public Dict tagEm = null;
		public Dict tagBig = null;
		public Dict tagSmall = null;
		public Dict tagParam = null;
		public Dict tagOption = null;
		public Dict tagOptgroup = null;
		public Dict tagImg = null;
		public Dict tagMap = null;
		public Dict tagArea = null;
		public Dict tagNobr = null;
		public Dict tagWbr = null;
		public Dict tagFont = null;
		public Dict tagSpacer = null;
		public Dict tagLayer = null;
		public Dict tagCenter = null;
		public Dict tagStyle = null;
		public Dict tagScript = null;
		public Dict tagNoscript = null;
		public Dict tagTable = null;
		public Dict tagCaption = null;
		public Dict tagForm = null;
		public Dict tagTextarea = null;
		public Dict tagBlockquote = null;
		public Dict tagApplet = null;
		public Dict tagObject = null;
		public Dict tagDiv = null;
		public Dict tagSpan = null;
		public Dict tagIframe = null;
		
		static TagTable()
		{
			tags = new Dict[]{
				new Dict("html", (Dict.CM_HTML | Dict.CM_OPT | Dict.CM_OMITST), ParserImpl.getParseHTML(), CheckAttribsImpl.getCheckHTML()), 
				new Dict("head", (Dict.CM_HTML | Dict.CM_OPT | Dict.CM_OMITST), ParserImpl.getParseHead(), null), 
				new Dict("title", Dict.CM_HEAD, ParserImpl.getParseTitle(), null), 
				new Dict("base", (Dict.CM_HEAD | Dict.CM_EMPTY), null, null), 
				new Dict("link", (Dict.CM_HEAD | Dict.CM_EMPTY), null, CheckAttribsImpl.getCheckLINK()), 
				new Dict("meta", (Dict.CM_HEAD | Dict.CM_EMPTY), null, null), 
				new Dict("style", Dict.CM_HEAD, ParserImpl.getParseScript(), CheckAttribsImpl.getCheckSTYLE()), 
				new Dict("script", (Dict.CM_HEAD | Dict.CM_MIXED | Dict.CM_BLOCK | Dict.CM_INLINE), ParserImpl.getParseScript(), CheckAttribsImpl.getCheckSCRIPT()), 
				new Dict("server", (Dict.CM_HEAD | Dict.CM_MIXED | Dict.CM_BLOCK | Dict.CM_INLINE), ParserImpl.getParseScript(), null), 
				new Dict("body", (Dict.CM_HTML | Dict.CM_OPT | Dict.CM_OMITST), ParserImpl.getParseBody(), null), 
				new Dict("frameset", (Dict.CM_HTML | Dict.CM_FRAMES), ParserImpl.getParseFrameSet(), null), 
				new Dict("p", (Dict.CM_BLOCK | Dict.CM_OPT), ParserImpl.getParseInline(), null), 
				new Dict("h1", (Dict.CM_BLOCK | Dict.CM_HEADING), ParserImpl.getParseInline(), null), 
				new Dict("h2", (Dict.CM_BLOCK | Dict.CM_HEADING), ParserImpl.getParseInline(), null), 
				new Dict("h3", (Dict.CM_BLOCK | Dict.CM_HEADING), ParserImpl.getParseInline(), null), 
				new Dict("h4", (Dict.CM_BLOCK | Dict.CM_HEADING), ParserImpl.getParseInline(), null), 
				new Dict("h5", (Dict.CM_BLOCK | Dict.CM_HEADING), ParserImpl.getParseInline(), null), 
				new Dict("h6", (Dict.CM_BLOCK | Dict.CM_HEADING), ParserImpl.getParseInline(), null), 
				new Dict("ul", Dict.CM_BLOCK, ParserImpl.getParseList(), null), 
				new Dict("ol", Dict.CM_BLOCK, ParserImpl.getParseList(), null), 
				new Dict("dl", Dict.CM_BLOCK, ParserImpl.getParseDefList(), null), 
				new Dict("dir", (Dict.CM_BLOCK | Dict.CM_OBSOLETE), ParserImpl.getParseList(), null), 
				new Dict("menu", (Dict.CM_BLOCK | Dict.CM_OBSOLETE), ParserImpl.getParseList(), null), 
				new Dict("pre", Dict.CM_BLOCK, ParserImpl.getParsePre(), null), 
				new Dict("listing", (Dict.CM_BLOCK | Dict.CM_OBSOLETE), ParserImpl.getParsePre(), null), 
				new Dict("xmp", (Dict.CM_BLOCK | Dict.CM_OBSOLETE), ParserImpl.getParsePre(), null), 
				new Dict("plaintext", (Dict.CM_BLOCK | Dict.CM_OBSOLETE), ParserImpl.getParsePre(), null), 
				new Dict("address", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("blockquote", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("form", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("isindex", (Dict.CM_BLOCK | Dict.CM_EMPTY), null, null), 
				new Dict("fieldset", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("table", Dict.CM_BLOCK, ParserImpl.getParseTableTag(), CheckAttribsImpl.getCheckTABLE()), 
				new Dict("hr", (Dict.CM_BLOCK | Dict.CM_EMPTY), null, CheckAttribsImpl.getCheckHR()), 
				new Dict("div", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("multicol", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("nosave", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 				 
				new Dict("layer", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("ilayer", Dict.CM_INLINE, ParserImpl.getParseInline(), null), 
				new Dict("nolayer", (Dict.CM_BLOCK | Dict.CM_INLINE | Dict.CM_MIXED), ParserImpl.getParseBlock(), null), 
				new Dict("align", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("center", Dict.CM_BLOCK, ParserImpl.getParseBlock(), null), 
				new Dict("ins", (Dict.CM_INLINE | Dict.CM_BLOCK | Dict.CM_MIXED), ParserImpl.getParseInline(), null), 
				new Dict("del", (Dict.CM_INLINE | Dict.CM_BLOCK | Dict.CM_MIXED), ParserImpl.getParseInline(), null), 
				new Dict("li", (Dict.CM_LIST | Dict.CM_OPT | Dict.CM_NO_INDENT), ParserImpl.getParseBlock(), null), 
				new Dict("dt", (Dict.CM_DEFLIST | Dict.CM_OPT | Dict.CM_NO_INDENT), ParserImpl.getParseInline(), null), 
				new Dict("dd", (Dict.CM_DEFLIST | Dict.CM_OPT | Dict.CM_NO_INDENT), ParserImpl.getParseBlock(), null), new Dict("caption", Dict.CM_TABLE, ParserImpl.getParseInline(), CheckAttribsImpl.getCheckCaption()), new Dict("colgroup", (Dict.CM_TABLE | Dict.CM_OPT), ParserImpl.getParseColGroup(), null), new Dict("col", (Dict.CM_TABLE | Dict.CM_EMPTY), null, null), new Dict("thead", (Dict.CM_TABLE | Dict.CM_ROWGRP | Dict.CM_OPT), ParserImpl.getParseRowGroup(), null), new Dict("tfoot", (Dict.CM_TABLE | Dict.CM_ROWGRP | Dict.CM_OPT), ParserImpl.getParseRowGroup(), null), new Dict("tbody", (Dict.CM_TABLE | Dict.CM_ROWGRP | Dict.CM_OPT), ParserImpl.getParseRowGroup(), null), new Dict("tr", (Dict.CM_TABLE | Dict.CM_OPT), ParserImpl.getParseRow(), null), new Dict("td", (Dict.CM_ROW | Dict.CM_OPT | Dict.CM_NO_INDENT), ParserImpl.getParseBlock(), CheckAttribsImpl.getCheckTableCell()), 
								 new Dict("th", (Dict.CM_ROW | Dict.CM_OPT | Dict.CM_NO_INDENT), ParserImpl.getParseBlock(), CheckAttribsImpl.getCheckTableCell()), new Dict("q", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("a", Dict.CM_INLINE, ParserImpl.getParseInline(), CheckAttribsImpl.getCheckAnchor()), new Dict("br", (Dict.CM_INLINE | Dict.CM_EMPTY), null, null), new Dict("img", (Dict.CM_INLINE | Dict.CM_IMG | Dict.CM_EMPTY), null, CheckAttribsImpl.getCheckIMG()), new Dict("object", (Dict.CM_OBJECT | Dict.CM_HEAD | Dict.CM_IMG | Dict.CM_INLINE | Dict.CM_PARAM), ParserImpl.getParseBlock(), null), new Dict("applet", (Dict.CM_OBJECT | Dict.CM_IMG | Dict.CM_INLINE | Dict.CM_PARAM), ParserImpl.getParseBlock(), null), new Dict("servlet", (Dict.CM_OBJECT | Dict.CM_IMG | Dict.CM_INLINE | Dict.CM_PARAM), ParserImpl.getParseBlock(), null), new Dict("param", (Dict.CM_INLINE | Dict.CM_EMPTY), null, null), new Dict("embed", (Dict.CM_INLINE | Dict.CM_IMG | Dict.CM_EMPTY), null, null), new Dict("noembed", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("iframe", Dict.CM_INLINE, ParserImpl.getParseBlock(), null), new Dict("frame", (Dict.CM_FRAMES | Dict.CM_EMPTY), null, null), new Dict("noframes", (Dict.CM_BLOCK | Dict.CM_FRAMES), ParserImpl.getParseNoFrames(), null), new Dict("noscript", (Dict.CM_BLOCK | Dict.CM_INLINE | Dict.CM_MIXED), ParserImpl.getParseBlock(), null), new Dict("b", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("i", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("u", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("tt", Dict.CM_INLINE, ParserImpl.getParseInline(), null), 
								 new Dict("s", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("strike", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("big", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("small", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("sub", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("sup", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("em", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("strong", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("dfn", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("code", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("samp", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("kbd", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("var", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("cite", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("abbr", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("acronym", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("span", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("blink", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("nobr", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("wbr", (Dict.CM_INLINE | Dict.CM_EMPTY), null, null), new Dict("marquee", (Dict.CM_INLINE | Dict.CM_OPT), ParserImpl.getParseInline(), null), new Dict("bgsound", (Dict.CM_HEAD | Dict.CM_EMPTY), null, null), new Dict("comment", Dict.CM_INLINE, ParserImpl.getParseInline(), null), 
								 new Dict("spacer", (Dict.CM_INLINE | Dict.CM_EMPTY), null, null), new Dict("keygen", (Dict.CM_INLINE | Dict.CM_EMPTY), null, null), new Dict("nolayer", (Dict.CM_BLOCK | Dict.CM_INLINE | Dict.CM_MIXED), ParserImpl.getParseBlock(), null), new Dict("ilayer", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("map", Dict.CM_INLINE, ParserImpl.getParseBlock(), CheckAttribsImpl.getCheckMap()), new Dict("area", (Dict.CM_BLOCK | Dict.CM_EMPTY), null, CheckAttribsImpl.getCheckAREA()), new Dict("input", (Dict.CM_INLINE | Dict.CM_IMG | Dict.CM_EMPTY), null, null), new Dict("select", (Dict.CM_INLINE | Dict.CM_FIELD), ParserImpl.getParseSelect(), null), new Dict("option", (Dict.CM_FIELD | Dict.CM_OPT), ParserImpl.getParseText(), null), new Dict("optgroup", (Dict.CM_FIELD | Dict.CM_OPT), ParserImpl.getParseOptGroup(), null), new Dict("textarea", (Dict.CM_INLINE | Dict.CM_FIELD), ParserImpl.getParseText(), null), new Dict("label", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("legend", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("button", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("basefont", (Dict.CM_INLINE | Dict.CM_EMPTY), null, null), new Dict("font", Dict.CM_INLINE, ParserImpl.getParseInline(), null), new Dict("bdo", Dict.CM_INLINE, ParserImpl.getParseInline(), null),
								 new Dict("o:p", (Dict.CM_INLINE | Dict.CM_NO_INDENT | Dict.CM_NEW), ParserImpl.getParseBlock(), null)
							 };
		}
	}
}