using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Lexer for html parser</summary>
	public class Lexer
	{
		public StreamIn in_Renamed; /* file stream */
		public System.IO.MemoryStream errout; /* error output stream */
		public short badAccess; /* for accessibility errors */
		public short badLayout; /* for bad style errors */
		public short badChars; /* for bad char encodings */
		public short badForm; /* for mismatched/mispositioned form tags */
		public short warnings; /* count of warnings in this document */
		public short errors; /* count of errors */
		public int lines; /* lines seen */
		public int columns; /* at start of current token */
		public bool waswhite; /* used to collapse contiguous white space */
		public bool pushed; /* true after token has been pushed back */
		public bool insertspace; /* when space is moved after end tag */
		public bool excludeBlocks; /* Netscape compatibility */
		public bool exiled; /* true if moved out of table */
		public bool isvoyager; /* true if xmlns attribute on html element */
		public int txtstart; /* start of current node */
		public int txtend; /* end of current node */
		public short state; /* state of lexer's finite state machine */
		public Node token;
		public byte[] lexbuf; /* byte buffer of UTF-8 chars */
		public int lexlength; /* allocated */
		public int lexsize; /* used */
		public Node inode; /* for deferring text node */
		public int insert; /* for inferring inline tags */
		public System.Collections.Stack istack;
		public int istackbase; /* start of frame */
		public Style styles; /* used for cleaning up presentation markup */
		public Configuration configuration;
		protected internal int seenBodyEndTag; /* used by parser */
		private System.Collections.ArrayList nodeList;
		
		public Lexer(StreamIn in_Renamed, Configuration configuration)
		{
			this.in_Renamed = in_Renamed;
			this.lines = 1;
			this.columns = 1;
			this.state = LEX_CONTENT;
			this.badAccess = 0;
			this.badLayout = 0;
			this.badChars = 0;
			this.badForm = 0;
			this.warnings = 0;
			this.errors = 0;
			this.waswhite = false;
			this.pushed = false;
			this.insertspace = false;
			this.exiled = false;
			this.isvoyager = false;
			this.txtstart = 0;
			this.txtend = 0;
			this.token = null;
			this.lexbuf = null;
			this.lexlength = 0;
			this.lexsize = 0;
			this.inode = null;
			this.insert = - 1;
			this.istack = new System.Collections.Stack();
			this.istackbase = 0;
			this.styles = null;
			this.configuration = configuration;
			this.seenBodyEndTag = 0;
			this.nodeList = new System.Collections.ArrayList(10);
		}
		
		public virtual Node newNode()
		{
			Node node = new Node();
			nodeList.Add(node);
			return node;
		}
		
		public virtual Node newNode(short type, byte[] textarray, int start, int end)
		{
			Node node = new Node(type, textarray, start, end);
			nodeList.Add(node);
			return node;
		}
		
		public virtual Node newNode(short type, byte[] textarray, int start, int end, string element)
		{
			Node node = new Node(type, textarray, start, end, element, configuration.tt);
			nodeList.Add(node);
			return node;
		}
		
		public virtual Node cloneNode(Node node)
		{
			Node cnode = (Node) node.Clone();
			nodeList.Add(cnode);
			return cnode;
		}
		
		public virtual AttVal cloneAttributes(AttVal attrs)
		{
			AttVal cattrs = (AttVal) attrs.Clone();
			return cattrs;
		}
		
		protected internal virtual void updateNodeTextArrays(byte[] oldtextarray, byte[] newtextarray)
		{
			Node node;
			for (int i = 0; i < nodeList.Count; i++)
			{
				node = (Node) (nodeList[i]);
				if (node.textarray == oldtextarray)
					node.textarray = newtextarray;
			}
		}
		
		public virtual Node newLineNode()
		{
			Node node = newNode();
			
			node.textarray = this.lexbuf;
			node.start = this.lexsize;
			addCharToLexer((int) '\n');
			node.end = this.lexsize;
			return node;
		}
		
		// Should always be able convert to/from UTF-8, so encoding exceptions are
		// converted to an Error to avoid adding throws declarations in
		// lots of methods.
		
		public static byte[] getBytes(string str)
		{
			try
			{
				return SupportClass.ToByteArray(str);
			}
			catch (System.IO.IOException e)
			{
				throw new System.ApplicationException("string to UTF-8 conversion failed: " + e.Message);
			}
		}
		
		public static string getString(byte[] bytes, int offset, int length)
		{
			try
			{
				System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding(false);
				int charcount = utf8.GetCharCount(bytes, offset, length); // added by Ernie
				char[] utf8Chars = new char[charcount];
				utf8.GetChars(bytes, offset, length, utf8Chars, 0);
				return new string(utf8Chars);
			}
			catch (System.IO.IOException e)
			{
				throw new System.ApplicationException("UTF-8 to string conversion failed: " + e.Message);
			}
		}
		
		public virtual bool endOfInput()
		{
			return this.in_Renamed.isEndOfStream();
		}
		
		public virtual void addByte(int c)
		{
			if (this.lexsize + 1 >= this.lexlength)
			{
				while (this.lexsize + 1 >= this.lexlength)
				{
					if (this.lexlength == 0)
						this.lexlength = 8192;
					else
						this.lexlength = this.lexlength * 2;
				}
				
				byte[] temp = this.lexbuf;
				this.lexbuf = new byte[this.lexlength];
				if (temp != null)
				{
					Array.Copy(temp, 0, this.lexbuf, 0, temp.Length);
					updateNodeTextArrays(temp, this.lexbuf);
				}
			}
			
			this.lexbuf[this.lexsize++] = (byte) c;
		}
		
		public virtual void changeChar(byte c)
		{
			if (this.lexsize > 0)
			{
				this.lexbuf[this.lexsize - 1] = c;
			}
		}
		
		public virtual void addCharToLexer(int c)
		{
			if (c < 128)
				addByte(c);
			else if (c <= 0x7FF)
			{
				addByte(0xC0 | (c >> 6));
				addByte(0x80 | (c & 0x3F));
			}
			else if (c <= 0xFFFF)
			{
				addByte(0xE0 | (c >> 12));
				addByte(0x80 | ((c >> 6) & 0x3F));
				addByte(0x80 | (c & 0x3F));
			}
			else if (c <= 0x1FFFFF)
			{
				addByte(0xF0 | (c >> 18));
				addByte(0x80 | ((c >> 12) & 0x3F));
				addByte(0x80 | ((c >> 6) & 0x3F));
				addByte(0x80 | (c & 0x3F));
			}
			else
			{
				addByte(0xF8 | (c >> 24));
				addByte(0x80 | ((c >> 18) & 0x3F));
				addByte(0x80 | ((c >> 12) & 0x3F));
				addByte(0x80 | ((c >> 6) & 0x3F));
				addByte(0x80 | (c & 0x3F));
			}
		}
		
		public virtual void parseEntity(short mode)
		{
			short map;
			int start;
			bool first = true;
			bool semicolon = false;
			bool numeric = false;
			int c, ch, startcol;
			string str;
			
			start = this.lexsize - 1; /* to start at "&" */
			startcol = this.in_Renamed.curcol - 1;
			
			while (true)
			{
				c = this.in_Renamed.readChar();
				if (c == StreamIn.EndOfStream)
					break;
				if (c == ';')
				{
					semicolon = true;
					break;
				}
				
				if (first && c == '#')
				{
					addCharToLexer(c);
					first = false;
					numeric = true;
					continue;
				}
				
				first = false;
				map = MAP((char) c);
				
				if (numeric && ((c == 'x') || ((map & DIGIT) != 0)))
				{
					addCharToLexer(c);
					continue;
				}
				if (!numeric && ((map & NAMECHAR) != 0))
				{
					addCharToLexer(c);
					continue;
				}
				
				this.in_Renamed.ungetChar(c);
				break;
			}
			
			str = getString(this.lexbuf, start, this.lexsize - start);
			ch = EntityTable.DefaultEntityTable.entityCode(str);
			
			if (ch <= 0)
			{
				this.lines = this.in_Renamed.curline;
				this.columns = startcol;
				
				if (this.lexsize > start + 1)
				{
					Report.entityError(this, Report.UNKNOWN_ENTITY, str, ch);
					
					if (semicolon)
						addCharToLexer(';');
				}
				else
				{
					Report.entityError(this, Report.UNESCAPED_AMPERSAND, str, ch);
				}
			}
			else
			{
				if (c != ';')
				{
					this.lines = this.in_Renamed.curline;
					this.columns = startcol;
					Report.entityError(this, Report.MISSING_SEMICOLON, str, c);
				}
				
				this.lexsize = start;
				
				if (ch == 160 && (mode & Preformatted) != 0)
					ch = ' ';
				
				addCharToLexer(ch);
			}
		}
		
		public virtual char parseTagName()
		{
			short map;
			int c;
			
			c = this.lexbuf[this.txtstart];
			map = MAP((char) c);
			
			if ((map & UPPERCASE) != 0)
			{
				c += (int) ((int) 'a' - (int) 'A');
				this.lexbuf[this.txtstart] = (byte) c;
			}
			
			while (true)
			{
				c = this.in_Renamed.readChar();
				if (c == StreamIn.EndOfStream)
					break;
				map = MAP((char) c);
				
				if ((map & NAMECHAR) == 0)
					break;
				
				if ((map & UPPERCASE) != 0)
					c += (int) ((int) 'a' - (int) 'A');
				
				addCharToLexer(c);
			}
			
			this.txtend = this.lexsize;
			return (char) c;
		}
		
		public virtual void addStringLiteral(System.String str)
		{
			for (int i = 0; i < str.Length; i++)
			{
				addCharToLexer((int) str[i]);
			}
		}

		public virtual bool setXHTMLDocType(Node root)
		{
			Node doctype;
			doctype = root.findDocType();

			if (doctype != null)
				Node.discardElement(doctype);
			return true;
		}
		
		public virtual Node inferredTag(string name)
		{
			Node node;
			
			node = newNode(Node.StartTag, this.lexbuf, this.txtstart, this.txtend, name);
			node.implicit_Renamed = true;
			return node;
		}
		
		public static bool expectsContent(Node node)
		{
			if (node.type != Node.StartTag)
				return false;
			
			if (node.tag == null)
				return true;
			
			if ((node.tag.model & Dict.CM_EMPTY) != 0)
				return false;
			
			return true;
		}
		
		public virtual Node getCDATA(Node container)
		{
			int c, lastc, start, len, i;
			string str;
			bool endtag = false;
			
			this.lines = this.in_Renamed.curline;
			this.columns = this.in_Renamed.curcol;
			this.waswhite = false;
			this.txtstart = this.lexsize;
			this.txtend = this.lexsize;
			
			lastc = (int) '\x0000';
			start = - 1;
			
			while (true)
			{
				c = this.in_Renamed.readChar();
				if (c == StreamIn.EndOfStream)
					break;
				
				if (c == (int) '/' && lastc == (int) '<')
				{
					if (endtag)
					{
						this.lines = this.in_Renamed.curline;
						this.columns = this.in_Renamed.curcol - 3;
						
						Report.warning(this, null, null, Report.BAD_CDATA_CONTENT);
					}
					
					start = this.lexsize + 1; /* to first letter */
					endtag = true;
				}
				else if (c == (int) '>' && start >= 0)
				{
					len = this.lexsize - start;
					if (len == container.element.Length)
					{
						str = getString(this.lexbuf, start, len);
						if (Lexer.wstrcasecmp(str, container.element) == 0)
						{
							this.txtend = start - 2;
							break;
						}
					}
					
					this.lines = this.in_Renamed.curline;
					this.columns = this.in_Renamed.curcol - 3;
					
					Report.warning(this, null, null, Report.BAD_CDATA_CONTENT);
					
					if (ParserImpl.isJavaScript(container))
					{
						for (i = this.lexsize; i > start - 1; --i)
							this.lexbuf[i] = this.lexbuf[i - 1];
						
						this.lexbuf[start - 1] = (byte) '\\';
						this.lexsize++;
					}
					
					start = - 1;
				}
				else if (c == (int) '\r')
				{
					c = this.in_Renamed.readChar();
					
					if (c != (int) '\n')
						this.in_Renamed.ungetChar(c);
					
					c = (int) '\n';
				}
				
				addCharToLexer((int) c);
				this.txtend = this.lexsize;
				lastc = c;
			}
			
			if (c == StreamIn.EndOfStream)
				Report.warning(this, container, null, Report.MISSING_ENDTAG_FOR);
			
			if (this.txtend > this.txtstart)
			{
				this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
				return this.token;
			}
			
			return null;
		}
		
		public virtual void ungetToken()
		{
			this.pushed = true;
		}
		
		public const short IgnoreWhitespace = 0;
		public const short MixedContent = 1;
		public const short Preformatted = 2;
		public const short IgnoreMarkup = 3;
		
		public virtual Node getToken(short mode)
		{
			short map;
			int c = 0;
			int lastc;
			int badcomment = 0;
			MutableBoolean isempty = new MutableBoolean();
			AttVal attributes;
			
			if (this.pushed)
			{
				if (this.token.type != Node.TextNode || (this.insert == - 1 && this.inode == null))
				{
					this.pushed = false;
					return this.token;
				}
			}
			
			if (this.insert != - 1 || this.inode != null)
				return insertedToken();
			
			this.lines = this.in_Renamed.curline;
			this.columns = this.in_Renamed.curcol;
			this.waswhite = false;
			
			this.txtstart = this.lexsize;
			this.txtend = this.lexsize;
			
			while (true)
			{
				c = this.in_Renamed.readChar();
				if (c == StreamIn.EndOfStream)
					break;
				if (this.insertspace && mode != IgnoreWhitespace)
				{
					addCharToLexer(' ');
					this.waswhite = true;
					this.insertspace = false;
				}
				
				if (c == '\r')
				{
					c = this.in_Renamed.readChar();
					
					if (c != '\n')
						this.in_Renamed.ungetChar(c);
					
					c = '\n';
				}
				
				addCharToLexer(c);
				
				switch (this.state)
				{
					case LEX_CONTENT: 
						map = MAP((char) c);
						
						if (((map & WHITE) != 0) && (mode == IgnoreWhitespace) && this.lexsize == this.txtstart + 1)
						{
							--this.lexsize;
							this.waswhite = false;
							this.lines = this.in_Renamed.curline;
							this.columns = this.in_Renamed.curcol;
							continue;
						}
						
						if (c == '<')
						{
							this.state = LEX_GT;
							continue;
						}
						
						if ((map & WHITE) != 0)
						{
							if (this.waswhite)
							{
								if (mode != Preformatted && mode != IgnoreMarkup)
								{
									--this.lexsize;
									this.lines = this.in_Renamed.curline;
									this.columns = this.in_Renamed.curcol;
								}
							}
							else
							{
								this.waswhite = true;
								lastc = c;
								
								if (mode != Preformatted && mode != IgnoreMarkup && c != ' ')
									changeChar((byte) ' ');
							}
							
							continue;
						}
						else if (c == '&' && mode != IgnoreMarkup)
							parseEntity(mode);
						
						if (mode == IgnoreWhitespace)
							mode = MixedContent;
						
						this.waswhite = false;
						continue;
					
					case LEX_GT: 
						if (c == '/')
						{
							c = this.in_Renamed.readChar();
							if (c == StreamIn.EndOfStream)
							{
								this.in_Renamed.ungetChar(c);
								continue;
							}
							
							addCharToLexer(c);
							map = MAP((char) c);
							
							if ((map & LETTER) != 0)
							{
								this.lexsize -= 3;
								this.txtend = this.lexsize;
								this.in_Renamed.ungetChar(c);
								this.state = LEX_ENDTAG;
								this.in_Renamed.curcol -= 2;
								
								if (this.txtend > this.txtstart)
								{
									if (mode == IgnoreWhitespace && this.lexbuf[this.lexsize - 1] == (byte) ' ')
									{
										this.lexsize -= 1;
										this.txtend = this.lexsize;
									}
									
									this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
									return this.token;
								}
								
								continue; /* no text so keep going */
							}
							
							this.waswhite = false;
							this.state = LEX_CONTENT;
							continue;
						}
						
						if (mode == IgnoreMarkup)
						{
							this.waswhite = false;
							this.state = LEX_CONTENT;
							continue;
						}

						if (c == '!')
						{
							c = this.in_Renamed.readChar();
							
							if (c == '-')
							{
								c = this.in_Renamed.readChar();
								
								if (c == '-')
								{
									this.state = LEX_COMMENT; /* comment */
									this.lexsize -= 2;
									this.txtend = this.lexsize;
									
									if (this.txtend > this.txtstart)
									{
										this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
										return this.token;
									}
									
									this.txtstart = this.lexsize;
									continue;
								}
								
								Report.warning(this, null, null, Report.MALFORMED_COMMENT);
							}
							else if (c == 'd' || c == 'D')
							{
								this.state = LEX_DOCTYPE; /* doctype */
								this.lexsize -= 2;
								this.txtend = this.lexsize;
								mode = IgnoreWhitespace;
								
								for (; ; )
								{
									c = this.in_Renamed.readChar();
									
									if (c == StreamIn.EndOfStream || c == '>')
									{
										this.in_Renamed.ungetChar(c);
										break;
									}
									
									map = MAP((char) c);
									
									if ((map & WHITE) == 0)
										continue;
									
									for (; ; )
									{
										c = this.in_Renamed.readChar();
										
										if (c == StreamIn.EndOfStream || c == '>')
										{
											this.in_Renamed.ungetChar(c);
											break;
										}
										
										map = MAP((char) c);
										
										if ((map & WHITE) != 0)
											continue;
										
										this.in_Renamed.ungetChar(c);
										break;
									}
									
									break;
								}
								
								if (this.txtend > this.txtstart)
								{
									this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
									return this.token;
								}
								
								this.txtstart = this.lexsize;
								continue;
							}
							else if (c == '[')
							{
								this.lexsize -= 2;
								this.state = LEX_SECTION;
								this.txtend = this.lexsize;
								
								if (this.txtend > this.txtstart)
								{
									this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
									return this.token;
								}
								
								this.txtstart = this.lexsize;
								continue;
							}
							
							while (true)
							{
								c = this.in_Renamed.readChar();
								if (c == '>')
									break;
								if (c == - 1)
								{
									this.in_Renamed.ungetChar(c);
									break;
								}
							}
							
							this.lexsize -= 2;
							this.state = LEX_CONTENT;
							continue;
						}
						
						if (c == '?')
						{
							this.lexsize -= 2;
							this.state = LEX_PROCINSTR;
							this.txtend = this.lexsize;
							
							if (this.txtend > this.txtstart)
							{
								this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
								return this.token;
							}
							
							this.txtstart = this.lexsize;
							continue;
						}
						
						map = MAP((char) c);
						
						if ((map & LETTER) != 0)
						{
							this.in_Renamed.ungetChar(c); /* push back letter */
							this.lexsize -= 2; /* discard "<" + letter */
							this.txtend = this.lexsize;
							this.state = LEX_STARTTAG; /* ready to read tag name */
							
							if (this.txtend > this.txtstart)
							{
								this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
								return this.token;
							}
							
							continue; /* no text so keep going */
						}
						
						this.state = LEX_CONTENT;
						this.waswhite = false;
						continue;
					
					case LEX_ENDTAG: 
						this.txtstart = this.lexsize - 1;
						this.in_Renamed.curcol += 2;
						c = parseTagName();
						this.token = newNode(Node.EndTag, this.lexbuf, this.txtstart, this.txtend, getString(this.lexbuf, this.txtstart, this.txtend - this.txtstart));
						this.lexsize = this.txtstart;
						this.txtend = this.txtstart;
						
						while (c != '>')
						{
							c = this.in_Renamed.readChar();
							
							if (c == StreamIn.EndOfStream)
								break;
						}
						
						if (c == StreamIn.EndOfStream)
						{
							this.in_Renamed.ungetChar(c);
							continue;
						}
						
						this.state = LEX_CONTENT;
						this.waswhite = false;
						return this.token; /* the endtag token */
					
					case LEX_STARTTAG: 
						this.txtstart = this.lexsize - 1; /* set txtstart to first letter */
						c = parseTagName();
						isempty.value_Renamed = false;
						attributes = null;
						this.token = newNode(Node.StartTag, this.lexbuf, this.txtstart, this.txtend, getString(this.lexbuf, this.txtstart, this.txtend - this.txtstart));
						
						if (c != '>')
						{
							if (c == '/')
								this.in_Renamed.ungetChar(c);
							
							attributes = parseAttrs(isempty);
						}
						
						if (isempty.value_Renamed)						
							this.token.type = (short)Node.StartEndTag;
						
						this.token.attributes = attributes;
						this.lexsize = this.txtstart;
						this.txtend = this.txtstart;
						
						if (expectsContent(this.token) || this.token.tag == configuration.tt.tagBr)
						{
							
							c = this.in_Renamed.readChar();
							
							if (c == '\r')
							{
								c = this.in_Renamed.readChar();
								
								if (c != '\n')
									this.in_Renamed.ungetChar(c);
							}
							else if (c != '\n' && c != '\f')
								this.in_Renamed.ungetChar(c);
							
							this.waswhite = true; /* to swallow leading whitespace */
						}
						else
							this.waswhite = false;
						
						this.state = LEX_CONTENT;
						
						if (this.token.tag == null)
							Report.error(this, null, this.token, Report.UNKNOWN_ELEMENT);
						else
						{
							if (this.token.tag.chkattrs != null)
							{
								this.token.checkUniqueAttributes(this);
								this.token.tag.chkattrs.check(this, this.token);
							}
							else
								this.token.checkAttributes(this);
						}
						
						return this.token; /* return start tag */
					
					case LEX_COMMENT: 
						
						if (c != '-')
							continue;
						
						c = this.in_Renamed.readChar();
						addCharToLexer(c);
						
						if (c != '-')
							continue;
						
						while (true)
						{
							c = this.in_Renamed.readChar();
							
							if (c == '>')
							{
								if (badcomment != 0)
									Report.warning(this, null, null, Report.MALFORMED_COMMENT);
								
								this.txtend = this.lexsize - 2;
								this.state = LEX_CONTENT;
								this.waswhite = false;
								this.token = newNode(Node.CommentTag, this.lexbuf, this.txtstart, this.txtend);
								
								c = this.in_Renamed.readChar();
								
								if (c == '\r')
								{
									c = this.in_Renamed.readChar();
									
									if (c != '\n')
										this.token.linebreak = true;
								}
								
								if (c == '\n')
									this.token.linebreak = true;
								else
									this.in_Renamed.ungetChar(c);
								
								return this.token;
							}
							
							if (badcomment == 0)
							{
								this.lines = this.in_Renamed.curline;
								this.columns = this.in_Renamed.curcol - 3;
							}
							
							badcomment++;
							if (this.configuration.FixComments)
								this.lexbuf[this.lexsize - 2] = (byte) '=';
							
							addCharToLexer(c);
							
							if (c != '-')
							{
								goto end_comment_brk;
							}
						}

						end_comment_brk: ;
						
						this.lexbuf[this.lexsize - 2] = (byte) '=';
						continue;
					
					case LEX_DOCTYPE: 
						map = MAP((char) c);
						
						if ((map & WHITE) != 0)
						{
							if (this.waswhite)
								this.lexsize -= 1;
							
							this.waswhite = true;
						}
						else
							this.waswhite = false;
						
						if (c != '>')
							continue;
						
						this.lexsize -= 1;
						this.txtend = this.lexsize;
						this.state = LEX_CONTENT;
						this.waswhite = false;
						this.token = newNode(Node.DocTypeTag, this.lexbuf, this.txtstart, this.txtend);
						return this.token;
					
					case LEX_PROCINSTR:
						if (c != '>')
							continue;
						
						this.lexsize -= 1;
						this.txtend = this.lexsize;
						this.state = LEX_CONTENT;
						this.waswhite = false;
						this.token = newNode(Node.ProcInsTag, this.lexbuf, this.txtstart, this.txtend);
						return this.token;

					case LEX_SECTION: 
						if (c == '[')
						{
							if (this.lexsize == (this.txtstart + 6) && (getString(this.lexbuf, this.txtstart, 6)).Equals("CDATA["))
							{
								this.state = LEX_CDATA;
								this.lexsize -= 6;
								continue;
							}
						}
						
						if (c != ']')
							continue;
						
						c = this.in_Renamed.readChar();
						
						if (c != '>')
						{
							this.in_Renamed.ungetChar(c);
							continue;
						}
						
						this.lexsize -= 1;
						this.txtend = this.lexsize;
						this.state = LEX_CONTENT;
						this.waswhite = false;
						this.token = newNode(Node.SectionTag, this.lexbuf, this.txtstart, this.txtend);
						return this.token;
					
					case LEX_CDATA: 
						if (c != ']')
							continue;
						
						c = this.in_Renamed.readChar();
						
						if (c != ']')
						{
							this.in_Renamed.ungetChar(c);
							continue;
						}
						
						c = this.in_Renamed.readChar();
						
						if (c != '>')
						{
							this.in_Renamed.ungetChar(c);
							continue;
						}
						
						this.lexsize -= 1;
						this.txtend = this.lexsize;
						this.state = LEX_CONTENT;
						this.waswhite = false;
						this.token = newNode(Node.CDATATag, this.lexbuf, this.txtstart, this.txtend);
						return this.token;
				}
			}
			
			if (this.state == LEX_CONTENT)
			{
				this.txtend = this.lexsize;
				
				if (this.txtend > this.txtstart)
				{
					this.in_Renamed.ungetChar(c);
					
					if (this.lexbuf[this.lexsize - 1] == (byte) ' ')
					{
						this.lexsize -= 1;
						this.txtend = this.lexsize;
					}
					
					this.token = newNode(Node.TextNode, this.lexbuf, this.txtstart, this.txtend);
					return this.token;
				}
			}
			else if (this.state == LEX_COMMENT)
			{
				if (c == StreamIn.EndOfStream)
					Report.warning(this, null, null, Report.MALFORMED_COMMENT);
				
				this.txtend = this.lexsize;
				this.state = LEX_CONTENT;
				this.waswhite = false;
				this.token = newNode(Node.CommentTag, this.lexbuf, this.txtstart, this.txtend);
				return this.token;
			}
			
			return null;
		}
		
		public virtual string parseAttribute(MutableBoolean isempty)
		{
			int start = 0;
			short map;
			string attr;
			int c = 0;
			
			for (; ; )
			{
				c = this.in_Renamed.readChar();
				
				if (c == '/')
				{
					c = this.in_Renamed.readChar();
					
					if (c == '>')
					{
						isempty.value_Renamed = true;
						return null;
					}
					
					this.in_Renamed.ungetChar(c);
					c = '/';
					break;
				}
				
				if (c == '>')
					return null;
				
				if (c == '<')
				{
					c = this.in_Renamed.readChar();			
					this.in_Renamed.ungetChar(c);
					Report.attrError(this, this.token, null, Report.UNEXPECTED_GT);
					return null;
				}
				
				if (c == '"' || c == '\'')
				{
					Report.attrError(this, this.token, null, Report.UNEXPECTED_QUOTEMARK);
					continue;
				}
				
				if (c == StreamIn.EndOfStream)
				{
					Report.attrError(this, this.token, null, Report.UNEXPECTED_END_OF_FILE);
					this.in_Renamed.ungetChar(c);
					return null;
				}
				
				map = MAP((char) c);
				
				if ((map & WHITE) == 0)
					break;
			}
			
			start = this.lexsize;
			
			for (; ; )
			{
				if (c == '=' || c == '>')
				{
					this.in_Renamed.ungetChar(c);
					break;
				}
				
				if (c == '<' || c == StreamIn.EndOfStream)
				{
					this.in_Renamed.ungetChar(c);
					break;
				}
				
				map = MAP((char) c);
				
				if ((map & WHITE) != 0)
					break;
				
				if ((map & UPPERCASE) != 0)
					c += (int) ('a' - 'A');
				
				addCharToLexer(c);
				
				c = this.in_Renamed.readChar();
			}
			
			int len = this.lexsize - start;
			attr = (len > 0?getString(this.lexbuf, start, len):null);
			this.lexsize = start;
			
			return attr;
		}
		
		public virtual int parseServerInstruction()
		{
			int c, map, delim = '"';
			bool isrule = false;
			
			c = this.in_Renamed.readChar();
			addCharToLexer(c);
			
			if (c == '%' || c == '?' || c == '@')
				isrule = true;
			
			for (; ; )
			{
				c = this.in_Renamed.readChar();
				
				if (c == StreamIn.EndOfStream)
					break;
				
				if (c == '>')
				{
					if (isrule)
						addCharToLexer(c);
					else
						this.in_Renamed.ungetChar(c);
					
					break;
				}
				
				if (!isrule)
				{
					map = MAP((char) c);
					
					if ((map & WHITE) != 0)
						break;
				}
				
				addCharToLexer(c);
				
				if (c == '"')
				{
					do 
					{
						c = this.in_Renamed.readChar();
						addCharToLexer(c);
					}
					while (c != '"');
					delim = '\'';
					continue;
				}
				
				if (c == '\'')
				{
					do 
					{
						c = this.in_Renamed.readChar();
						addCharToLexer(c);
					}
					while (c != '\'');
				}
			}
			
			return delim;
		}
		
		public virtual string parseValue(string name, bool foldCase, MutableBoolean isempty, MutableInteger pdelim)
		{
			int len = 0;
			int start;
			short map;
			bool seen_gt = false;
			int c = 0;
			int lastc, delim, quotewarning;
			string value_Renamed;
			
			delim = 0;
			pdelim.value_Renamed = (int) '"';
			
			for (; ; )
			{
				c = this.in_Renamed.readChar();
				
				if (c == StreamIn.EndOfStream)
				{
					this.in_Renamed.ungetChar(c);
					break;
				}
				
				map = MAP((char) c);
				
				if ((map & WHITE) == 0)
					break;
			}
			
			if (c != '=')
			{
				this.in_Renamed.ungetChar(c);
				return null;
			}
			
			for (; ; )
			{
				c = this.in_Renamed.readChar();
				
				if (c == StreamIn.EndOfStream)
				{
					this.in_Renamed.ungetChar(c);
					break;
				}
				
				map = MAP((char) c);
				
				if ((map & WHITE) == 0)
					break;
			}
			
			if (c == '"' || c == '\'')
				delim = c;
			else if (c == '<')
			{
				start = this.lexsize;
				addCharToLexer(c);
				pdelim.value_Renamed = parseServerInstruction();
				len = this.lexsize - start;
				this.lexsize = start;
				return (len > 0?getString(this.lexbuf, start, len):null);
			}
			else
				this.in_Renamed.ungetChar(c);
			
			quotewarning = 0;
			start = this.lexsize;
			c = '\x0000';
			
			for (; ; )
			{
				lastc = c; /* track last character */
				c = this.in_Renamed.readChar();
				
				if (c == StreamIn.EndOfStream)
				{
					Report.attrError(this, this.token, null, Report.UNEXPECTED_END_OF_FILE);
					this.in_Renamed.ungetChar(c);
					break;
				}
				
				if (delim == (char) 0)
				{
					if (c == '>')
					{
						this.in_Renamed.ungetChar(c);
						break;
					}
					
					if (c == '"' || c == '\'')
					{
						Report.attrError(this, this.token, null, Report.UNEXPECTED_QUOTEMARK);
						break;
					}
					
					if (c == '<')
					{
						Report.attrError(this, this.token, null, Report.UNEXPECTED_GT);
					}
					
					if (c == '/')
					{
						c = this.in_Renamed.readChar();
						
						if (c == '>' && !AttributeTable.DefaultAttributeTable.isUrl(name))
						{
							isempty.value_Renamed = true;
							this.in_Renamed.ungetChar(c);
							break;
						}
						
						this.in_Renamed.ungetChar(c);
						c = '/';
					}
				}
				else
				{
					if (c == delim)
						break;
					
					
					if (c == '\r')
					{
						c = this.in_Renamed.readChar();
						if (c != '\n')
							this.in_Renamed.ungetChar(c);
						
						c = '\n';
					}
					
					if (c == '\n' || c == '<' || c == '>')
						++quotewarning;
					
					if (c == '>')
						seen_gt = true;
				}
				
				if (c == '&')
				{
					addCharToLexer(c);
					parseEntity((short) 0);
					continue;
				}
				
				if (c == '\\')
				{
					c = this.in_Renamed.readChar();
					
					if (c != '\n')
					{
						this.in_Renamed.ungetChar(c);
						c = '\\';
					}
				}
				
				map = MAP((char) c);
				
				if ((map & WHITE) != 0)
				{
					if (delim == (char) 0)
						break;
					
					c = ' ';
						
					if (lastc == ' ')
						continue;
				}
				else if (foldCase && (map & UPPERCASE) != 0)
					c += (int) ('a' - 'A');
				
				addCharToLexer(c);
			}
			
			if (quotewarning > 10 && seen_gt)
			{
				if (!AttributeTable.DefaultAttributeTable.isScript(name) && !(AttributeTable.DefaultAttributeTable.isUrl(name) && (getString(this.lexbuf, start, 11)).Equals("javascript:")))
					Report.error(this, null, null, Report.SUSPECTED_MISSING_QUOTE);
			}
			
			len = this.lexsize - start;
			this.lexsize = start;
			
			if (len > 0 || delim != 0)
				value_Renamed = getString(this.lexbuf, start, len);
			else
				value_Renamed = null;
			
			if (delim != 0)
				pdelim.value_Renamed = delim;
			else
				pdelim.value_Renamed = (int) '"';
			
			return value_Renamed;
		}
		
		public static bool isValidAttrName(string attr)
		{
			short map;
			char c;
			int i;
			
			c = attr[0];
			map = MAP(c);
			
			if (!((map & LETTER) != 0))
				return false;
			
			for (i = 1; i < attr.Length; i++)
			{
				c = attr[i];
				map = MAP(c);
				
				if ((map & NAMECHAR) != 0)
					continue;
				
				return false;
			}
			
			return true;
		}
		
		public virtual AttVal parseAttrs(MutableBoolean isempty)
		{
			AttVal av, list;
			string attribute, value_Renamed;
			MutableInteger delim = new MutableInteger();

			list = null;
			
			for (; !endOfInput(); )
			{
				attribute = parseAttribute(isempty);
				
				if ((System.Object) attribute == null)
				{
					break;
				}

				value_Renamed = parseValue(attribute, false, isempty, delim);
				if (attribute.ToLower() == "href")
				{
					if (configuration.StripBaseHref)
					{
						try
						{
							if (value_Renamed.StartsWith("http"))
							{
								Uri uri = new Uri(value_Renamed);
								if (uri.Host == configuration.Domain)
								{
									value_Renamed = uri.AbsolutePath + uri.Query + uri.Fragment;
								}
							}
						}
						catch (ArgumentNullException ane)	// a uri is null, leave value_renamed alone
						{
							Report.tidyPrintln(this.errout, "StripBaseHref Null Error: " + ane.ToString());
							Report.attrError(this, null, "href", Report.BAD_ATTRIBUTE_VALUE);
						}
						catch (UriFormatException ufe)	// a uri is malformed, leave value_renamed alone
						{
							Report.tidyPrintln(this.errout, "StripBaseHref Format Error: " + ufe.ToString());
							Report.attrError(this, null, "href", Report.BAD_ATTRIBUTE_VALUE);
						}
					}
					if (configuration.StripRedundantURLBeforeHash)
					{
						try
						{
							if (value_Renamed.IndexOf("#") > 0)
							{
								Uri inUri, inUriLower, oUri, oUriLower;
								inUri = new Uri(configuration.CurrentURL);
								inUriLower = new Uri((inUri.Scheme + "://" + inUri.Authority + inUri.AbsolutePath).ToLower() + inUri.Query + inUri.Fragment);
								if (value_Renamed.StartsWith("/"))	// if no base href, then pass one in
									oUri = new Uri(inUri.Scheme + "://" + inUri.Authority + value_Renamed);
								else if (value_Renamed.StartsWith("."))	// set to relative from current Uri
									oUri = new Uri(inUri, value_Renamed);
								else
									oUri = new Uri(value_Renamed);
								oUriLower = new Uri((oUri.Scheme + "://" + oUri.Authority + oUri.AbsolutePath).ToLower() + oUri.Query + oUri.Fragment);
								if (oUriLower.Equals(inUriLower) && oUri.Query.Equals(inUri.Query) && oUri.Fragment.Length > 0)
								{
									value_Renamed = oUri.Fragment;
								}
							}
						}
						catch (ArgumentNullException ane)	// a uri is null, leave value_renamed alone
						{
							Report.tidyPrintln(this.errout, "StripRedundantURLBeforeHash Null Error: " + ane.ToString());
							Report.attrError(this, null, "href", Report.BAD_ATTRIBUTE_VALUE);
						}
						catch (UriFormatException ufe)	// a uri is malformed, leave value_renamed alone
						{
							Report.tidyPrintln(this.errout, "StripRedundantURLBeforeHash Format Error: " + ufe.ToString());
							Report.attrError(this, null, "href", Report.BAD_ATTRIBUTE_VALUE);
						}
					}
				}
				
				if ((System.Object) attribute != null && isValidAttrName(attribute))
				{
					av = new AttVal(list, null, delim.value_Renamed, attribute, value_Renamed);
					av.dict = AttributeTable.DefaultAttributeTable.findAttribute(av);
					list = av;
				}
				else
				{
					av = new AttVal(null, null, 0, attribute, value_Renamed);
					Report.attrError(this, this.token, value_Renamed, Report.BAD_ATTRIBUTE_VALUE);
				}
			}
			
			return list;
		}
		
		public virtual void pushInline(Node node)
		{
			IStack is_Renamed;
			
			if (node.implicit_Renamed)
				return ;
			
			if (node.tag == null)
				return ;
			
			if ((node.tag.model & Dict.CM_INLINE) == 0)
				return ;
			
			if ((node.tag.model & Dict.CM_OBJECT) != 0)
				return ;
			
			if (node.tag != configuration.tt.tagFont && isPushed(node))
				return ;
			
			// make sure there is enough space for the stack
			is_Renamed = new IStack();
			is_Renamed.tag = node.tag;
			is_Renamed.element = node.element;
			if (node.attributes != null)
				is_Renamed.attributes = cloneAttributes(node.attributes);
			SupportClass.StackPush(this.istack, is_Renamed);
		}
		
		public virtual void popInline(Node node)
		{
			IStack is_Renamed;
			
			if (node != null)
			{
				
				if (node.tag == null)
					return ;
				
				if ((node.tag.model & Dict.CM_INLINE) == 0)
					return ;
				
				if ((node.tag.model & Dict.CM_OBJECT) != 0)
					return ;
				
				// if node is </a> then pop until we find an <a>
				if (node.tag == configuration.tt.tagA)
				{
					
					while (this.istack.Count > 0)
					{
						is_Renamed = (IStack) this.istack.Pop();
						if (is_Renamed.tag == configuration.tt.tagA)
						{
							break;
						}
					}
					
					if (this.insert >= this.istack.Count)
						this.insert = - 1;
					return ;
				}
			}
			
			if (this.istack.Count > 0)
			{
				is_Renamed = (IStack) this.istack.Pop();
				if (this.insert >= this.istack.Count)
					this.insert = - 1;
			}
		}
		
		public virtual bool isPushed(Node node)
		{
			int i;
			IStack is_Renamed;
			
			for (i = this.istack.Count - 1; i >= 0; --i)
			{
				is_Renamed = (IStack) (this.istack.ToArray())[this.istack.Count - (i + 1)];
				if (is_Renamed.tag == node.tag)
					return true;
			}
			
			return false;
		}
		
		public virtual int inlineDup(Node node)
		{
			int n;
			
			n = this.istack.Count - this.istackbase;
			if (n > 0)
			{
				this.insert = this.istackbase;
				this.inode = node;
			}
			
			return n;
		}
		
		public virtual Node insertedToken()
		{
			Node node;
			IStack is_Renamed;
			int n;
			
			// this will only be null if inode != null
			if (this.insert == - 1)
			{
				node = this.inode;
				this.inode = null;
				return node;
			}
			
			// is this is the "latest" node then update
			// the position, otherwise use current values
			if (this.inode == null)
			{
				this.lines = this.in_Renamed.curline;
				this.columns = this.in_Renamed.curcol;
			}
			
			node = newNode(Node.StartTag, this.lexbuf, this.txtstart, this.txtend);
			node.implicit_Renamed = true;
			is_Renamed = (IStack) (this.istack.ToArray())[this.istack.Count - (this.insert + 1)];
			node.element = is_Renamed.element;
			node.tag = is_Renamed.tag;
			if (is_Renamed.attributes != null)
				node.attributes = cloneAttributes(is_Renamed.attributes);
			
			// advance lexer to next item on the stack
			n = this.insert;
			
			// and recover state if we have reached the end
			if (++n < this.istack.Count)
			{
				this.insert = n;
			}
			else
			{
				this.insert = - 1;
			}
			
			return node;
		}
		
		public static int wstrcasecmp(string s1, string s2)
		{
			return (s1.ToUpper().Equals(s2.ToUpper())?0:1);
		}
		
		public static bool wsubstr(string s1, string s2)
		{
			int i;
			int len1 = s1.Length;
			int len2 = s2.Length;
			
			for (i = 0; i <= len1 - len2; ++i)
			{
				if (s2.ToUpper().Equals(s1.Substring(i).ToUpper()))
					return true;
			}
			
			return false;
		}
		
		public virtual bool canPrune(Node element)
		{
			if (element.type == Node.TextNode)
				return true;
			
			if (element.content != null)
				return false;
			
			if (element.tag == configuration.tt.tagA && element.attributes != null)
				return false;
			
			if (element.tag == null)
				return false;
			
			if ((element.tag.model & Dict.CM_ROW) != 0)
				return false;
			
			if (element.tag == configuration.tt.tagApplet)
				return false;
			
			if (element.tag == configuration.tt.tagObject)
				return false;

			if (element.tag == configuration.tt.tagIframe)
				return false;
			
			if (element.attributes != null && (element.getAttrByName("id") != null || element.getAttrByName("name") != null))
				return false;
			
			return true;
		}
		
		public virtual void fixId(Node node)
		{
			AttVal name = node.getAttrByName("name");
			AttVal id = node.getAttrByName("id");
			
			if (name != null)
			{
				if (id != null)
				{
					if (!id.value_Renamed.Equals(name.value_Renamed))
						Report.attrError(this, node, "name", Report.ID_NAME_MISMATCH);
				}
				else
					node.addAttribute("id", name.value_Renamed);
			}
		}
		
		public virtual void deferDup()
		{
			this.insert = - 1;
			this.inode = null;
		}
		
		private const short DIGIT = 1;
		private const short LETTER = 2;
		private const short NAMECHAR = 4;
		private const short WHITE = 8;
		private const short NEWLINE = 16;
		private const short LOWERCASE = 32;
		private const short UPPERCASE = 64;
		private const short LEX_CONTENT = 0;
		private const short LEX_GT = 1;
		private const short LEX_ENDTAG = 2;
		private const short LEX_STARTTAG = 3;
		private const short LEX_COMMENT = 4;
		private const short LEX_DOCTYPE = 5;
		private const short LEX_PROCINSTR = 6;
		private const short LEX_ENDCOMMENT = 7;
		private const short LEX_CDATA = 8;
		private const short LEX_SECTION = 9;
		private const short LEX_ASP = 10;
		private const short LEX_JSTE = 11;
		private const short LEX_PHP = 12;
		private static short[] lexmap;
		
		private static void mapStr(string str, short code)
		{
			int j;
			
			for (int i = 0; i < str.Length; i++)
			{
				j = (int) str[i];
				lexmap[j] = (short)((ushort)lexmap[j] | (ushort)code);
			}
		}
		
		private static short MAP(char c)
		{
			return (short)(((int) c < 128) ? (int) lexmap[(int) c] : 0);
		}

		public static char foldCase(char c)
		{
			short m;

			m = MAP(c);
			if ((m & UPPERCASE) != 0)
				c = (char) ((int) c + (int) 'a' - (int) 'A');
			return c;
		}
		
		static Lexer()
		{
			lexmap = new short[128];
		{
			mapStr("\r\n\f", (short) (NEWLINE | WHITE));
			mapStr(" \t", WHITE);
			mapStr("-.:_", NAMECHAR);
			mapStr("0123456789", (short) (DIGIT | NAMECHAR));
			mapStr("abcdefghijklmnopqrstuvwxyz", (short) (LOWERCASE | LETTER | NAMECHAR));
			mapStr("ABCDEFGHIJKLMNOPQRSTUVWXYZ", (short) (UPPERCASE | LETTER | NAMECHAR));
		}
		}
	}
}