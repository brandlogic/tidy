using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Output Stream Implementation</summary>
	public class OutImpl:Out
	{
		public OutImpl()
		{
			this.outStream = null;
		}
		
		public override void outc(int c)
		{
			int ch;
			
			try
			{
				if (c < 128)
				{
					this.outStream.WriteByte((byte)c);
				}
				else if (c <= 0x7FF)
				{
					ch = (0xC0 | (c >> 6)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | (c & 0x3F)); this.outStream.WriteByte((byte)ch);
				}
				else if (c <= 0xFFFF)
				{
					ch = (0xE0 | (c >> 12)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | ((c >> 6) & 0x3F)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | (c & 0x3F)); this.outStream.WriteByte((byte)ch);
				}
				else if (c <= 0x1FFFFF)
				{
					ch = (0xF0 | (c >> 18)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | ((c >> 12) & 0x3F)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | ((c >> 6) & 0x3F)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | (c & 0x3F)); this.outStream.WriteByte((byte)ch);
				}
				else
				{
					ch = (0xF8 | (c >> 24)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | ((c >> 18) & 0x3F)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | ((c >> 12) & 0x3F)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | ((c >> 6) & 0x3F)); this.outStream.WriteByte((byte)ch);
					ch = (0x80 | (c & 0x3F)); this.outStream.WriteByte((byte)ch);
				}
			}
			catch (System.IO.IOException e)
			{
				System.Console.Error.WriteLine("OutImpl.outc: " + e.ToString());
			}
		}
		
		public override void  newline()
		{
			try
			{
				byte[] temp_byteArray;
				temp_byteArray = SupportClass.ToByteArray(System.Environment.NewLine);
				this.outStream.Write(temp_byteArray, 0, temp_byteArray.Length);
			}
			catch (System.IO.IOException e)
			{
				System.Console.Error.WriteLine("OutImpl.newline: " + e.ToString());
			}
		}		
	}
}