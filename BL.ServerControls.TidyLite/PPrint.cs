/// <summary> 
/// Pretty print parse tree
/// </summary>

/*
Block-level and unknown elements are printed on
new lines and their contents indented 2 spaces

Inline elements are printed inline.

Inline content is wrapped on spaces (except in
attribute values or preformatted text, after
start tags and before end tags*/
using System;
namespace BL.ServerControls.TidyLite
{
	public class PPrint
	{
		private const short NORMAL = 0;
		private const short PREFORMATTED = 1;
		private const short COMMENT = 2;
		private const short ATTRIBVALUE = 4;
		private const short NOWRAP = 8;
		private const short CDATA = 16;
		
		private int[] linebuf = null;
		private int lbufsize = 0;
		private int linelen = 0;
		private int wraphere = 0;
		private bool inAttVal = false;
		private bool InString = false;
		
		private Configuration configuration;
		
		public PPrint(Configuration configuration)
		{
			this.configuration = configuration;
		}

		public static int getUTF8(byte[] str, int start, MutableInteger ch)
		{
			int c, n, i, bytes;
			
			c = ((int) str[start]) & 0xFF; // Convert to unsigned.
			
			if ((c & 0xE0) == 0xC0)
				/* 110X XXXX  two bytes */
			{
				n = c & 31;
				bytes = 2;
			}
			else if ((c & 0xF0) == 0xE0)
				/* 1110 XXXX  three bytes */
			{
				n = c & 15;
				bytes = 3;
			}
			else if ((c & 0xF8) == 0xF0)
				/* 1111 0XXX  four bytes */
			{
				n = c & 7;
				bytes = 4;
			}
			else if ((c & 0xFC) == 0xF8)
				/* 1111 10XX  five bytes */
			{
				n = c & 3;
				bytes = 5;
			}
			else if ((c & 0xFE) == 0xFC)
				/* 1111 110X  six bytes */
			{
				n = c & 1;
				bytes = 6;
			}
				/* 0XXX XXXX one byte */
			else
			{
				ch.value_Renamed = c;
				return 0;
			}
			
			/* successor bytes should have the form 10XX XXXX */
			for (i = 1; i < bytes; ++i)
			{
				c = ((int) str[start + i]) & 0xFF; // Convert to unsigned.
				n = (n << 6) | (c & 0x3F);
			}
			
			ch.value_Renamed = n;
			return bytes - 1;
		}
		
		/* store char c as UTF-8 encoded byte stream */
		public static int putUTF8(byte[] buf, int start, int c)
		{
			if (c < 128)
				buf[start++] = (byte) c;
			else if (c <= 0x7FF)
			{
				buf[start++] = (byte) (0xC0 | (c >> 6));
				buf[start++] = (byte) (0x80 | (c & 0x3F));
			}
			else if (c <= 0xFFFF)
			{
				buf[start++] = (byte) (0xE0 | (c >> 12));
				buf[start++] = (byte) (0x80 | ((c >> 6) & 0x3F));
				buf[start++] = (byte) (0x80 | (c & 0x3F));
			}
			else if (c <= 0x1FFFFF)
			{
				buf[start++] = (byte) (0xF0 | (c >> 18));
				buf[start++] = (byte) (0x80 | ((c >> 12) & 0x3F));
				buf[start++] = (byte) (0x80 | ((c >> 6) & 0x3F));
				buf[start++] = (byte) (0x80 | (c & 0x3F));
			}
			else
			{
				buf[start++] = (byte) (0xF8 | (c >> 24));
				buf[start++] = (byte) (0x80 | ((c >> 18) & 0x3F));
				buf[start++] = (byte) (0x80 | ((c >> 12) & 0x3F));
				buf[start++] = (byte) (0x80 | ((c >> 6) & 0x3F));
				buf[start++] = (byte) (0x80 | (c & 0x3F));
			}
			
			return start;
		}
		
		private void addC(int c, int index)
		{
			if (index + 1 >= lbufsize)
			{
				while (index + 1 >= lbufsize)
				{
					if (lbufsize == 0)
						lbufsize = 256;
					else
						lbufsize = lbufsize * 2;
				}
				
				int[] temp = new int[lbufsize];
				if (linebuf != null)
					Array.Copy((System.Array) linebuf, 0, (System.Array) temp, 0, index);
				linebuf = temp;
			}
			
			linebuf[index] = c;
		}
		
		private void wrapLine(Out fout, int indent)
		{
			int i, p, q;
			
			if (wraphere == 0)
				return ;
			
			for (i = 0; i < indent; ++i)
				fout.outc((int) ' ');
			
			for (i = 0; i < wraphere; ++i)
				fout.outc(linebuf[i]);
			
			if (InString)
			{
				fout.outc((int) ' ');
				fout.outc((int) '\\');
			}
			
			fout.newline();
			
			if (linelen > wraphere)
			{
				p = 0;
				
				if (linebuf[wraphere] == ' ')
					++wraphere;
				
				q = wraphere;
				addC('\x0000', linelen);
				
				while (true)
				{
					linebuf[p] = linebuf[q];
					if (linebuf[q] == 0)
						break;
					p++;
					q++;
				}
				linelen -= wraphere;
			}
			else
				linelen = 0;
			
			wraphere = 0;
		}
		
		private void wrapAttrVal(Out fout, int indent, bool inString)
		{
			int i, p, q;
			
			for (i = 0; i < indent; ++i)
				fout.outc((int) ' ');
			
			for (i = 0; i < wraphere; ++i)
				fout.outc(linebuf[i]);
			
			fout.outc((int) ' ');
			
			if (inString)
				fout.outc((int) '\\');
			
			fout.newline();
			
			if (linelen > wraphere)
			{
				p = 0;
				
				if (linebuf[wraphere] == ' ')
					++wraphere;
				
				q = wraphere;
				addC('\x0000', linelen);
				
				while (true)
				{
					linebuf[p] = linebuf[q];
					if (linebuf[q] == 0)
						break;
					p++;
					q++;
				}
				linelen -= wraphere;
			}
			else
				linelen = 0;
			
			wraphere = 0;
		}
		
		public virtual void flushLine(Out fout, int indent)
		{
			int i;
			
			if (linelen > 0)
			{
				if (indent + linelen >= this.configuration.wraplen)
					wrapLine(fout, indent);
				
				if (!inAttVal)
				{
					for (i = 0; i < indent; ++i)
						fout.outc((int) ' ');
				}
				
				for (i = 0; i < linelen; ++i)
					fout.outc(linebuf[i]);
			}
			
			fout.newline();
			linelen = 0;
			wraphere = 0;
			inAttVal = false;
		}
		
		public virtual void condFlushLine(Out fout, int indent)
		{
			int i;
			
			if (linelen > 0)
			{
				if (indent + linelen >= this.configuration.wraplen)
					wrapLine(fout, indent);
				
				if (!inAttVal)
				{
					for (i = 0; i < indent; ++i)
						fout.outc((int) ' ');
				}
				
				for (i = 0; i < linelen; ++i)
					fout.outc(linebuf[i]);
				
				fout.newline();
				linelen = 0;
				wraphere = 0;
				inAttVal = false;
			}
		}
		
		private void printChar(int c, short mode)
		{
			if (c == ' ' && !((mode & (PREFORMATTED | COMMENT | ATTRIBVALUE)) != 0))
			{
				/* coerce a space character to a non-breaking space */
				if ((mode & NOWRAP) != 0)
				{
					addC('&', linelen++);
					addC('#', linelen++);
					addC('1', linelen++);
					addC('6', linelen++);
					addC('0', linelen++);
					addC(';', linelen++);
					return;
				}
				else
					wraphere = linelen;
			}
			
			/* comment characters are passed raw */
			if ((mode & COMMENT) != 0)
			{
				addC(c, linelen++);
				return ;
			}
			
			/* except in CDATA map < to &lt; etc. */
			if (!((mode & CDATA) != 0))
			{
				if (c == '<')
				{
					addC('&', linelen++);
					addC('l', linelen++);
					addC('t', linelen++);
					addC(';', linelen++);
					return ;
				}
				
				if (c == '>')
				{
					addC('&', linelen++);
					addC('g', linelen++);
					addC('t', linelen++);
					addC(';', linelen++);
					return ;
				}
				
				/*
				naked '&' chars can be left alone or
				quoted as &amp; The latter is required
				for XML where naked '&' are illegal.
				*/
				if (c == '&')
				{
					addC('&', linelen++);
					addC('a', linelen++);
					addC('m', linelen++);
					addC('p', linelen++);
					addC(';', linelen++);
					return ;
				}
				
				if (c == '"' && this.configuration.QuoteMarks)
				{
					addC('&', linelen++);
					addC('q', linelen++);
					addC('u', linelen++);
					addC('o', linelen++);
					addC('t', linelen++);
					addC(';', linelen++);
					return ;
				}
				
				if (c == '\'' && this.configuration.QuoteMarks)
				{
					addC('&', linelen++);
					addC('#', linelen++);
					addC('3', linelen++);
					addC('9', linelen++);
					addC(';', linelen++);
					return ;
				}
				
				if (c == 160)
				{
					addC('&', linelen++);
					addC('#', linelen++);
					addC('1', linelen++);
					addC('6', linelen++);
					addC('0', linelen++);
					addC(';', linelen++);
					return ;
				}
			}

			/* if preformatted text, map &nbsp; to space */
			if (c == 160 && ((mode & PREFORMATTED) != 0))
			{
				addC(' ', linelen++);
				return ;
			}

			addC(c, linelen++);
			return;
		}
		
		/* 
		The line buffer is uint not char so we can
		hold Unicode values unencoded. The translation
		to UTF-8 is deferred to the outc routine called
		to flush the line buffer.
		*/
		private void printText(Out fout, short mode, int indent, byte[] textarray, int start, int end)
		{
			int i, c;
			MutableInteger ci = new MutableInteger();
			
			for (i = start; i < end; ++i)
			{
				if (indent + linelen >= this.configuration.wraplen)
					wrapLine(fout, indent);
				
				c = ((int) textarray[i]) & 0xFF; // Convert to unsigned.
				
				/* look for UTF-8 multibyte character */
				if (c > 0x7F)
				{
					i += getUTF8(textarray, i, ci);
					c = ci.value_Renamed;
				}
				
				if (c == '\n')
				{
					flushLine(fout, indent);
					continue;
				}
				
				printChar(c, mode);
			}
		}
		
		private void printString(Out fout, int indent, string str)
		{
			for (int i = 0; i < str.Length; i++)
				addC((int) str[i], linelen++);
		}
		
		private void  printAttrValue(Out fout, int indent, string value_Renamed, int delim, bool wrappable)
		{
			int c;
			MutableInteger ci = new MutableInteger();
			bool wasinstring = false;
			byte[] valueChars = null;
			int i;
			short mode = (wrappable ? (short)(NORMAL | ATTRIBVALUE):(short) (PREFORMATTED | ATTRIBVALUE));
			
			if ((System.Object) value_Renamed != null)
			{
				valueChars = Lexer.getBytes(value_Renamed);
			}
			
			/* look for ASP, Tango or PHP instructions for computed attribute value */
			if (valueChars != null && valueChars.Length >= 5 && valueChars[0] == '<')
			{
				char[] tmpChar;
				tmpChar = new char[valueChars.Length];
				valueChars.CopyTo(tmpChar, 0);
				if (valueChars[1] == '%' || valueChars[1] == '@' || (new string(tmpChar, 0, 5)).Equals("<?php"))
					mode = (short)((ushort)mode | (ushort)CDATA);
			}
			
			if (delim == 0)
				delim = '"';
			
			addC('=', linelen++);
			addC(delim, linelen++);
			
			if ((System.Object) value_Renamed != null)
			{
				InString = false;
				
				i = 0;
				while (i < valueChars.Length)
				{
					c = ((int) valueChars[i]) & 0xFF; // Convert to unsigned.
					
					if (wrappable && c == ' ' && indent + linelen < this.configuration.wraplen)
					{
						wraphere = linelen;
						wasinstring = InString;
					}
					
					if (wrappable && wraphere > 0 && indent + linelen >= this.configuration.wraplen)
						wrapAttrVal(fout, indent, wasinstring);
					
					if (c == delim)
					{
						System.String entity;
						
						entity = (c == '"'?"&quot;":"&#39;");
						
						for (int j = 0; j < entity.Length; j++)
							addC(entity[j], linelen++);
						
						++i;
						continue;
					}
					else if (c == '"')
					{
						if (this.configuration.QuoteMarks)
						{
							addC('&', linelen++);
							addC('q', linelen++);
							addC('u', linelen++);
							addC('o', linelen++);
							addC('t', linelen++);
							addC(';', linelen++);
						}
						else
							addC('"', linelen++);
						
						if (delim == '\'')
							InString = !InString;
						
						++i;
						continue;
					}
					else if (c == '\'')
					{
						if (this.configuration.QuoteMarks)
						{
							addC('&', linelen++);
							addC('#', linelen++);
							addC('3', linelen++);
							addC('9', linelen++);
							addC(';', linelen++);
						}
						else
							addC('\'', linelen++);
						
						if (delim == '"')
							InString = !InString;
						
						++i;
						continue;
					}
					
					/* look for UTF-8 multibyte character */
					if (c > 0x7F)
					{
						i += getUTF8(valueChars, i, ci);
						c = ci.value_Renamed;
					}
					
					++i;
					
					if (c == '\n')
					{
						flushLine(fout, indent);
						continue;
					}
					
					printChar(c, mode);
				}
			}
			
			InString = false;
			addC(delim, linelen++);
		}
		
		private void printAttribute(Out fout, int indent, Node node, AttVal attr)
		{
			string name;
			bool wrappable = false;
			
			name = attr.attribute;
			
			if (indent + linelen >= this.configuration.wraplen)
				wrapLine(fout, indent);

			if (indent + linelen < this.configuration.wraplen)
			{
				wraphere = linelen;
				addC(' ', linelen++);
			}
			else
			{
				condFlushLine(fout, indent);
				addC(' ', linelen++);
			}
			
			for (int i = 0; i < name.Length; i++)
				addC((int) Lexer.foldCase(name[i]), linelen++);
			
			if (indent + linelen >= this.configuration.wraplen)
				wrapLine(fout, indent);
			
			if ((System.Object) attr.value_Renamed == null)
			{
				printAttrValue(fout, indent, attr.attribute, attr.delim, true);
			}
			else
				printAttrValue(fout, indent, attr.value_Renamed, attr.delim, wrappable);
		}
		
		private void printAttrs(Out fout, int indent, Node node, AttVal attr)
		{
			if (attr != null)
			{
				if (attr.next != null)
					printAttrs(fout, indent, node, attr.next);
				
				if ((System.Object) attr.attribute != null)
					printAttribute(fout, indent, node, attr);
			}
		}
		
		/*
		Line can be wrapped immediately after inline start tag provided
		if follows a text node ending in a space, or it parent is an
		inline element that that rule applies to. This behaviour was
		reverse engineered from Netscape 3.0
		*/
		private static bool afterSpace(Node node)
		{
			Node prev;
			int c;
			
			if (node == null || node.tag == null || !((node.tag.model & Dict.CM_INLINE) != 0))
				return true;
			
			prev = node.prev;
			
			if (prev != null)
			{
				if (prev.type == Node.TextNode && prev.end > prev.start)
				{
					c = ((int) prev.textarray[prev.end - 1]) & 0xFF; // Convert to unsigned.
					
					if (c == 160 || c == ' ' || c == '\n')
						return true;
				}
				
				return false;
			}
			
			return afterSpace(node.parent);
		}
		
		private void printTag(Lexer lexer, Out fout, short mode, int indent, Node node)
		{
			string p;
			TagTable tt = this.configuration.tt;
			
			addC('<', linelen++);
			
			if (node.type == Node.EndTag)
				addC('/', linelen++);
			
			p = node.element;
			for (int i = 0; i < p.Length; i++)
				addC((int) Lexer.foldCase(p[i]), linelen++);
			
			printAttrs(fout, indent, node, node.attributes);
			
			if ((node.type == Node.StartEndTag || (node.tag.model & Dict.CM_EMPTY) != 0))
			{
				addC(' ', linelen++); /* compatibility hack */
				addC('/', linelen++);
			}
			
			addC('>', linelen++);
			
			if (node.type != Node.StartEndTag && !((mode & PREFORMATTED) != 0))
			{
				if (indent + linelen >= this.configuration.wraplen)
					wrapLine(fout, indent);
				
				if (indent + linelen < this.configuration.wraplen)
				{
					/*
					wrap after start tag if is <br/> or if it's not
					inline or it is an empty tag followed by </a>
					*/
					if (afterSpace(node))
					{
						if (!((mode & NOWRAP) != 0) && (!((node.tag.model & Dict.CM_INLINE) != 0) || (node.tag == tt.tagBr) || (((node.tag.model & Dict.CM_EMPTY) != 0) && node.next == null && node.parent.tag == tt.tagA)))
						{
							wraphere = linelen;
						}
					}
				}
				else
					condFlushLine(fout, indent);
			}
		}
		
		private void  printEndTag(Out fout, short mode, int indent, Node node)
		{
			string p;
			
			addC('<', linelen++);
			addC('/', linelen++);
			
			p = node.element;
			for (int i = 0; i < p.Length; i++)
				addC((int) Lexer.foldCase(p[i]), linelen++);
			
			addC('>', linelen++);
		}
		
		private void printComment(Out fout, int indent, Node node)
		{
			if (indent + linelen < this.configuration.wraplen)
				wraphere = linelen;
			
			addC('<', linelen++);
			addC('!', linelen++);
			addC('-', linelen++);
			addC('-', linelen++);

			printText(fout, COMMENT, indent, node.textarray, node.start, node.end);
			
			addC('-', linelen++);
			addC('-', linelen++);
			addC('>', linelen++);
			
			if (node.linebreak)
				flushLine(fout, indent);
		}
		
		private void printDocType(Out fout, int indent, Node node)
		{
			bool q = this.configuration.QuoteMarks;
			
			this.configuration.QuoteMarks = false;
			
			if (indent + linelen < this.configuration.wraplen)
				wraphere = linelen;
			
			condFlushLine(fout, indent);
			
			addC('<', linelen++);
			addC('!', linelen++);
			addC('D', linelen++);
			addC('O', linelen++);
			addC('C', linelen++);
			addC('T', linelen++);
			addC('Y', linelen++);
			addC('P', linelen++);
			addC('E', linelen++);
			addC(' ', linelen++);
			
			if (indent + linelen < this.configuration.wraplen)
				wraphere = linelen;
			
			printText(fout, 0, indent, node.textarray, node.start, node.end);
			
			if (linelen < this.configuration.wraplen)
				wraphere = linelen;
			
			addC('>', linelen++);
			this.configuration.QuoteMarks = q;
			condFlushLine(fout, indent);
		}
		
		private void printPI(Out fout, int indent, Node node)
		{
			if (indent + linelen < this.configuration.wraplen)
				wraphere = linelen;
			
			addC('<', linelen++);
			addC('?', linelen++);
			
			/* set CDATA to pass < and > unescaped */
			printText(fout, CDATA, indent, node.textarray, node.start, node.end);
			
			if (node.textarray[node.end - 1] != (byte) '?')
				addC('?', linelen++);
			
			addC('>', linelen++);
			condFlushLine(fout, indent);
		}
		
		private void printCDATA(Out fout, int indent, Node node)
		{
			int savewraplen = this.configuration.wraplen;
			
			condFlushLine(fout, indent);
			
			/* disable wrapping */
			
			this.configuration.wraplen = 0xFFFFFF; /* a very large number */
			
			addC('<', linelen++);
			addC('!', linelen++);
			addC('[', linelen++);
			addC('C', linelen++);
			addC('D', linelen++);
			addC('A', linelen++);
			addC('T', linelen++);
			addC('A', linelen++);
			addC('[', linelen++);
			
			printText(fout, COMMENT, indent, node.textarray, node.start, node.end);
			
			addC(']', linelen++);
			addC(']', linelen++);
			addC('>', linelen++);
			condFlushLine(fout, indent);
			this.configuration.wraplen = savewraplen;
		}
		
		private void printSection(Out fout, int indent, Node node)
		{
			int savewraplen = this.configuration.wraplen;
			
			addC('<', linelen++);
			addC('!', linelen++);
			addC('[', linelen++);
			
			printText(fout, CDATA, indent, node.textarray, node.start, node.end);
			
			addC(']', linelen++);
			addC('>', linelen++);
			this.configuration.wraplen = savewraplen;
		}
		
		private bool shouldIndent(Node node)
		{
			TagTable tt = this.configuration.tt;
			
			if (!this.configuration.IndentContent)
				return false;
			
			if ((node.tag.model & (Dict.CM_FIELD | Dict.CM_OBJECT)) != 0)
				return true;
			
			if (node.tag == tt.tagMap)
				return true;
			
			return !((node.tag.model & Dict.CM_INLINE) != 0);
		}
		
		public virtual void printTree(Out fout, short mode, int indent, Lexer lexer, Node node)
		{
			Node content, last;
			TagTable tt = this.configuration.tt;
			
			if (node == null)
				return ;
			
			if (node.type == Node.TextNode)
				printText(fout, mode, indent, node.textarray, node.start, node.end);
			else if (node.type == Node.CommentTag)
			{
				printComment(fout, indent, node);
			}
			else if (node.type == Node.RootNode)
			{
				for (content = node.content; content != null; content = content.next)
					printTree(fout, mode, indent, lexer, content);
			}
			else if (node.type == Node.DocTypeTag)
				printDocType(fout, indent, node);
			else if (node.type == Node.ProcInsTag)
				printPI(fout, indent, node);
			else if (node.type == Node.CDATATag)
				printCDATA(fout, indent, node);
			else if (node.type == Node.SectionTag)
				printSection(fout, indent, node);
			else if ((node.tag.model & Dict.CM_EMPTY) != 0 || node.type == Node.StartEndTag)
			{
				if (!((node.tag.model & Dict.CM_INLINE) != 0))
					condFlushLine(fout, indent);
				
				printTag(lexer, fout, mode, indent, node);
				
				if (node.tag == tt.tagParam || node.tag == tt.tagArea)
					condFlushLine(fout, indent);
				else if (node.tag == tt.tagBr || node.tag == tt.tagHr)
					flushLine(fout, indent);
			}
			/* some kind of container element */
			else
			{
				if (node.tag != null && node.tag.parser == ParserImpl.getParsePre())
				{
					condFlushLine(fout, indent);
					
					indent = 0;
					condFlushLine(fout, indent);
					printTag(lexer, fout, mode, indent, node);
					flushLine(fout, indent);
					
					for (content = node.content; content != null; content = content.next)
						printTree(fout, (short)((ushort)mode | (ushort)PREFORMATTED | (ushort)NOWRAP), indent, lexer, content);
					
					condFlushLine(fout, indent);
					printEndTag(fout, mode, indent, node);
					flushLine(fout, indent);
					
					if (this.configuration.IndentContent == false && node.next != null)
						flushLine(fout, indent);
				}
				else if (node.tag == tt.tagStyle || node.tag == tt.tagScript)
				{
					condFlushLine(fout, indent);
					
					indent = 0;
					condFlushLine(fout, indent);
					printTag(lexer, fout, mode, indent, node);
					flushLine(fout, indent);
					
					for (content = node.content; content != null; content = content.next)
						printTree(fout, (short) ((ushort)mode | (ushort)PREFORMATTED | (ushort)NOWRAP | (ushort)CDATA), indent, lexer, content);
					
					condFlushLine(fout, indent);
					printEndTag(fout, mode, indent, node);
					flushLine(fout, indent);
					
					if (this.configuration.IndentContent == false && node.next != null)
						flushLine(fout, indent);
				}
				else if ((node.tag.model & Dict.CM_INLINE) != 0)
				{
					printTag(lexer, fout, mode, indent, node);
					
					/* indent content for SELECT, TEXTAREA, MAP, OBJECT and APPLET */
					if (shouldIndent(node))
					{
						condFlushLine(fout, indent);
						indent += this.configuration.spaces;
						
						for (content = node.content; content != null; content = content.next)
							printTree(fout, mode, indent, lexer, content);
						
						condFlushLine(fout, indent);
						indent -= this.configuration.spaces;
						condFlushLine(fout, indent);
					}
					else
					{
						
						for (content = node.content; content != null; content = content.next)
							printTree(fout, mode, indent, lexer, content);
					}
					
					printEndTag(fout, mode, indent, node);
				}
				/* other tags */
				else
				{
					condFlushLine(fout, indent);
					printTag(lexer, fout, mode, indent, node);
						
					if (shouldIndent(node))
						condFlushLine(fout, indent);
					else if ((node.tag.model & Dict.CM_HTML) != 0 || node.tag == tt.tagNoframes || ((node.tag.model & Dict.CM_HEAD) != 0 && !(node.tag == tt.tagTitle)))
						flushLine(fout, indent);
					last = null;
						
					for (content = node.content; content != null; content = content.next)
					{
						/* kludge for naked text before block level tag */
						if (last != null && !this.configuration.IndentContent && last.type == Node.TextNode && content.tag != null && (content.tag.model & Dict.CM_BLOCK) != 0)
						{
							flushLine(fout, indent);
							flushLine(fout, indent);
						}
							
						printTree(fout, mode, (shouldIndent(node)?indent + this.configuration.spaces:indent), lexer, content);
							
						last = content;
					}
					
					/* don't flush line for td and th */
					if (shouldIndent(node) || (((node.tag.model & Dict.CM_HTML) != 0 || node.tag == tt.tagNoframes || ((node.tag.model & Dict.CM_HEAD) != 0 && !(node.tag == tt.tagTitle)))))
					{
						condFlushLine(fout, (this.configuration.IndentContent?indent + this.configuration.spaces:indent));
						printEndTag(fout, mode, indent, node);
						flushLine(fout, indent);
					}
					else
					{
						printEndTag(fout, mode, indent, node);
						flushLine(fout, indent);
					}
					
					if (this.configuration.IndentContent == false && node.next != null && (node.tag.model & (Dict.CM_BLOCK | Dict.CM_LIST | Dict.CM_DEFLIST | Dict.CM_TABLE)) != 0)
					{
						flushLine(fout, indent);
					}
				}
			}
		}
	}
}