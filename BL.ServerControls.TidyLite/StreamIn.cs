using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Input Stream</summary>
	public abstract class StreamIn
	{
		public const int EndOfStream = - 1; // EOF

		public bool pushed;
		public int c;
		public int tabs;
		public int tabsize;
		public int lastcol;
		public int curcol;
		public int curline;
		public int encoding;
		public System.IO.MemoryStream stream;
		public bool endOfStream;
		public System.Object lexer; /* needed for error reporting */
		
		public abstract int readCharFromStream();
		
		public abstract int readChar();
		
		public abstract void  ungetChar(int c);
		
		public abstract bool isEndOfStream();
	}
}