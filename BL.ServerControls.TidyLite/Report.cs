using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Error/informational message reporter.</summary>
	public class Report
	{
		public const string ACCESS_URL = "http://www.w3.org/WAI/GL";
		public const short MISSING_SEMICOLON = 1;
		public const short UNKNOWN_ENTITY = 2;
		public const short UNESCAPED_AMPERSAND = 3;
		
		public const short MISSING_ENDTAG_FOR = 1;
		public const short MISSING_ENDTAG_BEFORE = 2;
		public const short DISCARDING_UNEXPECTED = 3;
		public const short NESTED_EMPHASIS = 4;
		public const short NON_MATCHING_ENDTAG = 5;
		public const short TAG_NOT_ALLOWED_IN = 6;
		public const short MISSING_STARTTAG = 7;
		public const short UNEXPECTED_ENDTAG = 8;
		public const short USING_BR_INPLACE_OF = 9;
		public const short INSERTING_TAG = 10;
		public const short SUSPECTED_MISSING_QUOTE = 11;
		public const short MISSING_TITLE_ELEMENT = 12;
		public const short DUPLICATE_FRAMESET = 13;
		public const short CANT_BE_NESTED = 14;
		public const short OBSOLETE_ELEMENT = 15;
		public const short PROPRIETARY_ELEMENT = 16;
		public const short UNKNOWN_ELEMENT = 17;
		public const short TRIM_EMPTY_ELEMENT = 18;
		public const short COERCE_TO_ENDTAG = 19;
		public const short ILLEGAL_NESTING = 20;
		public const short NOFRAMES_CONTENT = 21;
		public const short CONTENT_AFTER_BODY = 22;
		public const short INCONSISTENT_VERSION = 23;
		public const short MALFORMED_COMMENT = 24;
		public const short BAD_COMMENT_CHARS = 25;
		public const short BAD_XML_COMMENT = 26;
		public const short BAD_CDATA_CONTENT = 27;
		public const short INCONSISTENT_NAMESPACE = 28;
		public const short DOCTYPE_AFTER_TAGS = 29;
		public const short MALFORMED_DOCTYPE = 30;
		public const short UNEXPECTED_END_OF_FILE = 31;
		public const short DTYPE_NOT_UPPER_CASE = 32;
		public const short TOO_MANY_ELEMENTS = 33;
		
		public const short UNKNOWN_ATTRIBUTE = 1;
		public const short MISSING_ATTRIBUTE = 2;
		public const short MISSING_ATTR_VALUE = 3;
		public const short BAD_ATTRIBUTE_VALUE = 4;
		public const short UNEXPECTED_GT = 5;
		public const short PROPRIETARY_ATTR_VALUE = 6;
		public const short REPEATED_ATTRIBUTE = 7;
		public const short MISSING_IMAGEMAP = 8;
		public const short XML_ATTRIBUTE_VALUE = 9;
		public const short UNEXPECTED_QUOTEMARK = 10;
		public const short ID_NAME_MISMATCH = 11;
		
		public const short MISSING_IMAGE_ALT = 1;
		public const short MISSING_LINK_ALT = 2;
		public const short MISSING_SUMMARY = 4;
		public const short MISSING_IMAGE_MAP = 8;
		public const short USING_FRAMES = 16;
		public const short USING_NOFRAMES = 32;
		
		public const short USING_SPACER = 1;
		public const short USING_LAYER = 2;
		public const short USING_NOBR = 4;
		public const short USING_FONT = 8;
		public const short USING_BODY = 16;
		
		public const short WINDOWS_CHARS = 1;
		public const short NON_ASCII = 2;
		public const short FOUND_UTF16 = 4;
		
		private static short optionerrors;
		
		public static void tidyPrint(System.IO.MemoryStream p, System.String msg)
		{
			byte[] buffer = SupportClass.ToByteArray(msg);
			p.Write(buffer, 0, buffer.Length);
		}
		
		public static void tidyPrintln(System.IO.MemoryStream p, System.String msg)
		{
			byte[] buffer = SupportClass.ToByteArray(msg + "\n");
			p.Write(buffer, 0, buffer.Length);
		}
		
		public static void tidyPrintln(System.IO.MemoryStream p)
		{
			byte[] buffer = SupportClass.ToByteArray("\n");
			p.Write(buffer, 0, buffer.Length);
		}
		
		public static void tag(Lexer lexer, Node tag)
		{
			if (tag != null)
			{
				if (tag.type == Node.StartTag)
					tidyPrint(lexer.errout, "<" + tag.element + ">");
				else if (tag.type == Node.EndTag)
					tidyPrint(lexer.errout, "</" + tag.element + ">");
				else if (tag.type == Node.DocTypeTag)
					tidyPrint(lexer.errout, "<!DOCTYPE>");
				else if (tag.type == Node.TextNode)
					tidyPrint(lexer.errout, "plain text");
				else
					tidyPrint(lexer.errout, tag.element);
			}
		}
		
		/* lexer is not defined when this is called */
		public static void  badArgument(System.String option)
		{
			optionerrors++;
			try
			{
				System.Console.Error.WriteLine("bad argument");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				System.Console.Error.WriteLine(e.ToString());
			}
		}
		
		
		public static void  position(Lexer lexer)
		{
			try
			{
				tidyPrint(lexer.errout, "line column");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
				lexer.errout.Write(buffer, 0, buffer.Length);
			}
		}
		
		public static void entityError(Lexer lexer, short code, string entity, int c)
		{
			lexer.warnings++;
			
			if (lexer.configuration.ShowWarnings)
			{
				position(lexer);
				
				
				if (code == MISSING_SEMICOLON)
				{
					try
					{
						tidyPrint(lexer.errout, "missing semicolon");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == UNKNOWN_ENTITY)
				{
					try
					{
						tidyPrint(lexer.errout, "unknown_entity");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == UNESCAPED_AMPERSAND)
				{
					try
					{
						tidyPrint(lexer.errout, "unescaped_ampersand");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				tidyPrintln(lexer.errout);
			}
		}
		
		public static void attrError(Lexer lexer, Node node, string attr, short code)
		{
			lexer.warnings++;
			
			/* keep quiet after 6 errors */
			if (lexer.errors > 6)
				return ;
			
			if (lexer.configuration.ShowWarnings)
			{
				/* on end of file adjust reported position to end of input */
				if (code == UNEXPECTED_END_OF_FILE)
				{
					lexer.lines = lexer.in_Renamed.curline;
					lexer.columns = lexer.in_Renamed.curcol;
				}
				
				position(lexer);
				
				if (code == UNKNOWN_ATTRIBUTE)
				{
					try
					{
						tidyPrint(lexer.errout, "unknown attribute");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MISSING_ATTRIBUTE)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "missing attribute");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MISSING_ATTR_VALUE)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "missing attr value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MISSING_IMAGEMAP)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "missing image map");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					short lexerDotBadAccess = lexer.badAccess;
					lexerDotBadAccess = (short)((ushort)lexerDotBadAccess | (ushort)MISSING_IMAGE_MAP);
					lexer.badAccess = lexerDotBadAccess;
				}
				else if (code == BAD_ATTRIBUTE_VALUE)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "bad attribute value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == XML_ATTRIBUTE_VALUE)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "xml attribute value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == UNEXPECTED_GT)
				{
					try
					{
						tidyPrint(lexer.errout, "error");
						tag(lexer, node);
						tidyPrint(lexer.errout, "unexpected gt");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					lexer.errors++; ;
				}
				else if (code == UNEXPECTED_QUOTEMARK)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "unexpected quotemark");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == REPEATED_ATTRIBUTE)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "repeated attribute");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == PROPRIETARY_ATTR_VALUE)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "proprietary attr value");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == UNEXPECTED_END_OF_FILE)
				{
					try
					{
						tidyPrint(lexer.errout, "unexpected end of file");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == ID_NAME_MISMATCH)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
						tag(lexer, node);
						tidyPrint(lexer.errout, "id name mismatch");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				tidyPrintln(lexer.errout);
			}
			else if (code == UNEXPECTED_GT)
			{
				position(lexer);
				try
				{
					tidyPrint(lexer.errout, "error");
					tag(lexer, node);
					tidyPrint(lexer.errout, "unexpected gt");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
				tidyPrintln(lexer.errout);
				lexer.errors++; ;
			}
		}
		
		public static void warning(Lexer lexer, Node element, Node node, short code)
		{
			TagTable tt = lexer.configuration.tt;
			
			lexer.warnings++;
			
			if (lexer.errors > 6)
				return ;
			
			if (lexer.configuration.ShowWarnings)
			{
				if (code == UNEXPECTED_END_OF_FILE)
				{
					lexer.lines = lexer.in_Renamed.curline;
					lexer.columns = lexer.in_Renamed.curcol;
				}
				
				position(lexer);
				
				if (code == MISSING_ENDTAG_FOR)
				{
					try
					{
						tidyPrint(lexer.errout, "missing endtag for");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MISSING_ENDTAG_BEFORE)
				{
					try
					{
						tidyPrint(lexer.errout, "missing endtag before");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
				}
				else if (code == DISCARDING_UNEXPECTED)
				{
					try
					{
						tidyPrint(lexer.errout, "discarding unexpected");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
				}
				else if (code == NESTED_EMPHASIS)
				{
					try
					{
						tidyPrint(lexer.errout, "nested emphasis");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
				}
				else if (code == COERCE_TO_ENDTAG)
				{
					try
					{
						tidyPrint(lexer.errout, "coerce to endtag");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == NON_MATCHING_ENDTAG)
				{
					try
					{
						tidyPrint(lexer.errout, "non matching endtag 1");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
					try
					{
						tidyPrint(lexer.errout, "non matching endtag 2");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == TAG_NOT_ALLOWED_IN)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
					try
					{
						tidyPrint(lexer.errout, "tag not allowed in");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == DOCTYPE_AFTER_TAGS)
				{
					try
					{
						tidyPrint(lexer.errout, "doctype after tags");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MISSING_STARTTAG)
				{
					try
					{
						tidyPrint(lexer.errout, "missing starttag");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == UNEXPECTED_ENDTAG)
				{
					try
					{
						tidyPrint(lexer.errout, "unexpected endtag");
						if (element != null)
						{
							tidyPrint(lexer.errout, "unexpected endtag suffix");
						}
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == TOO_MANY_ELEMENTS)
				{
					try
					{
						tidyPrint(lexer.errout, "too many elements");
						if (element != null)
						{
							tidyPrint(lexer.errout, "too many elements suffix");
						}
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == USING_BR_INPLACE_OF)
				{
					try
					{
						tidyPrint(lexer.errout, "using br inplace of");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
				}
				else if (code == INSERTING_TAG)
				{
					try
					{
						tidyPrint(lexer.errout, "inserting tag");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == CANT_BE_NESTED)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
					try
					{
						tidyPrint(lexer.errout, "cant be nested");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == PROPRIETARY_ELEMENT)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
					try
					{
						tidyPrint(lexer.errout, "proprietarty element");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					
					if (node.tag == tt.tagLayer)
						lexer.badLayout = (short)((ushort)lexer.badLayout | (ushort)USING_LAYER);
					else if (node.tag == tt.tagSpacer)
						lexer.badLayout = (short)((ushort)lexer.badLayout | (ushort)USING_SPACER);
					else if (node.tag == tt.tagNobr)
						lexer.badLayout = (short)((ushort)lexer.badLayout | (ushort)USING_NOBR);
				}
				else if (code == OBSOLETE_ELEMENT)
				{
					try
					{
						if (element.tag != null && (element.tag.model & Dict.CM_OBSOLETE) != 0)
						{
							tidyPrint(lexer.errout, "obsolete element");
						}
						else
						{
							tidyPrint(lexer.errout, "replacing element");
						}
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, element);
					try
					{
						tidyPrint(lexer.errout, "by");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
				}
				else if (code == TRIM_EMPTY_ELEMENT)
				{
					try
					{
						tidyPrint(lexer.errout, "trim empty element");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, element);
				}
				else if (code == MISSING_TITLE_ELEMENT)
				{
					try
					{
						tidyPrint(lexer.errout, "missing title element");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == ILLEGAL_NESTING)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, element);
					try
					{
						tidyPrint(lexer.errout, "illegal nesting");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == NOFRAMES_CONTENT)
				{
					try
					{
						tidyPrint(lexer.errout, "warning");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, node);
					try
					{
						tidyPrint(lexer.errout, "noframes content");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == INCONSISTENT_VERSION)
				{
					try
					{
						tidyPrint(lexer.errout, "inconsistent version");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MALFORMED_DOCTYPE)
				{
					try
					{
						tidyPrint(lexer.errout, "malformed doctype");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == CONTENT_AFTER_BODY)
				{
					try
					{
						tidyPrint(lexer.errout, "content after body");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == MALFORMED_COMMENT)
				{
					try
					{
						tidyPrint(lexer.errout, "malformed comment");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == BAD_COMMENT_CHARS)
				{
					try
					{
						tidyPrint(lexer.errout, "bad comment chars");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == BAD_XML_COMMENT)
				{
					try
					{
						tidyPrint(lexer.errout, "bad xml comment");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == BAD_CDATA_CONTENT)
				{
					try
					{
						tidyPrint(lexer.errout, "bad cdata content");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == INCONSISTENT_NAMESPACE)
				{
					try
					{
						tidyPrint(lexer.errout, "inconsistent namespace");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == DTYPE_NOT_UPPER_CASE)
				{
					try
					{
						tidyPrint(lexer.errout, "dtype not upper case");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				else if (code == UNEXPECTED_END_OF_FILE)
				{
					try
					{
						tidyPrint(lexer.errout, "unexpected end of file");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
					tag(lexer, element);
				}
				
				tidyPrintln(lexer.errout);
			}
		}
		
		public static void error(Lexer lexer, Node element, Node node, short code)
		{
			lexer.warnings++;
			
			if (lexer.errors > 6)
				return ;
			
			lexer.errors++;
			
			position(lexer);
			
			if (code == SUSPECTED_MISSING_QUOTE)
			{
				try
				{
					tidyPrint(lexer.errout, "suspected missing quote");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
			}
			else if (code == DUPLICATE_FRAMESET)
			{
				try
				{
					tidyPrint(lexer.errout, "duplicate frameset");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
			}
			else if (code == UNKNOWN_ELEMENT)
			{
				try
				{
					tidyPrint(lexer.errout, "error");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
				tag(lexer, node);
				try
				{
					tidyPrint(lexer.errout, "unknown element");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
			}
			else if (code == UNEXPECTED_ENDTAG)
			{
				try
				{
					tidyPrint(lexer.errout, "unexpected endtag");
					if (element != null)
					{
						tidyPrint(lexer.errout, "unexpected endtag suffix");
					}
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
			}
			
			tidyPrintln(lexer.errout);
		}
		
		public static void errorSummary(Lexer lexer)
		{
			if ((lexer.badAccess & (USING_FRAMES | USING_NOFRAMES)) != 0)
			{
				if (!(((lexer.badAccess & USING_FRAMES) != 0) && ((lexer.badAccess & USING_NOFRAMES) == 0)))
					lexer.badAccess &= ~ (USING_FRAMES | USING_NOFRAMES);
			}
			
			if (lexer.badChars != 0)
			{
				if ((lexer.badChars & WINDOWS_CHARS) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "badchars summary");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
			}
			
			if (lexer.badForm != 0)
			{
				try
				{
					tidyPrint(lexer.errout, "badform summary");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
			}
			
			if (lexer.badAccess != 0)
			{
				if ((lexer.badAccess & MISSING_SUMMARY) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "badaccess missing summary");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badAccess & MISSING_IMAGE_ALT) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "The alt attribute should be used to give a short description of an image; longer descriptions should be given with the longdesc attribute which takes a URL linked to the description.\n These measures are needed for people using non-graphical browsers.");

					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badAccess & MISSING_IMAGE_MAP) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "Use client-side image maps in preference to server-side image maps as the latter are inaccessible to people using non-graphical browsers. In addition, client-side maps are easier to set up and provide immediate feedback to users.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badAccess & MISSING_LINK_ALT) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "For hypertext links defined using a client-side image map, you need to use the alt attribute to provide a textual description of the link for people using non-graphical browsers.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if (((lexer.badAccess & USING_FRAMES) != 0) && ((lexer.badAccess & USING_NOFRAMES) == 0))
				{
					try
					{
						tidyPrint(lexer.errout, "Pages designed using frames presents problems for people who are either blind or using a browser that doesn't support frames. A frames-based page should always include an alternative layout inside a NOFRAMES element.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				try
				{
					tidyPrint(lexer.errout, "For further advice on how to make your pages accessible see " + ACCESS_URL + ". You may also want to try \"http://www.cast.org/bobby/\" which is a free Web-based service for checking URLs for accessibility.");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					lexer.errout.Write(buffer, 0, buffer.Length);
				}
			}
			
			if (lexer.badLayout != 0)
			{
				if ((lexer.badLayout & USING_LAYER) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "The Cascading Style Sheets (CSS) Positioning mechanism is recommended in preference to the proprietary <LAYER> element due to limited vendor support for LAYER.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badLayout & USING_SPACER) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "You are recommended to use CSS for controlling white space (e.g. for indentation, margins and line spacing). The proprietary <SPACER> element has limited vendor support.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badLayout & USING_FONT) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "You are recommended to use CSS to specify the font and properties such as its size and color. This will reduce the size of HTML files and make them easier maintain compared with using <FONT> elements.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badLayout & USING_NOBR) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "You are recommended to use CSS to control line wrapping.");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
				
				if ((lexer.badLayout & USING_BODY) != 0)
				{
					try
					{
						tidyPrint(lexer.errout, "You are recommended to use CSS to specify page and link colors");
					}
					catch (System.Resources.MissingManifestResourceException e)
					{
						byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
						lexer.errout.Write(buffer, 0, buffer.Length);
					}
				}
			}
		}
		
		public static void  needsAuthorIntervention(System.IO.MemoryStream errout)
		{
			try
			{
				tidyPrintln(errout, "This document has errors that must be fixed before using HTML Tidy to generate a tidied up version.");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
				errout.Write(buffer, 0, buffer.Length);
			}
		}
		
		public static void reportNumWarnings(System.IO.MemoryStream errout, Lexer lexer)
		{
			if (lexer.warnings > 0)
			{
				try
				{
					tidyPrintln(errout, lexer.warnings.ToString() + " warnings/errors were found!");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					errout.Write(buffer, 0, buffer.Length);
				}
			}
			else
			{
				try
				{
					tidyPrintln(errout, "no warnings or errors were found\n");
				}
				catch (System.Resources.MissingManifestResourceException e)
				{
					byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
					errout.Write(buffer, 0, buffer.Length);
				}
			}
		}
		
		public static void badTree(System.IO.MemoryStream errout)
		{
			try
			{
				tidyPrintln(errout, "\nPanic - tree has lost its integrity\n");
			}
			catch (System.Resources.MissingManifestResourceException e)
			{
				byte[] buffer = SupportClass.ToByteArray(e.ToString() + "\n");
				errout.Write(buffer, 0, buffer.Length);
			}
		}
	}
}