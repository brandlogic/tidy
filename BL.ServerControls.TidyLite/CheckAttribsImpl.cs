using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Check HTML attributes implementation</summary>
	public class CheckAttribsImpl
	{		
		public class CheckHTML : CheckAttribs
		{			
			public virtual void check(Lexer lexer, Node node)
			{
				AttVal attval;
				Attribute attribute;
				
				node.checkUniqueAttributes(lexer);
				
				for (attval = node.attributes; attval != null; attval = attval.next)
				{
					attribute = attval.checkAttribute(lexer, node);
					
					if (attribute == AttributeTable.attrXmlns)
						lexer.isvoyager = true;
				}
			}
		}
		
		public class CheckSCRIPT : CheckAttribs
		{	
			public virtual void check(Lexer lexer, Node node)
			{
				AttVal lang, type;
				
				node.checkUniqueAttributes(lexer);
				lang = node.getAttrByName("language");
				type = node.getAttrByName("type");
				
				if (type == null)
				{
					Report.attrError(lexer, node, "type", Report.MISSING_ATTRIBUTE);
					
					/* check for javascript */
					if (lang != null)
					{
						System.String str = lang.value_Renamed;
						if (str.Length > 10)
							str = str.Substring(0, (10) - (0));
						if ((Lexer.wstrcasecmp(str, "javascript") == 0) || (Lexer.wstrcasecmp(str, "jscript") == 0))
						{
							node.addAttribute("type", "text/javascript");
						}
					}
					else
						node.addAttribute("type", "text/javascript");
				}
			}
		}
		
		public class CheckTABLE : CheckAttribs
		{
			public virtual void  check(Lexer lexer, Node node)
			{
				AttVal attval;
				node.checkUniqueAttributes(lexer);
				attval = node.getAttrByName("border");
				if (attval != null)
				{
					if ((System.Object) attval.value_Renamed == null)
						attval.value_Renamed = "1";
				}
			}
		}
		
		public class CheckCaption : CheckAttribs
		{
			public virtual void  check(Lexer lexer, Node node)
			{
				AttVal attval;
				System.String value_Renamed = null;
				
				node.checkUniqueAttributes(lexer);
				
				for (attval = node.attributes; attval != null; attval = attval.next)
				{
					if (Lexer.wstrcasecmp(attval.attribute, "align") == 0)
					{
						value_Renamed = attval.value_Renamed;
						break;
					}
				}
			}
		}
		
		public class CheckHR : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				if (node.getAttrByName("src") != null)
					Report.attrError(lexer, node, "src", Report.PROPRIETARY_ATTR_VALUE);
			}
		}
		
		public class CheckIMG : CheckAttribs
		{	
			public virtual void  check(Lexer lexer, Node node)
			{
				AttVal attval;
				Attribute attribute;
				bool hasAlt = false;
				bool hasSrc = false;
				bool hasUseMap = false;
				bool hasIsMap = false;
				bool hasDataFld = false;
				
				node.checkUniqueAttributes(lexer);
				
				for (attval = node.attributes; attval != null; attval = attval.next)
				{
					attribute = attval.checkAttribute(lexer, node);
					
					if (attribute == AttributeTable.attrAlt)
						hasAlt = true;
					else if (attribute == AttributeTable.attrSrc)
						hasSrc = true;
					else if (attribute == AttributeTable.attrUsemap)
						hasUseMap = true;
					else if (attribute == AttributeTable.attrIsmap)
						hasIsMap = true;
					else if (attribute == AttributeTable.attrDatafld)
						hasDataFld = true;
				}
				
				if (!hasAlt)
				{
					node.addAttribute("alt", "");
				}
				
				if (!hasSrc && !hasDataFld)
					Report.attrError(lexer, node, "src", Report.MISSING_ATTRIBUTE);
				
				if (hasIsMap && !hasUseMap)
					Report.attrError(lexer, node, "ismap", Report.MISSING_IMAGEMAP);
			}
		}
		
		public class CheckAREA : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				AttVal attval;
				Attribute attribute;
				bool hasAlt = false;
				bool hasHref = false;
				
				node.checkUniqueAttributes(lexer);
				
				for (attval = node.attributes; attval != null; attval = attval.next)
				{
					attribute = attval.checkAttribute(lexer, node);
					
					if (attribute == AttributeTable.attrAlt)
						hasAlt = true;
					else if (attribute == AttributeTable.attrHref)
						hasHref = true;
				}
				
				if (!hasAlt)
				{
					short lexerDotBadAccess = lexer.badAccess;
					lexerDotBadAccess = (short)((ushort)lexerDotBadAccess | (ushort)Report.MISSING_LINK_ALT);
					lexer.badAccess = lexerDotBadAccess;
					Report.attrError(lexer, node, "alt", Report.MISSING_ATTRIBUTE);
				}
				if (!hasHref)
					Report.attrError(lexer, node, "href", Report.MISSING_ATTRIBUTE);
			}
		}
		
		public class CheckAnchor : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				node.checkUniqueAttributes(lexer);
				lexer.fixId(node);
			}
		}
		
		public class CheckMap : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				node.checkUniqueAttributes(lexer);
				lexer.fixId(node);
			}
		}
		
		public class CheckSTYLE : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				AttVal type = node.getAttrByName("type");
				node.checkUniqueAttributes(lexer);
				
				if (type == null)
				{
					Report.attrError(lexer, node, "type", Report.MISSING_ATTRIBUTE);
					node.addAttribute("type", "text/css");
				}
			}
		}
		
		public class CheckTableCell : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				node.checkUniqueAttributes(lexer);
			}
		}
		
		public class CheckLINK : CheckAttribs
		{
			public virtual void check(Lexer lexer, Node node)
			{
				AttVal rel = node.getAttrByName("rel");
				node.checkUniqueAttributes(lexer);
				
				if (rel != null && (System.Object) rel.value_Renamed != null && rel.value_Renamed.Equals("stylesheet"))
				{
					AttVal type = node.getAttrByName("type");
					
					if (type == null)
					{
						Report.attrError(lexer, node, "type", Report.MISSING_ATTRIBUTE);
						node.addAttribute("type", "text/css");
					}
				}
			}
		}
		
		public static CheckAttribs getCheckHTML()
		{
			return _checkHTML;
		}
		
		public static CheckAttribs getCheckSCRIPT()
		{
			return _checkSCRIPT;
		}
		
		public static CheckAttribs getCheckTABLE()
		{
			return _checkTABLE;
		}
		
		public static CheckAttribs getCheckCaption()
		{
			return _checkCaption;
		}
		
		public static CheckAttribs getCheckIMG()
		{
			return _checkIMG;
		}
		
		public static CheckAttribs getCheckAREA()
		{
			return _checkAREA;
		}
		
		public static CheckAttribs getCheckAnchor()
		{
			return _checkAnchor;
		}
		
		public static CheckAttribs getCheckMap()
		{
			return _checkMap;
		}
		
		public static CheckAttribs getCheckSTYLE()
		{
			return _checkStyle;
		}
		
		public static CheckAttribs getCheckTableCell()
		{
			return _checkTableCell;
		}
		
		public static CheckAttribs getCheckLINK()
		{
			return _checkLINK;
		}
		
		public static CheckAttribs getCheckHR()
		{
			return _checkHR;
		}
		
		private static CheckAttribs _checkHTML;
		private static CheckAttribs _checkSCRIPT;
		private static CheckAttribs _checkTABLE;
		private static CheckAttribs _checkCaption;
		private static CheckAttribs _checkIMG;
		private static CheckAttribs _checkAREA;
		private static CheckAttribs _checkAnchor;
		private static CheckAttribs _checkMap;
		private static CheckAttribs _checkStyle;
		private static CheckAttribs _checkTableCell;
		private static CheckAttribs _checkLINK;
		private static CheckAttribs _checkHR;
		static CheckAttribsImpl()
		{
			_checkHTML = new CheckHTML();
			_checkSCRIPT = new CheckSCRIPT();
			_checkTABLE = new CheckTABLE();
			_checkCaption = new CheckCaption();
			_checkIMG = new CheckIMG();
			_checkAREA = new CheckAREA();
			_checkAnchor = new CheckAnchor();
			_checkMap = new CheckMap();
			_checkStyle = new CheckSTYLE();
			_checkTableCell = new CheckTableCell();
			_checkLINK = new CheckLINK();
			_checkHR = new CheckHR();
		}
	}
}