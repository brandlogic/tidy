using System;

namespace BL.ServerControls.TidyLite
{
	/// <summary>
	/// Contains conversion support elements such as classes, interfaces and static methods.
	/// </summary>
	public class SupportClass
	{
		/// <summary>
		/// Adds a new key-and-value pair into the hash table
		/// </summary>
		/// <param name="collection">The collection to work with</param>
		/// <param name="key">Key used to obtain the value</param>
		/// <param name="newValue">Value asociated with the key</param>
		/// <returns>The old element associated with the key</returns>
		public static System.Object PutElement(System.Collections.Specialized.NameValueCollection collection, System.Object key, System.Object newValue)
		{
			System.Object element = collection[key.ToString()];
			collection[key.ToString()] = newValue.ToString();
			return element;
		}

		public static System.Object PutElement(System.Collections.IDictionary collection, System.Object key, System.Object newValue)
		{
			System.Object element = collection[key.ToString()];
			collection[key.ToString()] = newValue;
			return element;
		}

		/// <summary>
		/// Converts a string to an array of bytes
		/// </summary>
		/// <param name="sourceString">The string to be converted</param>
		/// <returns>The new array of bytes</returns>
		public static byte[] ToByteArray(string sourceString)
		{
			System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding(false);
			return utf8.GetBytes(sourceString);
		}

		/// <summary>
		/// Adds an element to the top end of a Stack instance.
		/// </summary>
		/// <param name="stack">The Stack instance</param>
		/// <param name="element">The element to add</param>
		/// <returns>The element added</returns>  
		public static System.Object StackPush(System.Collections.Stack stack, System.Object element)
		{
			stack.Push(element);
			return element;
		}
	}
}