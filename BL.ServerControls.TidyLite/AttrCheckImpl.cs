using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Check attribute values implementations</summary>
	public class AttrCheckImpl
	{
		public class CheckUrl : AttrCheck
		{	
			public virtual void check(Lexer lexer, Node node, AttVal attval)
			{
				if ((System.Object) attval.value_Renamed == null)
					Report.attrError(lexer, node, attval.attribute, Report.MISSING_ATTR_VALUE);
				else
				{
					attval.value_Renamed = attval.value_Renamed.Replace('\\', '/');
				}
			}
		}

		public class CheckAlign : AttrCheck
		{
			public virtual void check(Lexer lexer, Node node, AttVal attval)
			{
				string value_Renamed;
				
				/* IMG, OBJECT, APPLET and EMBED use align for vertical position */
				if (node.tag != null && ((node.tag.model & Dict.CM_IMG) != 0))
				{
					getCheckValign().check(lexer, node, attval);
					return ;
				}
				
				value_Renamed = attval.value_Renamed;
				
				if ((System.Object) value_Renamed == null)
					Report.attrError(lexer, node, attval.attribute, Report.MISSING_ATTR_VALUE);
				else if (!(Lexer.wstrcasecmp(value_Renamed, "left") == 0 || Lexer.wstrcasecmp(value_Renamed, "center") == 0 || Lexer.wstrcasecmp(value_Renamed, "right") == 0 || Lexer.wstrcasecmp(value_Renamed, "justify") == 0))
					Report.attrError(lexer, node, attval.value_Renamed, Report.BAD_ATTRIBUTE_VALUE);
			}
		}
		
		public class CheckValign : AttrCheck
		{
			public virtual void check(Lexer lexer, Node node, AttVal attval)
			{
				string value_Renamed;
				
				value_Renamed = attval.value_Renamed;
				
				if ((System.Object) value_Renamed == null)
					Report.attrError(lexer, node, attval.attribute, Report.MISSING_ATTR_VALUE);
				else if (Lexer.wstrcasecmp(value_Renamed, "top") == 0 || Lexer.wstrcasecmp(value_Renamed, "middle") == 0 || Lexer.wstrcasecmp(value_Renamed, "bottom") == 0 || Lexer.wstrcasecmp(value_Renamed, "baseline") == 0)
				{
					/* all is fine */
					return;
				}
				else if (Lexer.wstrcasecmp(value_Renamed, "left") == 0 || Lexer.wstrcasecmp(value_Renamed, "right") == 0)
				{
					if (!(node.tag != null && ((node.tag.model & Dict.CM_IMG) != 0)))
						Report.attrError(lexer, node, value_Renamed, Report.BAD_ATTRIBUTE_VALUE);
				}
				else if (Lexer.wstrcasecmp(value_Renamed, "texttop") == 0 || Lexer.wstrcasecmp(value_Renamed, "absmiddle") == 0 || Lexer.wstrcasecmp(value_Renamed, "absbottom") == 0 || Lexer.wstrcasecmp(value_Renamed, "textbottom") == 0)
				{
					Report.attrError(lexer, node, value_Renamed, Report.PROPRIETARY_ATTR_VALUE);
				}
				else
					Report.attrError(lexer, node, value_Renamed, Report.BAD_ATTRIBUTE_VALUE);
			}
		}

		public static AttrCheck getCheckUrl()
		{
			return _checkUrl;
		}
		
		public static AttrCheck getCheckAlign()
		{
			return _checkAlign;
		}
		
		public static AttrCheck getCheckValign()
		{
			return _checkValign;
		}
		
		private static AttrCheck _checkUrl;
		private static AttrCheck _checkAlign;
		private static AttrCheck _checkValign;
		static AttrCheckImpl()
		{
			_checkUrl = new CheckUrl();
			_checkAlign = new CheckAlign();
			_checkValign = new CheckValign();
		}
	}
}