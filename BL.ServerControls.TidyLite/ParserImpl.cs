using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>HTML Parser implementation</summary>
	public class ParserImpl
	{
		private static void parseTag(Lexer lexer, Node node, short mode)
		{
			
			if (!((node.tag.model & Dict.CM_INLINE) != 0))
				lexer.insertspace = false;
			
			if ((node.tag.model & Dict.CM_EMPTY) != 0)
			{
				lexer.waswhite = false;
				return ;
			}
			
			if (node.tag.parser == null || node.type == Node.StartEndTag)
				return ;
			
			node.tag.parser.parse(lexer, node, mode);
		}
		
		private static void moveToHead(Lexer lexer, Node element, Node node)
		{
			Node head;
			TagTable tt = lexer.configuration.tt;
			
			if (node.type == Node.StartTag || node.type == Node.StartEndTag)
			{
				Report.warning(lexer, element, node, Report.TAG_NOT_ALLOWED_IN);
				
				while (element.tag != tt.tagHtml)
					element = element.parent;
				
				for (head = element.content; head != null; head = head.next)
				{
					if (head.tag == tt.tagHead)
					{
						Node.insertNodeAtEnd(head, node);
						break;
					}
				}
				
				if (node.tag.parser != null)
					parseTag(lexer, node, Lexer.IgnoreWhitespace);
			}
			else
			{
				Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
			}
		}
		
		public class ParseHTML : Parser
		{
			public virtual void  parse(Lexer lexer, Node html, short mode)
			{
				Node node, head;
				Node frameset = null;
				Node noframes = null;
				
				lexer.seenBodyEndTag = 0;
				TagTable tt = lexer.configuration.tt;
				
				for (; ; )
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					
					if (node == null)
					{
						node = lexer.inferredTag("head");
						break;
					}
					
					if (node.tag == tt.tagHead)
						break;
					
					if (node.tag == html.tag && node.type == Node.EndTag)
					{
						Report.warning(lexer, html, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (Node.insertMisc(html, node))
						continue;
					
					lexer.ungetToken();
					node = lexer.inferredTag("head");
					break;
				}
				
				head = node;
				Node.insertNodeAtEnd(html, head);
				getParseHead().parse(lexer, head, mode);
				
				for (; ; )
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					
					if (node == null)
					{
						if (frameset == null)
							node = lexer.inferredTag("body");
						
						return ;
					}
					
					if (node.tag == html.tag)
					{
						if (node.type != Node.StartTag && frameset == null)
							Report.warning(lexer, html, node, Report.DISCARDING_UNEXPECTED);
						
						continue;
					}
					
					if (Node.insertMisc(html, node))
						continue;
					
					if (node.tag == tt.tagBody)
					{
						if (node.type != Node.StartTag)
						{
							Report.warning(lexer, html, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (frameset != null)
						{
							lexer.ungetToken();
							
							if (noframes == null)
							{
								noframes = lexer.inferredTag("noframes");
								Node.insertNodeAtEnd(frameset, noframes);
								Report.warning(lexer, html, noframes, Report.INSERTING_TAG);
							}
							
							parseTag(lexer, noframes, mode);
							continue;
						}
						
						break; /* to parse body */
					}
					
					if (node.tag == tt.tagFrameset)
					{
						if (node.type != Node.StartTag)
						{
							Report.warning(lexer, html, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (frameset != null)
							Report.error(lexer, html, node, Report.DUPLICATE_FRAMESET);
						else
							frameset = node;
						
						Node.insertNodeAtEnd(html, node);
						parseTag(lexer, node, mode);
						
						for (node = frameset.content; node != null; node = node.next)
						{
							if (node.tag == tt.tagNoframes)
								noframes = node;
						}
						continue;
					}
					
					if (node.tag == tt.tagNoframes)
					{
						if (node.type != Node.StartTag)
						{
							Report.warning(lexer, html, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (frameset == null)
						{
							Report.warning(lexer, html, node, Report.DISCARDING_UNEXPECTED);
							node = lexer.inferredTag("body");
							break;
						}
						
						if (noframes == null)
						{
							noframes = node;
							Node.insertNodeAtEnd(frameset, noframes);
						}
						
						parseTag(lexer, noframes, mode);
						continue;
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if (node.tag != null && (node.tag.model & Dict.CM_HEAD) != 0)
						{
							moveToHead(lexer, html, node);
							continue;
						}
					}
					
					lexer.ungetToken();
					
					if (frameset != null)
					{
						if (noframes == null)
						{
							noframes = lexer.inferredTag("noframes");
							Node.insertNodeAtEnd(frameset, noframes);
						}
						else
							Report.warning(lexer, html, node, Report.NOFRAMES_CONTENT);
						
						parseTag(lexer, noframes, mode);
						continue;
					}
					
					node = lexer.inferredTag("body");
					break;
				}
				
				Node.insertNodeAtEnd(html, node);
				parseTag(lexer, node, mode);
			}
		}
		
		public class ParseHead : Parser
		{
			public virtual void  parse(Lexer lexer, Node head, short mode)
			{
				Node node;
				int HasTitle = 0;
				int HasBase = 0;
				TagTable tt = lexer.configuration.tt;
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == head.tag && node.type == Node.EndTag)
					{
						head.closed = true;
						break;
					}
					
					if (node.type == Node.TextNode)
					{
						lexer.ungetToken();
						break;
					}
					
					if (Node.insertMisc(head, node))
						continue;
					
					if (node.type == Node.DocTypeTag)
					{
						Node.insertDocType(lexer, head, node);
						continue;
					}
					
					if (node.tag == null)
					{
						Report.warning(lexer, head, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (!((node.tag.model & Dict.CM_HEAD) != 0))
					{
						lexer.ungetToken();
						break;
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if (node.tag == tt.tagTitle)
						{
							++HasTitle;
							
							if (HasTitle > 1)
								Report.warning(lexer, head, node, Report.TOO_MANY_ELEMENTS);
						}
						else if (node.tag == tt.tagBase)
						{
							++HasBase;
							
							if (HasBase > 1)
								Report.warning(lexer, head, node, Report.TOO_MANY_ELEMENTS);
						}
						else if (node.tag == tt.tagNoscript)
							Report.warning(lexer, head, node, Report.TAG_NOT_ALLOWED_IN);
						
						Node.insertNodeAtEnd(head, node);
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						continue;
					}
					
					Report.warning(lexer, head, node, Report.DISCARDING_UNEXPECTED);
				}
				
				if (HasTitle == 0)
				{
					Report.warning(lexer, head, null, Report.MISSING_TITLE_ELEMENT);
					Node.insertNodeAtEnd(head, lexer.inferredTag("title"));
				}
			}
		}
		
		public class ParseTitle : Parser
		{
			public virtual void  parse(Lexer lexer, Node title, short mode)
			{
				Node node;
				
				while (true)
				{
					node = lexer.getToken(Lexer.MixedContent);
					if (node == null)
						break;
					if (node.tag == title.tag && node.type == Node.EndTag)
					{
						title.closed = true;
						Node.trimSpaces(lexer, title);
						return ;
					}
					
					if (node.type == Node.TextNode)
					{
						if (title.content == null)
							Node.trimInitialSpace(lexer, title, node);
						
						if (node.start >= node.end)
						{
							continue;
						}
						
						Node.insertNodeAtEnd(title, node);
						continue;
					}
					
					if (Node.insertMisc(title, node))
						continue;
					
					if (node.tag == null)
					{
						Report.warning(lexer, title, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					Report.warning(lexer, title, node, Report.MISSING_ENDTAG_BEFORE);
					lexer.ungetToken();
					Node.trimSpaces(lexer, title);
					return ;
				}
				
				Report.warning(lexer, title, node, Report.MISSING_ENDTAG_FOR);
			}
		}
		
		public class ParseScript : Parser
		{
			public virtual void parse(Lexer lexer, Node script, short mode)
			{
				Node node;
				
				node = lexer.getCDATA(script);
				
				if (node != null)
					Node.insertNodeAtEnd(script, node);
			}
		}
		
		public class ParseBody : Parser
		{
			public virtual void parse(Lexer lexer, Node body, short mode)
			{
				Node node;
				bool checkstack, iswhitenode;
				
				mode = Lexer.IgnoreWhitespace;
				checkstack = true;
				TagTable tt = lexer.configuration.tt;
				
				while (true)
				{
					node = lexer.getToken(mode);
					if (node == null)
						break;
					if (node.tag == body.tag && node.type == Node.EndTag)
					{
						body.closed = true;
						Node.trimSpaces(lexer, body);
						lexer.seenBodyEndTag = 1;
						mode = Lexer.IgnoreWhitespace;
						
						if (body.parent.tag == tt.tagNoframes)
							break;
						
						continue;
					}
					
					if (node.tag == tt.tagNoframes)
					{
						if (node.type == Node.StartTag)
						{
							Node.insertNodeAtEnd(body, node);
							getParseBlock().parse(lexer, node, mode);
							continue;
						}
						
						if (node.type == Node.EndTag && body.parent.tag == tt.tagNoframes)
						{
							Node.trimSpaces(lexer, body);
							lexer.ungetToken();
							break;
						}
					}
					
					if ((node.tag == tt.tagFrame || node.tag == tt.tagFrameset) && body.parent.tag == tt.tagNoframes)
					{
						Node.trimSpaces(lexer, body);
						lexer.ungetToken();
						break;
					}
					
					if (node.tag == tt.tagHtml)
					{
						if (node.type == Node.StartTag || node.type == Node.StartEndTag)
							Report.warning(lexer, body, node, Report.DISCARDING_UNEXPECTED);
						
						continue;
					}
					
					iswhitenode = false;
					
					if (node.type == Node.TextNode && node.end <= node.start + 1 && node.textarray[node.start] == (byte) ' ')
						iswhitenode = true;
					
					if (Node.insertMisc(body, node))
						continue;
					
					if (lexer.seenBodyEndTag == 1 && !iswhitenode)
					{
						++lexer.seenBodyEndTag;
						Report.warning(lexer, body, node, Report.CONTENT_AFTER_BODY);
					}
					
					if (node.type == Node.TextNode)
					{
						if (iswhitenode && mode == Lexer.IgnoreWhitespace)
						{
							continue;
						}
						
						if (checkstack)
						{
							checkstack = false;
							
							if (lexer.inlineDup(node) > 0)
								continue;
						}
						
						Node.insertNodeAtEnd(body, node);
						mode = Lexer.MixedContent;
						continue;
					}
					
					if (node.type == Node.DocTypeTag)
					{
						Node.insertDocType(lexer, body, node);
						continue;
					}
					if (node.tag == null || node.tag == tt.tagParam)
					{
						Report.warning(lexer, body, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					lexer.excludeBlocks = false;
					
					if (!((node.tag.model & Dict.CM_BLOCK) != 0) && !((node.tag.model & Dict.CM_INLINE) != 0))
					{
						if (!((node.tag.model & Dict.CM_HEAD) != 0))
							Report.warning(lexer, body, node, Report.TAG_NOT_ALLOWED_IN);
						
						if ((node.tag.model & Dict.CM_HTML) != 0)
						{
							if (node.tag == tt.tagBody && body.implicit_Renamed && body.attributes == null)
							{
								body.attributes = node.attributes;
								node.attributes = null;
							}
							
							continue;
						}
						
						if ((node.tag.model & Dict.CM_HEAD) != 0)
						{
							moveToHead(lexer, body, node);
							continue;
						}
						
						if ((node.tag.model & Dict.CM_LIST) != 0)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("ul");
							Node.addClass(node, "noindent");
							lexer.excludeBlocks = true;
						}
						else if ((node.tag.model & Dict.CM_DEFLIST) != 0)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("dl");
							lexer.excludeBlocks = true;
						}
						else if ((node.tag.model & (Dict.CM_TABLE | Dict.CM_ROWGRP | Dict.CM_ROW)) != 0)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("table");
							lexer.excludeBlocks = true;
						}
						else
						{
							if (!((node.tag.model & (Dict.CM_ROW | Dict.CM_FIELD)) != 0))
							{
								lexer.ungetToken();
								return ;
							}
							
							continue;
						}
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagBr)
							node.type = (short)Node.StartTag;
						else if (node.tag == tt.tagP)
						{
							Node.coerceNode(lexer, node, tt.tagBr);
							Node.insertNodeAtEnd(body, node);
							node = lexer.inferredTag("br");
						}
						else if ((node.tag.model & Dict.CM_INLINE) != 0)
							lexer.popInline(node);
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if (((node.tag.model & Dict.CM_INLINE) != 0) && !((node.tag.model & Dict.CM_MIXED) != 0))
						{
							if (checkstack && !node.implicit_Renamed)
							{
								checkstack = false;
								
								if (lexer.inlineDup(node) > 0)
									continue;
							}
							
							mode = Lexer.MixedContent;
						}
						else
						{
							checkstack = true;
							mode = Lexer.IgnoreWhitespace;
						}
						
						if (node.implicit_Renamed)
							Report.warning(lexer, body, node, Report.INSERTING_TAG);
						
						Node.insertNodeAtEnd(body, node);
						parseTag(lexer, node, mode);
						continue;
					}
					
					Report.warning(lexer, body, node, Report.DISCARDING_UNEXPECTED);
				}
			}
		}
		
		public class ParseFrameSet : Parser
		{
			public virtual void  parse(Lexer lexer, Node frameset, short mode)
			{
				Node node;
				TagTable tt = lexer.configuration.tt;
				
				short lexerDotBadAccess = lexer.badAccess;
				lexerDotBadAccess = (short)((ushort)lexerDotBadAccess | (ushort)Report.USING_FRAMES);
				lexer.badAccess = lexerDotBadAccess;
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == frameset.tag && node.type == Node.EndTag)
					{
						frameset.closed = true;
						Node.trimSpaces(lexer, frameset);
						return ;
					}
					
					if (Node.insertMisc(frameset, node))
						continue;
					
					if (node.tag == null)
					{
						Report.warning(lexer, frameset, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if (node.tag != null && (node.tag.model & Dict.CM_HEAD) != 0)
						{
							moveToHead(lexer, frameset, node);
							continue;
						}
					}
					
					if (node.tag == tt.tagBody)
					{
						lexer.ungetToken();
						node = lexer.inferredTag("noframes");
						Report.warning(lexer, frameset, node, Report.INSERTING_TAG);
					}
					
					if (node.type == Node.StartTag && (node.tag.model & Dict.CM_FRAMES) != 0)
					{
						Node.insertNodeAtEnd(frameset, node);
						lexer.excludeBlocks = false;
						parseTag(lexer, node, Lexer.MixedContent);
						continue;
					}
					else if (node.type == Node.StartEndTag && (node.tag.model & Dict.CM_FRAMES) != 0)
					{
						Node.insertNodeAtEnd(frameset, node);
						continue;
					}
					
					Report.warning(lexer, frameset, node, Report.DISCARDING_UNEXPECTED);
				}
				
				Report.warning(lexer, frameset, node, Report.MISSING_ENDTAG_FOR);
			}
		}
		
		public class ParseInline : Parser
		{
			public virtual void  parse(Lexer lexer, Node element, short mode)
			{
				Node node, parent;
				TagTable tt = lexer.configuration.tt;
				
				if ((element.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				if (element.tag == tt.tagA)
				{
					if (element.attributes == null)
					{
						Report.warning(lexer, element.parent, element, Report.DISCARDING_UNEXPECTED);
						Node.discardElement(element);
						return ;
					}
				}
				
				if (((element.tag.model & Dict.CM_BLOCK) != 0) || (element.tag == tt.tagDt))
					lexer.inlineDup(null);
				else if ((element.tag.model & Dict.CM_INLINE) != 0 && element.tag != tt.tagA && element.tag != tt.tagSpan)
					lexer.pushInline(element);
				
				if (element.tag == tt.tagNobr)
					lexer.badLayout = (short)((ushort)lexer.badLayout | (ushort)Report.USING_NOBR);
				else if (element.tag == tt.tagFont)
					lexer.badLayout = (short)((ushort)lexer.badLayout | (ushort)Report.USING_FONT);
				
				if (mode != Lexer.Preformatted)
					mode = Lexer.MixedContent;
				
				while (true)
				{
					node = lexer.getToken(mode);
					if (node == null)
						break;
					/* end tag for current element */
					if (node.tag == element.tag && node.type == Node.EndTag)
					{
						if ((element.tag.model & Dict.CM_INLINE) != 0 && element.tag != tt.tagA)
							lexer.popInline(node);
						
						if (!((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, element);
						if (element.tag == tt.tagFont && element.content != null && element.content == element.last)
						{
							Node child = element.content;
							
							if (child.tag == tt.tagA)
							{
								child.parent = element.parent;
								child.next = element.next;
								child.prev = element.prev;
								
								if (child.prev != null)
									child.prev.next = child;
								else
									child.parent.content = child;
								
								if (child.next != null)
									child.next.prev = child;
								else
									child.parent.last = child;
								
								element.next = null;
								element.prev = null;
								element.parent = child;
								element.content = child.content;
								element.last = child.last;
								child.content = element;
								child.last = element;
								for (child = element.content; child != null; child = child.next)
									child.parent = element;
							}
						}
						element.closed = true;
						Node.trimSpaces(lexer, element);
						Node.trimEmptyElement(lexer, element);
						return ;
					}
					
					if (node.type == Node.StartTag && node.tag == element.tag && lexer.isPushed(node) && !node.implicit_Renamed && !element.implicit_Renamed && node.tag != null && ((node.tag.model & Dict.CM_INLINE) != 0) && node.tag != tt.tagA && node.tag != tt.tagFont && node.tag != tt.tagBig && node.tag != tt.tagSmall)
					{
						if (element.content != null && node.attributes == null)
						{
							Report.warning(lexer, element, node, Report.COERCE_TO_ENDTAG);
							node.type = (short)Node.EndTag;
							lexer.ungetToken();
							continue;
						}
						
						Report.warning(lexer, element, node, Report.NESTED_EMPHASIS);
					}
					
					if (node.type == Node.TextNode)
					{
						if (element.content == null && !((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, element);
						
						if (node.start >= node.end)
						{
							continue;
						}
						
						Node.insertNodeAtEnd(element, node);
						continue;
					}
					
					if (Node.insertMisc(element, node))
						continue;
					
					if (node.tag == tt.tagHtml)
					{
						if (node.type == Node.StartTag || node.type == Node.StartEndTag)
						{
							Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						lexer.ungetToken();
						if (!((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, element);
						Node.trimEmptyElement(lexer, element);
						return ;
					}
					
					if (node.tag == tt.tagP && node.type == Node.StartTag && ((mode & Lexer.Preformatted) != 0 || element.tag == tt.tagDt || element.isDescendantOf(tt.tagDt)))
					{
						node.tag = tt.tagBr;
						node.element = "br";
						Node.trimSpaces(lexer, element);
						Node.insertNodeAtEnd(element, node);
						continue;
					}
					
					if (node.tag == null || node.tag == tt.tagParam)
					{
						Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag == tt.tagBr && node.type == Node.EndTag)
						node.type = (short)Node.StartTag;
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagBr)
							node.type = (short)Node.StartTag;
						else if (node.tag == tt.tagP)
						{
							if (!element.isDescendantOf(tt.tagP))
							{
								Node.coerceNode(lexer, node, tt.tagBr);
								Node.trimSpaces(lexer, element);
								Node.insertNodeAtEnd(element, node);
								node = lexer.inferredTag("br");
								continue;
							}
						}
						else if ((node.tag.model & Dict.CM_INLINE) != 0 && node.tag != tt.tagA && !((node.tag.model & Dict.CM_OBJECT) != 0) && (element.tag.model & Dict.CM_INLINE) != 0)
						{
							lexer.popInline(element);
							
							if (element.tag != tt.tagA)
							{
								if (node.tag == tt.tagA && node.tag != element.tag)
								{
									Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
									lexer.ungetToken();
								}
								else
								{
									Report.warning(lexer, element, node, Report.NON_MATCHING_ENDTAG);
								}
								
								if (!((mode & Lexer.Preformatted) != 0))
									Node.trimSpaces(lexer, element);
								Node.trimEmptyElement(lexer, element);
								return ;
							}
							
							Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						else if (lexer.exiled && node.tag.model != 0 && (node.tag.model & Dict.CM_TABLE) != 0)
						{
							lexer.ungetToken();
							Node.trimSpaces(lexer, element);
							Node.trimEmptyElement(lexer, element);
							return ;
						}
					}
					
					if ((node.tag.model & Dict.CM_HEADING) != 0 && (element.tag.model & Dict.CM_HEADING) != 0)
					{
						if (node.tag == element.tag)
						{
							Report.warning(lexer, element, node, Report.NON_MATCHING_ENDTAG);
						}
						else
						{
							Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
							lexer.ungetToken();
						}
						if (!((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, element);
						Node.trimEmptyElement(lexer, element);
						return ;
					}
					
					if (node.tag == tt.tagA && !node.implicit_Renamed && lexer.isPushed(node))
					{
						if (node.attributes == null)
						{
							node.type = (short)Node.EndTag;
							Report.warning(lexer, element, node, Report.COERCE_TO_ENDTAG);
							lexer.popInline(node);
							lexer.ungetToken();
							continue;
						}
						
						lexer.ungetToken();
						Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
						lexer.popInline(element);
						if (!((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, element);
						Node.trimEmptyElement(lexer, element);
						return ;
					}
					
					if ((element.tag.model & Dict.CM_HEADING) != 0)
					{
						if (node.tag == tt.tagCenter || node.tag == tt.tagDiv)
						{
							if (node.type != Node.StartTag && node.type != Node.StartEndTag)
							{
								Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
								continue;
							}
							
							Report.warning(lexer, element, node, Report.TAG_NOT_ALLOWED_IN);
							
							if (element.content == null)
							{
								Node.insertNodeAsParent(element, node);
								continue;
							}
							
							Node.insertNodeAfterElement(element, node);
							
							if (!((mode & Lexer.Preformatted) != 0))
								Node.trimSpaces(lexer, element);
							
							element = lexer.cloneNode(element);
							element.start = lexer.lexsize;
							element.end = lexer.lexsize;
							Node.insertNodeAtEnd(node, element);
							continue;
						}
						
						if (node.tag == tt.tagHr)
						{
							if (node.type != Node.StartTag && node.type != Node.StartEndTag)
							{
								Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
								continue;
							}
							
							Report.warning(lexer, element, node, Report.TAG_NOT_ALLOWED_IN);
							
							if (element.content == null)
							{
								Node.insertNodeBeforeElement(element, node);
								continue;
							}
							
							Node.insertNodeAfterElement(element, node);
							
							if (!((mode & Lexer.Preformatted) != 0))
								Node.trimSpaces(lexer, element);
							
							element = lexer.cloneNode(element);
							element.start = lexer.lexsize;
							element.end = lexer.lexsize;
							Node.insertNodeAfterElement(node, element);
							continue;
						}
					}
					
					if (element.tag == tt.tagDt)
					{
						if (node.tag == tt.tagHr)
						{
							Node dd;
							
							if (node.type != Node.StartTag && node.type != Node.StartEndTag)
							{
								Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
								continue;
							}
							
							Report.warning(lexer, element, node, Report.TAG_NOT_ALLOWED_IN);
							dd = lexer.inferredTag("dd");
							
							if (element.content == null)
							{
								Node.insertNodeBeforeElement(element, dd);
								Node.insertNodeAtEnd(dd, node);
								continue;
							}
							
							Node.insertNodeAfterElement(element, dd);
							Node.insertNodeAtEnd(dd, node);
							
							if (!((mode & Lexer.Preformatted) != 0))
								Node.trimSpaces(lexer, element);
							
							element = lexer.cloneNode(element);
							element.start = lexer.lexsize;
							element.end = lexer.lexsize;
							Node.insertNodeAfterElement(dd, element);
							continue;
						}
					}
					
					if (node.type == Node.EndTag)
					{
						for (parent = element.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								if (!((element.tag.model & Dict.CM_OPT) != 0) && !element.implicit_Renamed)
									Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
								
								if (element.tag == tt.tagA)
									lexer.popInline(element);
								
								lexer.ungetToken();
								
								if (!((mode & Lexer.Preformatted) != 0))
									Node.trimSpaces(lexer, element);
								
								Node.trimEmptyElement(lexer, element);
								return ;
							}
						}
					}
					
					if (!((node.tag.model & Dict.CM_INLINE) != 0))
					{
						if (node.type != Node.StartTag)
						{
							Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (!((element.tag.model & Dict.CM_OPT) != 0))
							Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
						
						if ((node.tag.model & Dict.CM_HEAD) != 0 && !((node.tag.model & Dict.CM_BLOCK) != 0))
						{
							moveToHead(lexer, element, node);
							continue;
						}
						
						if (element.tag == tt.tagA)
						{
							if (node.tag != null && !((node.tag.model & Dict.CM_HEADING) != 0))
								lexer.popInline(element);
							else if (!(element.content != null))
							{
								Node.discardElement(element);
								lexer.ungetToken();
								return ;
							}
						}
						
						lexer.ungetToken();
						
						if (!((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, element);
						
						Node.trimEmptyElement(lexer, element);
						return ;
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if (node.implicit_Renamed)
							Report.warning(lexer, element, node, Report.INSERTING_TAG);
						
						if (node.tag == tt.tagBr)
							Node.trimSpaces(lexer, element);
						
						Node.insertNodeAtEnd(element, node);
						parseTag(lexer, node, mode);
						continue;
					}
					
					Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
				}
				
				if (!((element.tag.model & Dict.CM_OPT) != 0))
					Report.warning(lexer, element, node, Report.MISSING_ENDTAG_FOR);
				
				Node.trimEmptyElement(lexer, element);
			}
		}
		
		public class ParseList : Parser
		{
			public virtual void  parse(Lexer lexer, Node list, short mode)
			{
				Node node;
				Node parent;
				TagTable tt = lexer.configuration.tt;
				
				if ((list.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				lexer.insert = - 1; /* defer implicit inline start tags */
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					
					if (node.tag == list.tag && node.type == Node.EndTag)
					{
						if ((list.tag.model & Dict.CM_OBSOLETE) != 0)
							Node.coerceNode(lexer, list, tt.tagUl);
						
						list.closed = true;
						Node.trimEmptyElement(lexer, list);
						return ;
					}
					
					if (Node.insertMisc(list, node))
						continue;
					
					if (node.type != Node.TextNode && node.tag == null)
					{
						Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (node.tag != null && (node.tag.model & Dict.CM_INLINE) != 0)
						{
							Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
							lexer.popInline(node);
							continue;
						}
						
						for (parent = list.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								Report.warning(lexer, list, node, Report.MISSING_ENDTAG_BEFORE);
								lexer.ungetToken();
								
								if ((list.tag.model & Dict.CM_OBSOLETE) != 0)
									Node.coerceNode(lexer, list, tt.tagUl);
								
								Node.trimEmptyElement(lexer, list);
								return ;
							}
						}
						
						Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag != tt.tagLi)
					{
						lexer.ungetToken();
						
						if (node.tag != null && (node.tag.model & Dict.CM_BLOCK) != 0 && lexer.excludeBlocks)
						{
							Report.warning(lexer, list, node, Report.MISSING_ENDTAG_BEFORE);
							Node.trimEmptyElement(lexer, list);
							return ;
						}
						
						node = lexer.inferredTag("li");
						node.addAttribute("style", "list-style: none");
						Report.warning(lexer, list, node, Report.MISSING_STARTTAG);
					}
					
					Node.insertNodeAtEnd(list, node);
					parseTag(lexer, node, Lexer.IgnoreWhitespace);
				}
				
				if ((list.tag.model & Dict.CM_OBSOLETE) != 0)
					Node.coerceNode(lexer, list, tt.tagUl);
				
				Report.warning(lexer, list, node, Report.MISSING_ENDTAG_FOR);
				Node.trimEmptyElement(lexer, list);
			}
		}
		
		public class ParseDefList : Parser
		{
			public virtual void  parse(Lexer lexer, Node list, short mode)
			{
				Node node, parent;
				TagTable tt = lexer.configuration.tt;
				
				if ((list.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				lexer.insert = - 1; /* defer implicit inline start tags */
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == list.tag && node.type == Node.EndTag)
					{
						list.closed = true;
						Node.trimEmptyElement(lexer, list);
						return ;
					}
					
					if (Node.insertMisc(list, node))
						continue;
					
					if (node.type == Node.TextNode)
					{
						lexer.ungetToken();
						node = lexer.inferredTag("dt");
						Report.warning(lexer, list, node, Report.MISSING_STARTTAG);
					}
					
					if (node.tag == null)
					{
						Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						for (parent = list.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								Report.warning(lexer, list, node, Report.MISSING_ENDTAG_BEFORE);
								
								lexer.ungetToken();
								Node.trimEmptyElement(lexer, list);
								return ;
							}
						}
					}
					
					if (node.tag == tt.tagCenter)
					{
						if (list.content != null)
							Node.insertNodeAfterElement(list, node);
						else
						{
							Node.insertNodeBeforeElement(list, node);
							Node.discardElement(list);
						}
						
						parseTag(lexer, node, mode);
						
						list = lexer.inferredTag("dl");
						Node.insertNodeAfterElement(node, list);
						continue;
					}
					
					if (!(node.tag == tt.tagDt || node.tag == tt.tagDd))
					{
						lexer.ungetToken();
						
						if (!((node.tag.model & (Dict.CM_BLOCK | Dict.CM_INLINE)) != 0))
						{
							Report.warning(lexer, list, node, Report.TAG_NOT_ALLOWED_IN);
							Node.trimEmptyElement(lexer, list);
							return ;
						}
						
						if (!((node.tag.model & Dict.CM_INLINE) != 0) && lexer.excludeBlocks)
						{
							Node.trimEmptyElement(lexer, list);
							return ;
						}
						
						node = lexer.inferredTag("dd");
						Report.warning(lexer, list, node, Report.MISSING_STARTTAG);
					}
					
					if (node.type == Node.EndTag)
					{
						Report.warning(lexer, list, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					Node.insertNodeAtEnd(list, node);
					parseTag(lexer, node, Lexer.IgnoreWhitespace);
				}
				
				Report.warning(lexer, list, node, Report.MISSING_ENDTAG_FOR);
				Node.trimEmptyElement(lexer, list);
			}
		}
		
		public class ParsePre : Parser
		{
			public virtual void  parse(Lexer lexer, Node pre, short mode)
			{
				Node node, parent;
				TagTable tt = lexer.configuration.tt;
				
				if ((pre.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				if ((pre.tag.model & Dict.CM_OBSOLETE) != 0)
					Node.coerceNode(lexer, pre, tt.tagPre);
				
				lexer.inlineDup(null); /* tell lexer to insert inlines if needed */
				
				while (true)
				{
					node = lexer.getToken(Lexer.Preformatted);
					if (node == null)
						break;
					if (node.tag == pre.tag && node.type == Node.EndTag)
					{
						Node.trimSpaces(lexer, pre);
						pre.closed = true;
						Node.trimEmptyElement(lexer, pre);
						return ;
					}
					
					if (node.tag == tt.tagHtml)
					{
						if (node.type == Node.StartTag || node.type == Node.StartEndTag)
							Report.warning(lexer, pre, node, Report.DISCARDING_UNEXPECTED);
						
						continue;
					}
					
					if (node.type == Node.TextNode)
					{
						if (pre.content == null)
						{
							if (node.textarray[node.start] == (byte) '\n')
								++node.start;
							
							if (node.start >= node.end)
							{
								continue;
							}
						}
						
						Node.insertNodeAtEnd(pre, node);
						continue;
					}
					
					if (Node.insertMisc(pre, node))
						continue;
					
					if (node.tag == null || node.tag == tt.tagParam)
					{
						Report.warning(lexer, pre, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag == tt.tagP)
					{
						if (node.type == Node.StartTag)
						{
							Report.warning(lexer, pre, node, Report.USING_BR_INPLACE_OF);
							
							Node.trimSpaces(lexer, pre);
							
							Node.coerceNode(lexer, node, tt.tagBr);
							Node.insertNodeAtEnd(pre, node);
						}
						else
						{
							Report.warning(lexer, pre, node, Report.DISCARDING_UNEXPECTED);
						}
						continue;
					}
					
					if ((node.tag.model & Dict.CM_HEAD) != 0 && !((node.tag.model & Dict.CM_BLOCK) != 0))
					{
						moveToHead(lexer, pre, node);
						continue;
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, pre, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						for (parent = pre.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								Report.warning(lexer, pre, node, Report.MISSING_ENDTAG_BEFORE);
								
								lexer.ungetToken();
								Node.trimSpaces(lexer, pre);
								Node.trimEmptyElement(lexer, pre);
								return ;
							}
						}
					}
					
					if (!((node.tag.model & Dict.CM_INLINE) != 0))
					{
						if (node.type != Node.StartTag)
						{
							Report.warning(lexer, pre, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						Report.warning(lexer, pre, node, Report.MISSING_ENDTAG_BEFORE);
						lexer.excludeBlocks = true;
						
						if ((node.tag.model & Dict.CM_LIST) != 0)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("ul");
							Node.addClass(node, "noindent");
						}
						else if ((node.tag.model & Dict.CM_DEFLIST) != 0)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("dl");
						}
						else if ((node.tag.model & Dict.CM_TABLE) != 0)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("table");
						}
						
						Node.insertNodeAfterElement(pre, node);
						pre = lexer.inferredTag("pre");
						Node.insertNodeAfterElement(node, pre);
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						lexer.excludeBlocks = false;
						continue;
					}
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if (node.tag == tt.tagBr)
							Node.trimSpaces(lexer, pre);
						
						Node.insertNodeAtEnd(pre, node);
						parseTag(lexer, node, Lexer.Preformatted);
						continue;
					}
					
					Report.warning(lexer, pre, node, Report.DISCARDING_UNEXPECTED);
				}
				
				Report.warning(lexer, pre, node, Report.MISSING_ENDTAG_FOR);
				Node.trimEmptyElement(lexer, pre);
			}
		}
		
		public class ParseBlock : Parser
		{
			public virtual void  parse(Lexer lexer, Node element, short mode)
			{
				Node node, parent;
				bool checkstack;
				int istackbase = 0;
				TagTable tt = lexer.configuration.tt;
				
				checkstack = true;
				
				if ((element.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				if (element.tag == tt.tagForm && element.isDescendantOf(tt.tagForm))
					Report.warning(lexer, element, null, Report.ILLEGAL_NESTING);
				
				if ((element.tag.model & Dict.CM_OBJECT) != 0)
				{
					istackbase = lexer.istackbase;
					lexer.istackbase = lexer.istack.Count;
				}
				
				if (!((element.tag.model & Dict.CM_MIXED) != 0))
					lexer.inlineDup(null);
				
				mode = Lexer.IgnoreWhitespace;
				
				while (true)
				{
					node = lexer.getToken(mode);
					if (node == null)
						break;
					if (node.type == Node.EndTag && node.tag != null && (node.tag == element.tag || element.was == node.tag))
					{
						
						if ((element.tag.model & Dict.CM_OBJECT) != 0)
						{
							while (lexer.istack.Count > lexer.istackbase)
								lexer.popInline(null);
							lexer.istackbase = istackbase;
						}
						
						element.closed = true;
						Node.trimSpaces(lexer, element);
						Node.trimEmptyElement(lexer, element);
						return ;
					}
					
					if (node.tag == tt.tagHtml || node.tag == tt.tagHead || node.tag == tt.tagBody)
					{
						if (node.type == Node.StartTag || node.type == Node.StartEndTag)
							Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
						
						continue;
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == null)
						{
							Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
							
							continue;
						}
						else if (node.tag == tt.tagBr)
							node.type = (short)Node.StartTag;
						else if (node.tag == tt.tagP)
						{
							Node.coerceNode(lexer, node, tt.tagBr);
							Node.insertNodeAtEnd(element, node);
							node = lexer.inferredTag("br");
						}
						else
						{
							for (parent = element.parent; parent != null; parent = parent.parent)
							{
								if (node.tag == parent.tag)
								{
									if (!((element.tag.model & Dict.CM_OPT) != 0))
										Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
									
									lexer.ungetToken();
									
									if ((element.tag.model & Dict.CM_OBJECT) != 0)
									{
										/* pop inline stack */
										while (lexer.istack.Count > lexer.istackbase)
											lexer.popInline(null);
										lexer.istackbase = istackbase;
									}
									
									Node.trimSpaces(lexer, element);
									Node.trimEmptyElement(lexer, element);
									return ;
								}
							}
							if (lexer.exiled && node.tag.model != 0 && (node.tag.model & Dict.CM_TABLE) != 0)
							{
								lexer.ungetToken();
								Node.trimSpaces(lexer, element);
								Node.trimEmptyElement(lexer, element);
								return ;
							}
						}
					}
					
					if (node.type == Node.TextNode)
					{
						if (checkstack)
						{
							checkstack = false;
							
							if (!((element.tag.model & Dict.CM_MIXED) != 0))
							{
								if (lexer.inlineDup(node) > 0)
									continue;
							}
						}
						
						Node.insertNodeAtEnd(element, node);
						mode = Lexer.MixedContent;
						continue;
					}
					
					if (Node.insertMisc(element, node))
						continue;
					
					if (node.tag == tt.tagParam)
					{
						if (((element.tag.model & Dict.CM_PARAM) != 0) && (node.type == Node.StartTag || node.type == Node.StartEndTag))
						{
							Node.insertNodeAtEnd(element, node);
							continue;
						}
						
						Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag == tt.tagArea)
					{
						if ((element.tag == tt.tagMap) && (node.type == Node.StartTag || node.type == Node.StartEndTag))
						{
							Node.insertNodeAtEnd(element, node);
							continue;
						}
						
						Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag == null)
					{
						Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (!((node.tag.model & Dict.CM_INLINE) != 0))
					{
						if (node.type != Node.StartTag && node.type != Node.StartEndTag)
						{
							Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (element.tag == tt.tagTd || element.tag == tt.tagTh)
						{
							
							if ((node.tag.model & Dict.CM_HEAD) != 0)
							{
								moveToHead(lexer, element, node);
								continue;
							}
							
							if ((node.tag.model & Dict.CM_LIST) != 0)
							{
								lexer.ungetToken();
								node = lexer.inferredTag("ul");
								Node.addClass(node, "noindent");
								lexer.excludeBlocks = true;
							}
							else if ((node.tag.model & Dict.CM_DEFLIST) != 0)
							{
								lexer.ungetToken();
								node = lexer.inferredTag("dl");
								lexer.excludeBlocks = true;
							}
							
							if (!((node.tag.model & Dict.CM_BLOCK) != 0))
							{
								lexer.ungetToken();
								Node.trimSpaces(lexer, element);
								Node.trimEmptyElement(lexer, element);
								return ;
							}
						}
						else if ((node.tag.model & Dict.CM_BLOCK) != 0)
						{
							if (lexer.excludeBlocks)
							{
								if (!((element.tag.model & Dict.CM_OPT) != 0))
									Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
								
								lexer.ungetToken();
								
								if ((element.tag.model & Dict.CM_OBJECT) != 0)
									lexer.istackbase = istackbase;
								
								Node.trimSpaces(lexer, element);
								Node.trimEmptyElement(lexer, element);
								return ;
							}
						}
						else
						{
							if (!((element.tag.model & Dict.CM_OPT) != 0) && !element.implicit_Renamed)
								Report.warning(lexer, element, node, Report.MISSING_ENDTAG_BEFORE);
							
							if ((node.tag.model & Dict.CM_HEAD) != 0)
							{
								moveToHead(lexer, element, node);
								continue;
							}
							
							lexer.ungetToken();
							
							if ((node.tag.model & Dict.CM_LIST) != 0)
							{
								if (element.parent != null && element.parent.tag != null && element.parent.tag.parser == getParseList())
								{
									Node.trimSpaces(lexer, element);
									Node.trimEmptyElement(lexer, element);
									return ;
								}
								
								node = lexer.inferredTag("ul");
								Node.addClass(node, "noindent");
							}
							else if ((node.tag.model & Dict.CM_DEFLIST) != 0)
							{
								if (element.parent.tag == tt.tagDl)
								{
									Node.trimSpaces(lexer, element);
									Node.trimEmptyElement(lexer, element);
									return ;
								}
								
								node = lexer.inferredTag("dl");
							}
							else if ((node.tag.model & Dict.CM_TABLE) != 0 || (node.tag.model & Dict.CM_ROW) != 0)
							{
								node = lexer.inferredTag("table");
							}
							else if ((element.tag.model & Dict.CM_OBJECT) != 0)
							{
								while (lexer.istack.Count > lexer.istackbase)
									lexer.popInline(null);
								lexer.istackbase = istackbase;
								Node.trimSpaces(lexer, element);
								Node.trimEmptyElement(lexer, element);
								return ;
							}
							else
							{
								Node.trimSpaces(lexer, element);
								Node.trimEmptyElement(lexer, element);
								return ;
							}
						}
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						if ((node.tag.model & Dict.CM_INLINE) != 0)
						{
							if (checkstack && !node.implicit_Renamed)
							{
								checkstack = false;
								
								if (lexer.inlineDup(node) > 0)
									continue;
							}
							
							mode = Lexer.MixedContent;
						}
						else
						{
							checkstack = true;
							mode = Lexer.IgnoreWhitespace;
						}
						
						if (node.tag == tt.tagBr)
							Node.trimSpaces(lexer, element);
						
						Node.insertNodeAtEnd(element, node);
						
						if (node.implicit_Renamed)
							Report.warning(lexer, element, node, Report.INSERTING_TAG);
						
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						continue;
					}
					
					if (node.type == Node.EndTag)
						lexer.popInline(node); /* if inline end tag */
					
					Report.warning(lexer, element, node, Report.DISCARDING_UNEXPECTED);
				}
				
				if (!((element.tag.model & Dict.CM_OPT) != 0))
					Report.warning(lexer, element, node, Report.MISSING_ENDTAG_FOR);
				
				if ((element.tag.model & Dict.CM_OBJECT) != 0)
				{
					while (lexer.istack.Count > lexer.istackbase)
						lexer.popInline(null);
					lexer.istackbase = istackbase;
				}
				
				Node.trimSpaces(lexer, element);
				Node.trimEmptyElement(lexer, element);
			}
		}
		
		public class ParseTableTag : Parser
		{
			public virtual void  parse(Lexer lexer, Node table, short mode)
			{
				Node node, parent;
				int istackbase;
				TagTable tt = lexer.configuration.tt;
				
				lexer.deferDup();
				istackbase = lexer.istackbase;
				lexer.istackbase = lexer.istack.Count;
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == table.tag && node.type == Node.EndTag)
					{
						lexer.istackbase = istackbase;
						table.closed = true;
						Node.trimEmptyElement(lexer, table);
						return ;
					}
					
					if (Node.insertMisc(table, node))
						continue;
					
					if (node.tag == null && node.type != Node.TextNode)
					{
						Report.warning(lexer, table, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.type != Node.EndTag)
					{
						if (node.tag == tt.tagTd || node.tag == tt.tagTh || node.tag == tt.tagTable)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("tr");
							Report.warning(lexer, table, node, Report.MISSING_STARTTAG);
						}
						else if (node.type == Node.TextNode || (node.tag.model & (Dict.CM_BLOCK | Dict.CM_INLINE)) != 0)
						{
							Node.insertNodeBeforeElement(table, node);
							Report.warning(lexer, table, node, Report.TAG_NOT_ALLOWED_IN);
							lexer.exiled = true;
							
							lexer.exiled = false;
							continue;
						}
						else if ((node.tag.model & Dict.CM_HEAD) != 0)
						{
							moveToHead(lexer, table, node);
							continue;
						}
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, table, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (node.tag != null && (node.tag.model & (Dict.CM_TABLE | Dict.CM_ROW)) != 0)
						{
							Report.warning(lexer, table, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						for (parent = table.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								Report.warning(lexer, table, node, Report.MISSING_ENDTAG_BEFORE);
								lexer.ungetToken();
								lexer.istackbase = istackbase;
								Node.trimEmptyElement(lexer, table);
								return ;
							}
						}
					}
					
					if (!((node.tag.model & Dict.CM_TABLE) != 0))
					{
						lexer.ungetToken();
						Report.warning(lexer, table, node, Report.TAG_NOT_ALLOWED_IN);
						lexer.istackbase = istackbase;
						Node.trimEmptyElement(lexer, table);
						return ;
					}
					
					if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					{
						Node.insertNodeAtEnd(table, node); ;
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						continue;
					}
					
					Report.warning(lexer, table, node, Report.DISCARDING_UNEXPECTED);
				}
				
				Report.warning(lexer, table, node, Report.MISSING_ENDTAG_FOR);
				Node.trimEmptyElement(lexer, table);
				lexer.istackbase = istackbase;
			}
		}
		
		public class ParseColGroup : Parser
		{
			public virtual void  parse(Lexer lexer, Node colgroup, short mode)
			{
				Node node, parent;
				TagTable tt = lexer.configuration.tt;
				
				if ((colgroup.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == colgroup.tag && node.type == Node.EndTag)
					{
						colgroup.closed = true;
						return ;
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, colgroup, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						for (parent = colgroup.parent; parent != null; parent = parent.parent)
						{
							
							if (node.tag == parent.tag)
							{
								lexer.ungetToken();
								return ;
							}
						}
					}
					
					if (node.type == Node.TextNode)
					{
						lexer.ungetToken();
						return ;
					}
					
					if (Node.insertMisc(colgroup, node))
						continue;
					
					if (node.tag == null)
					{
						Report.warning(lexer, colgroup, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag != tt.tagCol)
					{
						lexer.ungetToken();
						return ;
					}
					
					if (node.type == Node.EndTag)
					{
						Report.warning(lexer, colgroup, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					Node.insertNodeAtEnd(colgroup, node);
					parseTag(lexer, node, Lexer.IgnoreWhitespace);
				}
			}
		}
		
		public class ParseRowGroup : Parser
		{
			public virtual void  parse(Lexer lexer, Node rowgroup, short mode)
			{
				Node node, parent;
				TagTable tt = lexer.configuration.tt;
				
				if ((rowgroup.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == rowgroup.tag)
					{
						if (node.type == Node.EndTag)
						{
							rowgroup.closed = true;
							Node.trimEmptyElement(lexer, rowgroup);
							return ;
						}
						
						lexer.ungetToken();
						return ;
					}
					
					if (node.tag == tt.tagTable && node.type == Node.EndTag)
					{
						lexer.ungetToken();
						Node.trimEmptyElement(lexer, rowgroup);
						return ;
					}
					
					if (Node.insertMisc(rowgroup, node))
						continue;
					
					if (node.tag == null && node.type != Node.TextNode)
					{
						Report.warning(lexer, rowgroup, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.type != Node.EndTag)
					{
						if (node.tag == tt.tagTd || node.tag == tt.tagTh)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("tr");
							Report.warning(lexer, rowgroup, node, Report.MISSING_STARTTAG);
						}
						else if (node.type == Node.TextNode || (node.tag.model & (Dict.CM_BLOCK | Dict.CM_INLINE)) != 0)
						{
							Node.moveBeforeTable(rowgroup, node, tt);
							Report.warning(lexer, rowgroup, node, Report.TAG_NOT_ALLOWED_IN);
							lexer.exiled = true;
							
							if (node.type != Node.TextNode)
								parseTag(lexer, node, Lexer.IgnoreWhitespace);
							
							lexer.exiled = false;
							continue;
						}
						else if ((node.tag.model & Dict.CM_HEAD) != 0)
						{
							Report.warning(lexer, rowgroup, node, Report.TAG_NOT_ALLOWED_IN);
							moveToHead(lexer, rowgroup, node);
							continue;
						}
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, rowgroup, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (node.tag == tt.tagTr || node.tag == tt.tagTd || node.tag == tt.tagTh)
						{
							Report.warning(lexer, rowgroup, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						for (parent = rowgroup.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								lexer.ungetToken();
								Node.trimEmptyElement(lexer, rowgroup);
								return ;
							}
						}
					}
					
					if ((node.tag.model & Dict.CM_ROWGRP) != 0)
					{
						if (node.type != Node.EndTag)
							lexer.ungetToken();
						
						Node.trimEmptyElement(lexer, rowgroup);
						return ;
					}
					
					if (node.type == Node.EndTag)
					{
						Report.warning(lexer, rowgroup, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (!(node.tag == tt.tagTr))
					{
						node = lexer.inferredTag("tr");
						Report.warning(lexer, rowgroup, node, Report.MISSING_STARTTAG);
						lexer.ungetToken();
					}
					
					Node.insertNodeAtEnd(rowgroup, node);
					parseTag(lexer, node, Lexer.IgnoreWhitespace);
				}
				
				Node.trimEmptyElement(lexer, rowgroup);
			}
		}
		
		public class ParseRow : Parser
		{
			public virtual void  parse(Lexer lexer, Node row, short mode)
			{
				Node node, parent;
				bool exclude_state;
				TagTable tt = lexer.configuration.tt;
				
				if ((row.tag.model & Dict.CM_EMPTY) != 0)
					return ;
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == row.tag)
					{
						if (node.type == Node.EndTag)
						{
							row.closed = true;
							Node.fixEmptyRow(lexer, row);
							return ;
						}
						
						lexer.ungetToken();
						Node.fixEmptyRow(lexer, row);
						return ;
					}
					
					if (node.type == Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.badForm = 1;
							Report.warning(lexer, row, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						if (node.tag == tt.tagTd || node.tag == tt.tagTh)
						{
							Report.warning(lexer, row, node, Report.DISCARDING_UNEXPECTED);
							continue;
						}
						
						for (parent = row.parent; parent != null; parent = parent.parent)
						{
							if (node.tag == parent.tag)
							{
								lexer.ungetToken();
								Node.trimEmptyElement(lexer, row);
								return ;
							}
						}
					}
					
					if (Node.insertMisc(row, node))
						continue;
					
					if (node.tag == null && node.type != Node.TextNode)
					{
						Report.warning(lexer, row, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag == tt.tagTable)
					{
						Report.warning(lexer, row, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.tag != null && (node.tag.model & Dict.CM_ROWGRP) != 0)
					{
						lexer.ungetToken();
						Node.trimEmptyElement(lexer, row);
						return ;
					}
					
					if (node.type == Node.EndTag)
					{
						Report.warning(lexer, row, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (node.type != Node.EndTag)
					{
						if (node.tag == tt.tagForm)
						{
							lexer.ungetToken();
							node = lexer.inferredTag("td");
							Report.warning(lexer, row, node, Report.MISSING_STARTTAG);
						}
						else if (node.type == Node.TextNode || (node.tag.model & (Dict.CM_BLOCK | Dict.CM_INLINE)) != 0)
						{
							Node.moveBeforeTable(row, node, tt);
							Report.warning(lexer, row, node, Report.TAG_NOT_ALLOWED_IN);
							lexer.exiled = true;
							
							if (node.type != Node.TextNode)
								parseTag(lexer, node, Lexer.IgnoreWhitespace);
							
							lexer.exiled = false;
							continue;
						}
						else if ((node.tag.model & Dict.CM_HEAD) != 0)
						{
							Report.warning(lexer, row, node, Report.TAG_NOT_ALLOWED_IN);
							moveToHead(lexer, row, node);
							continue;
						}
					}
					
					if (!(node.tag == tt.tagTd || node.tag == tt.tagTh))
					{
						Report.warning(lexer, row, node, Report.TAG_NOT_ALLOWED_IN);
						continue;
					}
					
					Node.insertNodeAtEnd(row, node);
					exclude_state = lexer.excludeBlocks;
					lexer.excludeBlocks = false;
					ParserImpl.parseTag(lexer, node, Lexer.IgnoreWhitespace);
					lexer.excludeBlocks = exclude_state;
					
					while (lexer.istack.Count > lexer.istackbase)
						lexer.popInline(null);
				}
				
				Node.trimEmptyElement(lexer, row);
			}
		}
		
		public class ParseNoFrames : Parser
		{
			public virtual void  parse(Lexer lexer, Node noframes, short mode)
			{
				Node node;
				TagTable tt = lexer.configuration.tt;
				
				short lexerDotBadAccess = lexer.badAccess;
				lexerDotBadAccess = (short)((ushort)lexerDotBadAccess | (ushort)Report.USING_NOFRAMES);
				lexer.badAccess = lexerDotBadAccess;
				mode = Lexer.IgnoreWhitespace;
				
				while (true)
				{
					node = lexer.getToken(mode);
					if (node == null)
						break;
					if (node.tag == noframes.tag && node.type == Node.EndTag)
					{
						noframes.closed = true;
						Node.trimSpaces(lexer, noframes);
						return ;
					}
					
					if ((node.tag == tt.tagFrame || node.tag == tt.tagFrameset))
					{
						Report.warning(lexer, noframes, node, Report.MISSING_ENDTAG_BEFORE);
						Node.trimSpaces(lexer, noframes);
						lexer.ungetToken();
						return ;
					}
					
					if (node.tag == tt.tagHtml)
					{
						if (node.type == Node.StartTag || node.type == Node.StartEndTag)
							Report.warning(lexer, noframes, node, Report.DISCARDING_UNEXPECTED);
						
						continue;
					}
					
					if (Node.insertMisc(noframes, node))
						continue;
					
					if (node.tag == tt.tagBody && node.type == Node.StartTag)
					{
						Node.insertNodeAtEnd(noframes, node);
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						continue;
					}
					
					if (node.type == Node.TextNode || node.tag != null)
					{
						lexer.ungetToken();
						node = lexer.inferredTag("body");
						Report.warning(lexer, noframes, node, Report.INSERTING_TAG);
						Node.insertNodeAtEnd(noframes, node);
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						continue;
					}
					Report.warning(lexer, noframes, node, Report.DISCARDING_UNEXPECTED);
				}
				
				Report.warning(lexer, noframes, node, Report.MISSING_ENDTAG_FOR);
			}
		}
		
		public class ParseSelect : Parser
		{
			public virtual void  parse(Lexer lexer, Node field, short mode)
			{
				Node node;
				TagTable tt = lexer.configuration.tt;
				
				lexer.insert = - 1; /* defer implicit inline start tags */
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == field.tag && node.type == Node.EndTag)
					{
						field.closed = true;
						Node.trimSpaces(lexer, field);
						return ;
					}
					
					if (Node.insertMisc(field, node))
						continue;
					
					if (node.type == Node.StartTag && (node.tag == tt.tagOption || node.tag == tt.tagOptgroup || node.tag == tt.tagScript))
					{
						Node.insertNodeAtEnd(field, node);
						parseTag(lexer, node, Lexer.IgnoreWhitespace);
						continue;
					}
					
					Report.warning(lexer, field, node, Report.DISCARDING_UNEXPECTED);
				}
				
				Report.warning(lexer, field, node, Report.MISSING_ENDTAG_FOR);
			}
		}
		
		public class ParseText : Parser
		{
			public virtual void  parse(Lexer lexer, Node field, short mode)
			{
				Node node;
				TagTable tt = lexer.configuration.tt;
				
				lexer.insert = - 1; /* defer implicit inline start tags */
				
				if (field.tag == tt.tagTextarea)
					mode = Lexer.Preformatted;
				
				while (true)
				{
					node = lexer.getToken(mode);
					if (node == null)
						break;
					if (node.tag == field.tag && node.type == Node.EndTag)
					{
						field.closed = true;
						Node.trimSpaces(lexer, field);
						return ;
					}
					
					if (Node.insertMisc(field, node))
						continue;
					
					if (node.type == Node.TextNode)
					{
						if (field.content == null && !((mode & Lexer.Preformatted) != 0))
							Node.trimSpaces(lexer, field);
						
						if (node.start >= node.end)
						{
							continue;
						}
						
						Node.insertNodeAtEnd(field, node);
						continue;
					}
					
					if (node.tag == tt.tagFont)
					{
						Report.warning(lexer, field, node, Report.DISCARDING_UNEXPECTED);
						continue;
					}
					
					if (!((field.tag.model & Dict.CM_OPT) != 0))
						Report.warning(lexer, field, node, Report.MISSING_ENDTAG_BEFORE);
					
					lexer.ungetToken();
					Node.trimSpaces(lexer, field);
					return ;
				}
				
				if (!((field.tag.model & Dict.CM_OPT) != 0))
					Report.warning(lexer, field, node, Report.MISSING_ENDTAG_FOR);
			}
		}
		
		public class ParseOptGroup : Parser
		{
			public virtual void  parse(Lexer lexer, Node field, short mode)
			{
				Node node;
				TagTable tt = lexer.configuration.tt;
				
				lexer.insert = - 1; /* defer implicit inline start tags */
				
				while (true)
				{
					node = lexer.getToken(Lexer.IgnoreWhitespace);
					if (node == null)
						break;
					if (node.tag == field.tag && node.type == Node.EndTag)
					{
						field.closed = true;
						Node.trimSpaces(lexer, field);
						return ;
					}
					
					if (Node.insertMisc(field, node))
						continue;
					
					if (node.type == Node.StartTag && (node.tag == tt.tagOption || node.tag == tt.tagOptgroup))
					{
						if (node.tag == tt.tagOptgroup)
							Report.warning(lexer, field, node, Report.CANT_BE_NESTED);
						
						Node.insertNodeAtEnd(field, node);
						parseTag(lexer, node, Lexer.MixedContent);
						continue;
					}
					
					Report.warning(lexer, field, node, Report.DISCARDING_UNEXPECTED);
				}
			}
		}
		
		public static Parser getParseHTML()
		{
			return _parseHTML;
		}
		
		public static Parser getParseHead()
		{
			return _parseHead;
		}
		
		public static Parser getParseTitle()
		{
			return _parseTitle;
		}
		
		public static Parser getParseScript()
		{
			return _parseScript;
		}
		
		public static Parser getParseBody()
		{
			return _parseBody;
		}
		
		public static Parser getParseFrameSet()
		{
			return _parseFrameSet;
		}
		
		public static Parser getParseInline()
		{
			return _parseInline;
		}
		
		public static Parser getParseList()
		{
			return _parseList;
		}
		
		public static Parser getParseDefList()
		{
			return _parseDefList;
		}
		
		public static Parser getParsePre()
		{
			return _parsePre;
		}
		
		public static Parser getParseBlock()
		{
			return _parseBlock;
		}
		
		public static Parser getParseTableTag()
		{
			return _parseTableTag;
		}
		
		public static Parser getParseColGroup()
		{
			return _parseColGroup;
		}
		
		public static Parser getParseRowGroup()
		{
			return _parseRowGroup;
		}
		
		public static Parser getParseRow()
		{
			return _parseRow;
		}
		
		public static Parser getParseNoFrames()
		{
			return _parseNoFrames;
		}
		
		public static Parser getParseSelect()
		{
			return _parseSelect;
		}
		
		public static Parser getParseText()
		{
			return _parseText;
		}
		
		public static Parser getParseOptGroup()
		{
			return _parseOptGroup;
		}
		
		private static Parser _parseHTML;
		private static Parser _parseHead;
		private static Parser _parseTitle;
		private static Parser _parseScript;
		private static Parser _parseBody;
		private static Parser _parseFrameSet;
		private static Parser _parseInline;
		private static Parser _parseList;
		private static Parser _parseDefList;
		private static Parser _parsePre;
		private static Parser _parseBlock;
		private static Parser _parseTableTag;
		private static Parser _parseColGroup;
		private static Parser _parseRowGroup;
		private static Parser _parseRow;
		private static Parser _parseNoFrames;
		private static Parser _parseSelect;
		private static Parser _parseText;
		private static Parser _parseOptGroup;
		
		public static Node parseDocument(Lexer lexer)
		{
			Node node, document, html;
			Node doctype = null;
			TagTable tt = lexer.configuration.tt;
			
			document = lexer.newNode();
			document.type = (short)Node.RootNode;
			
			while (true)
			{
				node = lexer.getToken(Lexer.IgnoreWhitespace);
				if (node == null)
					break;
				
				if (Node.insertMisc(document, node))
					continue;
				
				if (node.type == Node.DocTypeTag)
				{
					if (doctype == null)
					{
						Node.insertNodeAtEnd(document, node);
						doctype = node;
					}
					else
						Report.warning(lexer, document, node, Report.DISCARDING_UNEXPECTED);
					continue;
				}
				
				if (node.type == Node.EndTag)
				{
					Report.warning(lexer, document, node, Report.DISCARDING_UNEXPECTED);
					continue;
				}
				
				if (node.type != Node.StartTag || node.tag != tt.tagHtml)
				{
					lexer.ungetToken();
					html = lexer.inferredTag("html");
				}
				else
					html = node;
				
				Node.insertNodeAtEnd(document, html);
				getParseHTML().parse(lexer, html, (short) 0);
				break;
			}
			
			return document;
		}
		
		public static bool isJavaScript(Node node)
		{
			bool result = false;
			AttVal attr;
			
			if (node.attributes == null)
				return true;
			
			for (attr = node.attributes; attr != null; attr = attr.next)
			{
				if ((Lexer.wstrcasecmp(attr.attribute, "language") == 0 || Lexer.wstrcasecmp(attr.attribute, "type") == 0) && Lexer.wsubstr(attr.value_Renamed, "javascript"))
					result = true;
			}
			
			return result;
		}
		static ParserImpl()
		{
			_parseHTML = new ParseHTML();
			_parseHead = new ParseHead();
			_parseTitle = new ParseTitle();
			_parseScript = new ParseScript();
			_parseBody = new ParseBody();
			_parseFrameSet = new ParseFrameSet();
			_parseInline = new ParseInline();
			_parseList = new ParseList();
			_parseDefList = new ParseDefList();
			_parsePre = new ParsePre();
			_parseBlock = new ParseBlock();
			_parseTableTag = new ParseTableTag();
			_parseColGroup = new ParseColGroup();
			_parseRowGroup = new ParseRowGroup();
			_parseRow = new ParseRow();
			_parseNoFrames = new ParseNoFrames();
			_parseSelect = new ParseSelect();
			_parseText = new ParseText();
			_parseOptGroup = new ParseOptGroup();
		}
	}
}