using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Output Stream</summary>
	public abstract class Out
	{
		public System.IO.MemoryStream outStream;
		
		public abstract void  outc(int c);
		
		public abstract void  newline();
	}
}