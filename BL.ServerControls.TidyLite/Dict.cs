using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Tag dictionary node</summary>
	public class Dict
	{
		public const int CM_UNKNOWN = 0;
		public const int CM_EMPTY = (1 << 0);
		public const int CM_HTML = (1 << 1);
		public const int CM_HEAD = (1 << 2);
		public const int CM_BLOCK = (1 << 3);
		public const int CM_INLINE = (1 << 4);
		public const int CM_LIST = (1 << 5);
		public const int CM_DEFLIST = (1 << 6);
		public const int CM_TABLE = (1 << 7);
		public const int CM_ROWGRP = (1 << 8);
		public const int CM_ROW = (1 << 9);
		public const int CM_FIELD = (1 << 10);
		public const int CM_OBJECT = (1 << 11);
		public const int CM_PARAM = (1 << 12);
		public const int CM_FRAMES = (1 << 13);
		public const int CM_HEADING = (1 << 14);
		public const int CM_OPT = (1 << 15);
		public const int CM_IMG = (1 << 16);
		public const int CM_MIXED = (1 << 17);
		public const int CM_NO_INDENT = (1 << 18);
		public const int CM_OBSOLETE = (1 << 19);
		public const int CM_NEW = (1 << 20);
		public const int CM_OMITST = (1 << 21);
		
		public Dict(string name, int model, Parser parser, CheckAttribs chkattrs)
		{
			this.name = name;
			this.model = model;
			this.parser = parser;
			this.chkattrs = chkattrs;
		}
		
		public string name;
		public int model;
		public Parser parser;
		public CheckAttribs chkattrs;
	}
}