using System;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml;

namespace BL.ServerControls.TidyLite
{
	/// <summary>HTML parser</summary>
	public class Tidy
	{
		#region Properties
		virtual public Configuration Configuration
		{
			get
			{
				return configuration;
			}
		}
		/// <summary> ParseErrors - the number of errors that occurred in the most
		/// recent parse operation
		/// </summary>
		virtual public int ParseErrors
		{
			get
			{
				return parseErrors;
			}
		}
		/// <summary> ParseWarnings - the number of warnings that occurred in the most
		/// recent parse operation
		/// </summary>
		virtual public int ParseWarnings
		{	
			get
			{
				return parseWarnings;
			}
		}
		/// <summary> Errout - the error output stream</summary>
		virtual public System.IO.MemoryStream Errout
		{
			get
			{
				return errout;
			}
			set
			{
				this.errout = value;
			}
		}
		/// <summary> Spaces - default indentation</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#spaces">
		/// </seealso>
		virtual public int Spaces
		{
			get
			{
				return configuration.spaces;
			}
			set
			{
				configuration.spaces = value;
			}
		}
		/// <summary> Tabsize</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#tabsize">
		/// </seealso>
		virtual public int Tabsize
		{
			get
			{
				return configuration.tabsize;
			}
			set
			{
				configuration.tabsize = value;
			}
		}
		/// <summary> ShowWarnings - however errors are always shown</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#ShowWarnings">
		/// </seealso>
		virtual public bool ShowWarnings
		{
			get
			{
				return configuration.ShowWarnings;
			}
			set
			{
				configuration.ShowWarnings = value;
			}
		}
		/// <summary> Quiet - no 'Parsing X', guessed DTD or summary</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#Quiet">
		/// </seealso>
		virtual public bool Quiet
		{
			get
			{
				return configuration.Quiet;
			}
			set
			{
				configuration.Quiet = value;
			}
		}
		/// <summary> IndentContent - indent content of appropriate tags</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#IndentContent">
		/// </seealso>
		virtual public bool IndentContent
		{
			get
			{
				return configuration.IndentContent;
			}
			set
			{
				configuration.IndentContent = value;
			}
		}
		/// <summary> QuoteMarks - output " marks as &amp;quot;</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#QuoteMarks">
		/// </seealso>
		virtual public bool QuoteMarks
		{
			get
			{
				return configuration.QuoteMarks;
			}
			set
			{
				configuration.QuoteMarks = value;
			}
		}
		/// <summary> DropFontTags - discard presentation tags</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#DropFontTags">
		/// </seealso>
		virtual public bool DropFontTags
		{
			get
			{
				return configuration.DropFontTags;
			}
			set
			{
				configuration.DropFontTags = value;
			}
		}
		/// <summary> FixComments - fix comments with adjacent hyphens</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#FixComments">
		/// </seealso>
		virtual public bool FixComments
		{
			get
			{
				return configuration.FixComments;
			}
			set
			{
				configuration.FixComments = value;
			}
		}
		/// <summary> LogicalEmphasis - replace i by em and b by strong</summary>
		/// <seealso cref="org.w3c.tidy.Configuration#LogicalEmphasis">
		/// </seealso>
		virtual public bool LogicalEmphasis
		{
			get
			{
				return configuration.LogicalEmphasis;
			}
			set
			{
				configuration.LogicalEmphasis = value;
			}
		}
		/// <summary> Sets the configuration from a configuration file.</summary>
		virtual public System.String ConfigurationFromFile
		{
			set
			{
				configuration.parseFile(value);
			}
		}
		/// <summary> Sets the configuration from a properties object.</summary>
		virtual public System.Collections.Specialized.NameValueCollection ConfigurationFromProps
		{
			set
			{
				configuration.addProps(value);
			}
		}
		/// <summary>if StripBaseHref then pull out the http://domain from all hrefs (Domain must be set)</summary>
		virtual public bool StripBaseHref
		{
			get
			{
				return configuration.StripBaseHref;
			}
			set
			{
				configuration.StripBaseHref = value;
			}
		}
		/// <summary>This is the domain that will be removed if StripBaseHref</summary>
		virtual public string Domain
		{
			get
			{
				return configuration.Domain;
			}
			set
			{
				configuration.Domain = value;
			}
		}
		/// <summary>if StripRedundantURLBeforeHash then remove the current url from the href (CurrentURL must be set)</summary>
		virtual public bool StripRedundantURLBeforeHash
		{
			get
			{
				return configuration.StripRedundantURLBeforeHash;
			}
			set
			{
				configuration.StripRedundantURLBeforeHash = value;
			}
		}
		/// <summary>This is the url that would be removed from an href with a hash in it</summary>
		virtual public string CurrentURL
		{
			get
			{
				return configuration.CurrentURL;
			}
			set
			{
				configuration.CurrentURL = value;
			}
		}
		/// <summary>This is an XslTransform object. If set, tidy will use this object to transform the output.</summary>
		virtual public XslCompiledTransform XslTransformer
		{
			get
			{
				return configuration.XslTransformer;
			}
			set
			{
				configuration.XslTransformer = value;
			}
		}
		#endregion
		
		private System.IO.MemoryStream errout = null; /* error output stream */
		private Configuration configuration = null;
		private int parseErrors = 0;
		private int parseWarnings = 0;
		
		public Tidy()
		{
			init();
		}
		
		private void init()
		{
			configuration = new Configuration();
			TagTable tt = new TagTable();
			tt.Configuration = configuration;
			configuration.tt = tt;
			errout = new System.IO.MemoryStream();
		}
		
		/// <summary> Parses InputStream in and returns the root Node.
		/// If out is non-null, pretty prints to OutputStream out.
		/// <br/>
		/// Internal routine that actually does the parsing.  The caller
		/// can pass either an InputStream or file name.  If both are passed,
		/// the file name is preferred.
		/// <br/>
		/// This one will just use strings
		/// </summary>
		/// <param name="strIn"></param>
		/// <returns></returns>
		public virtual string parse(string strIn)
		{
			Lexer lexer;
			Node document = null;
			Out o = new OutImpl(); /* normal output stream */
			PPrint pprint;
			
			parseErrors = 0;
			parseWarnings = 0;
			
			if (strIn != null)
			{
				lexer = new Lexer(new StreamInImpl(strIn, configuration.tabsize), configuration);
				lexer.errout = errout;
				
				lexer.in_Renamed.lexer = lexer;
				
				lexer.warnings = 0;
				
				document = ParserImpl.parseDocument(lexer);
					
				Clean cleaner = new Clean(configuration.tt);
					
				cleaner.nestedEmphasis(document);
				cleaner.list2BQ(document);
				cleaner.bQ2Div(document);
					
				if (configuration.LogicalEmphasis)
					cleaner.emFromI(document);
					
				if (cleaner.isWord2000(document, configuration.tt))
				{
					cleaner.dropSections(lexer, document);
						
					cleaner.cleanWord2000(lexer, document);
				}
					
				cleaner.cleanPIs(document); //added by Ernie
					
				if (configuration.DropFontTags)
					cleaner.cleanTree(lexer, document);
					
				if (!document.checkNodeIntegrity())
				{
					Report.badTree(errout);
					return null;
				}
				if (document.content != null)
				{
					lexer.setXHTMLDocType(document);
				}
					
				if (!configuration.Quiet && document.content != null)
				{
					Report.reportNumWarnings(errout, lexer);
				}

				parseWarnings = lexer.warnings;
				parseErrors = lexer.errors;
								
				if (lexer.errors == 0)
				{
					pprint = new PPrint(configuration);
					o.outStream	= new System.IO.MemoryStream();
					pprint.printTree(o, 0, 0, lexer, document);
					pprint.flushLine(o, 0);

					if (configuration.XslTransformer != null) 
					{
						o.outStream = XslTranslate(o.outStream);
					}
				}
				else 
				{
					Report.needsAuthorIntervention(errout);
					Report.errorSummary(lexer);
				}
			}

			try 
			{
				System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding(false);
				char[] utf8Chars = utf8.GetChars(o.outStream.ToArray());
				o.outStream.Close();
				return new string(utf8Chars);
			}
			catch
			{
				return strIn;
			}
		}

		private System.IO.MemoryStream XslTranslate(System.IO.MemoryStream xmlStream)
		{
			try 
			{
				xmlStream.Position = 0;
				XPathDocument xpath = new XPathDocument(xmlStream);
				System.IO.MemoryStream translatedStream = new System.IO.MemoryStream();
				XmlTextWriter xWriter = new XmlTextWriter(translatedStream, new System.Text.UTF8Encoding(false));
				configuration.XslTransformer.Transform(xpath, null, xWriter, null);
				xWriter.Close();
				return translatedStream;
			}
			catch
			{
				return xmlStream;
			}
		}
	}
}