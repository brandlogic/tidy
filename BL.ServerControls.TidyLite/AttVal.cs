using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Attribute/Value linked list node</summary>
	public class AttVal:System.Object
	{
		public AttVal next;
		public Attribute dict;
		public int delim;
		public string attribute;
		public string value_Renamed;
		
		public AttVal()
		{
			this.next = null;
			this.dict = null;
			this.delim = 0;
			this.attribute = null;
			this.value_Renamed = null;
		}
		
		public AttVal(AttVal next, Attribute dict, int delim, string attribute, string value_Renamed)
		{
			this.next = next;
			this.dict = dict;
			this.delim = delim;
			this.attribute = attribute;
			this.value_Renamed = value_Renamed;
		}
		
		protected internal System.Object Clone()
		{
			AttVal av = new AttVal();
			if (next != null)
			{
				av.next = (AttVal) next.MemberwiseClone();
			}
			if ((System.Object) attribute != null)
				av.attribute = attribute;
			if ((System.Object) value_Renamed != null)
				av.value_Renamed = value_Renamed;
			av.delim = delim;
			av.dict = AttributeTable.DefaultAttributeTable.findAttribute(this);
			return av;
		}
		
		/* ignore unknown attributes for proprietary elements */
		public virtual Attribute checkAttribute(Lexer lexer, Node node)
		{
			TagTable tt = lexer.configuration.tt;
			this.checkUniqueAttribute(lexer, node);
			
			Attribute attribute = this.dict;
			if (attribute != null)
			{
				if (attribute.attrchk != null)
					attribute.attrchk.check(lexer, node, this);
			}
			return attribute;
		}
		
		/*
		the same attribute name can't be used
		more than once in each element
		*/
		public virtual void checkUniqueAttribute(Lexer lexer, Node node)
		{
			AttVal attr;
			int count = 0;
			
			for (attr = this.next; attr != null; attr = attr.next)
			{
				if ((System.Object) this.attribute != null && (System.Object) attr.attribute != null && Lexer.wstrcasecmp(this.attribute, attr.attribute) == 0)
					++count;
			}
			
			if (count > 0)
				Report.attrError(lexer, node, this.attribute, Report.REPEATED_ATTRIBUTE);
		}
	}
}