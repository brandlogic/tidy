using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>HTML attribute</summary>
	public class Attribute
	{
		public Attribute(string name, AttrCheck attrchk)
		{
			this.name = name;
			this.attrchk = attrchk;
		}
		
		public string name;
		public AttrCheck attrchk;
	}
}