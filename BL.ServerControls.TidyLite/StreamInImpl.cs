using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Input Stream Implementation</summary>
	public class StreamInImpl:StreamIn
	{
		public StreamInImpl(System.IO.MemoryStream stream, int tabsize)
		{
			this.stream = stream;
			this.pushed = false;
			this.c = (int) '\x0000';
			this.tabs = 0;
			this.tabsize = tabsize;
			this.curline = 1;
			this.curcol = 1;
			this.endOfStream = false;
		}
		
		public StreamInImpl(string strIn, int tabsize)
		{
			this.stream = new System.IO.MemoryStream(SupportClass.ToByteArray(strIn));
			this.pushed = false;
			this.c = (int) '\x0000';
			this.tabs = 0;
			this.tabsize = tabsize;
			this.curline = 1;
			this.curcol = 1;
			this.endOfStream = false;
		}
		
		public override int readCharFromStream()
		{
			int n, c, i, count;
			
			try
			{
				c = this.stream.ReadByte();
				
				if (c == EndOfStream)
				{
					this.endOfStream = true;
					return c;
				}
				/* deal with UTF-8 encoded char */
				
				if ((c & 0xE0) == 0xC0)
					/* 110X XXXX  two bytes */
				{
					n = c & 31;
					count = 1;
				}
				else if ((c & 0xF0) == 0xE0)
					/* 1110 XXXX  three bytes */
				{
					n = c & 15;
					count = 2;
				}
				else if ((c & 0xF8) == 0xF0)
					/* 1111 0XXX  four bytes */
				{
					n = c & 7;
					count = 3;
				}
				else if ((c & 0xFC) == 0xF8)
					/* 1111 10XX  five bytes */
				{
					n = c & 3;
					count = 4;
				}
				else if ((c & 0xFE) == 0xFC)
					/* 1111 110X  six bytes */
				{
					n = c & 1;
					count = 5;
				}
					/* 0XXX XXXX one byte */
				else
					return c;
				
				/* successor bytes should have the form 10XX XXXX */
				for (i = 1; i <= count; ++i)
				{
					c = this.stream.ReadByte();
					
					if (c == EndOfStream)
					{
						this.endOfStream = true;
						return c;
					}
					
					n = (n << 6) | (c & 0x3F);
				}
			}
			catch (System.IO.IOException e)
			{
				System.Console.Error.WriteLine("StreamInImpl.readCharFromStream: " + e.ToString());
				n = EndOfStream;
			}
			
			return n;
		}
		
		public override int readChar()
		{
			int c;
			
			if (this.pushed)
			{
				this.pushed = false;
				c = this.c;
				
				if (c == '\n')
				{
					this.curcol = 1;
					this.curline++;
					return c;
				}
				
				this.curcol++;
				return c;
			}
			
			this.lastcol = this.curcol;
			
			if (this.tabs > 0)
			{
				this.curcol++;
				this.tabs--;
				return ' ';
			}
			
			for (; ; )
			{
				c = readCharFromStream();
				
				if (c < 0)
					return EndOfStream;
				
				if (c == '\n')
				{
					this.curcol = 1;
					this.curline++;
					break;
				}
				
				if (c == '\r')
				{
					c = readCharFromStream();
					if (c != '\n')
					{
						ungetChar(c);
						c = '\n';
					}
					this.curcol = 1;
					this.curline++;
					break;
				}
				
				if (c == '\t')
				{
					this.tabs = this.tabsize - ((this.curcol - 1) % this.tabsize) - 1;
					this.curcol++;
					c = ' ';
					break;
				}
				
				if (c == '\x001B')
					break;
				
				if (0 < c && c < 32)
					continue;
				
				if (127 < c && c < 160)
				{
					continue;
				}
				
				this.curcol++;
				break;
			}
			
			return c;
		}
		
		public override void ungetChar(int c)
		{
			this.pushed = true;
			this.c = c;
			
			if (c == '\n')
			{
				--this.curline;
			}
			
			this.curcol = this.lastcol;
		}
		
		public override bool isEndOfStream()
		{
			return this.endOfStream;
		}
	}
}