using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>HTML attribute hash table</summary>
	public class AttributeTable
	{
		private void InitBlock()
		{
			attributeHashtable = new System.Collections.Hashtable();
		}
		public static AttributeTable DefaultAttributeTable
		{
			get
			{
				if (defaultAttributeTable == null)
				{
					defaultAttributeTable = new AttributeTable();
					for (int i = 0; i < attrs.Length; i++)
					{
						defaultAttributeTable.install(attrs[i]);
					}
					attrHref = defaultAttributeTable.lookup("href");
					attrSrc = defaultAttributeTable.lookup("src");
					attrId = defaultAttributeTable.lookup("id");
					attrName = defaultAttributeTable.lookup("name");
					attrSummary = defaultAttributeTable.lookup("summary");
					attrAlt = defaultAttributeTable.lookup("alt");
					attrLongdesc = defaultAttributeTable.lookup("longdesc");
					attrUsemap = defaultAttributeTable.lookup("usemap");
					attrIsmap = defaultAttributeTable.lookup("ismap");
					attrLanguage = defaultAttributeTable.lookup("language");
					attrType = defaultAttributeTable.lookup("type");
					attrTitle = defaultAttributeTable.lookup("title");
					attrXmlns = defaultAttributeTable.lookup("xmlns");
					attrValue = defaultAttributeTable.lookup("value");
					attrContent = defaultAttributeTable.lookup("content");
					attrDatafld = defaultAttributeTable.lookup("datafld");
					attrWidth = defaultAttributeTable.lookup("width");
					attrHeight = defaultAttributeTable.lookup("height");
				}
				return defaultAttributeTable;
			}
		}
		
		public AttributeTable()
		{
			InitBlock();
		}
		
		public virtual Attribute lookup(string name)
		{
			return (Attribute) attributeHashtable[name];
		}
		
		public virtual Attribute install(Attribute attr)
		{
			return (Attribute) SupportClass.PutElement(attributeHashtable, attr.name, attr);
		}
		
		/* public method for finding attribute definition by name */
		public virtual Attribute findAttribute(AttVal attval)
		{
			Attribute np;
			
			if ((System.Object) attval.attribute != null)
			{
				np = lookup(attval.attribute);
				return np;
			}
			
			return null;
		}
		
		public virtual bool isUrl(string attrname)
		{
			Attribute np;
			
			np = lookup(attrname);
			return (np != null && np.attrchk == AttrCheckImpl.getCheckUrl());
		}
		
		public virtual bool isScript(string attrname)
		{
			Attribute np;
			
			np = lookup(attrname);
			return (np != null);
		}
		
		private System.Collections.Hashtable attributeHashtable;
		private static AttributeTable defaultAttributeTable = null;
		private static Attribute[] attrs;
		
		public static Attribute attrHref = null;
		public static Attribute attrSrc = null;
		public static Attribute attrId = null;
		public static Attribute attrName = null;
		public static Attribute attrSummary = null;
		public static Attribute attrAlt = null;
		public static Attribute attrLongdesc = null;
		public static Attribute attrUsemap = null;
		public static Attribute attrIsmap = null;
		public static Attribute attrLanguage = null;
		public static Attribute attrType = null;
		public static Attribute attrTitle = null;
		public static Attribute attrXmlns = null;
		public static Attribute attrValue = null;
		public static Attribute attrContent = null;
		public static Attribute attrDatafld = null;
		public static Attribute attrWidth = null;
		public static Attribute attrHeight = null;
		static AttributeTable()
		{
			attrs = new Attribute[]{
				new Attribute("abbr", null), 
				new Attribute("accept-charset", null), 
				new Attribute("accept", null), 
				new Attribute("accesskey", null), 
				new Attribute("action", AttrCheckImpl.getCheckUrl()), 
				new Attribute("add_date", null), 
				new Attribute("align", AttrCheckImpl.getCheckAlign()), 
				new Attribute("alink", null), 
				new Attribute("alt", null), 
				new Attribute("archive", null), 
				new Attribute("axis", null), 
				new Attribute("background", AttrCheckImpl.getCheckUrl()), 
				new Attribute("bgcolor", null), 
				new Attribute("bgproperties", null), 
				new Attribute("border", null),
				new Attribute("bordercolor", null), 
				new Attribute("bottommargin", null), 
				new Attribute("cellpadding", null), 
				new Attribute("cellspacing", null), 
				new Attribute("char", null), 
				new Attribute("charoff", null), 
				new Attribute("charset", null), 
				new Attribute("checked", null), 
				new Attribute("cite", AttrCheckImpl.getCheckUrl()), 
				new Attribute("class", null), 
				new Attribute("classid", AttrCheckImpl.getCheckUrl()), 
				new Attribute("clear", null), 
				new Attribute("code", null), 
				new Attribute("codebase", AttrCheckImpl.getCheckUrl()), 
				new Attribute("codetype", null), 
				new Attribute("color", null), 
				new Attribute("cols", null), 
				new Attribute("colspan", null), 
				new Attribute("compact", null), 
				new Attribute("content", null), 
				new Attribute("coords", null), 
				new Attribute("data", AttrCheckImpl.getCheckUrl()), 
				new Attribute("datafld", null), 
				new Attribute("dataformatas", null), 
				new Attribute("datapagesize", null), 
				new Attribute("datasrc", AttrCheckImpl.getCheckUrl()), 
				new Attribute("datetime", null), 
				new Attribute("declare", null), 
				new Attribute("defer", null),
				new Attribute("dir", null), 
				new Attribute("disabled", null), 
				new Attribute("enctype", null), 
				new Attribute("face", null), 
				new Attribute("for", null), 
				new Attribute("frame", null), 
				new Attribute("frameborder", null), 
				new Attribute("framespacing", null), 
				new Attribute("gridx", null), 
				new Attribute("gridy", null), 
				new Attribute("headers", null), 
				new Attribute("height", null), 
				new Attribute("href", AttrCheckImpl.getCheckUrl()), 
				new Attribute("hreflang", null), 
				new Attribute("hspace", null), 
				new Attribute("http-equiv", null), 
				new Attribute("id", null), 
				new Attribute("ismap", null), 
				new Attribute("label", null), 
				new Attribute("lang", null), 
				new Attribute("language", null), 
				new Attribute("last_modified", null), 
				new Attribute("last_visit", null), 
				new Attribute("leftmargin", null), 
				new Attribute("link", null), 
				new Attribute("longdesc", AttrCheckImpl.getCheckUrl()), 
				new Attribute("lowsrc", AttrCheckImpl.getCheckUrl()), 
				new Attribute("marginheight", null), 
				new Attribute("marginwidth", null), 
				new Attribute("maxlength", null), 
				new Attribute("media", null), 
				new Attribute("method", null), 			    
				new Attribute("multiple", null), 
				new Attribute("name", null),
				new Attribute("nohref", null), 
				new Attribute("noresize", null), 
				new Attribute("noshade", null), 
				new Attribute("nowrap", null), 
				new Attribute("object", null), 
				new Attribute("onblur", null), 
				new Attribute("onchange", null), 
				new Attribute("onclick", null), 
				new Attribute("ondblclick", null), 
				new Attribute("onkeydown", null), 
				new Attribute("onkeypress", null), 
				new Attribute("onkeyup", null), 
				new Attribute("onload", null), 
				new Attribute("onmousedown", null), 
				new Attribute("onmousemove", null), 
				new Attribute("onmouseout", null), 
				new Attribute("onmouseover", null), 
				new Attribute("onmouseup", null), 
				new Attribute("onsubmit", null), 
				new Attribute("onreset", null), 
				new Attribute("onselect", null), 
				new Attribute("onunload", null), 
				new Attribute("onafterupdate", null), 
				new Attribute("onbeforeupdate", null), 
				new Attribute("onerrorupdate", null), 
				new Attribute("onrowenter", null), 
				new Attribute("onrowexit", null), 
				new Attribute("onbeforeunload", null), 
				new Attribute("ondatasetchanged", null), 
				new Attribute("ondataavailable", null), 
				new Attribute("ondatasetcomplete", null), 
				new Attribute("profile", AttrCheckImpl.getCheckUrl()), 
				new Attribute("prompt", null), 
				new Attribute("readonly", null), 
				new Attribute("rel", null), 
				new Attribute("rev", null), 
				new Attribute("rightmargin", null), 
				new Attribute("rows", null), 
				new Attribute("rowspan", null), 
				new Attribute("rules", null), 
				new Attribute("scheme", null), 
				new Attribute("scope", null), 
				new Attribute("scrolling", null), 
				new Attribute("selected", null),
				new Attribute("shape", null), 
				new Attribute("showgrid", null),
				new Attribute("showgridx", null),
				new Attribute("showgridy", null),
				new Attribute("size", null), 
				new Attribute("span", null), 
				new Attribute("src", AttrCheckImpl.getCheckUrl()), 
				new Attribute("standby", null), 
				new Attribute("start", null), 
				new Attribute("style", null), 
				new Attribute("summary", null), 
				new Attribute("tabindex", null), 
				new Attribute("target", null), 
				new Attribute("text", null), 
				new Attribute("title", null), 
				new Attribute("topmargin", null), 
				new Attribute("type", null), 
				new Attribute("usemap", null), 
				new Attribute("valign", AttrCheckImpl.getCheckValign()), 
				new Attribute("value", null), 
				new Attribute("valuetype", null), 
				new Attribute("version", null), 
				new Attribute("vlink", null), 
				new Attribute("vspace", null), 
				new Attribute("width", null), 
				new Attribute("wrap", null), 
				new Attribute("xml:lang", null), 
				new Attribute("xmlns", null)
			};
		}
	}
}