using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Linked list of style properties</summary>
	public class StyleProp
	{
		public StyleProp(string name, string value_Renamed, StyleProp next)
		{
			this.name = name;
			this.value_Renamed = value_Renamed;
			this.next = next;
		}
		
		public StyleProp(string name, string value_Renamed):this(name, value_Renamed, null)
		{
		}
		
		public StyleProp():this(null, null, null)
		{
		}
		
		public string name;
		public string value_Renamed;
		public StyleProp next;
	}
}