using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Clean up misuse of presentation markup</summary>
	public class Clean
	{
		private int classNum = 1;
		private TagTable tt;
		
		public Clean(TagTable tt)
		{
			this.tt = tt;
		}
		
		private StyleProp insertProperty(StyleProp props, string name, string value_Renamed)
		{
			StyleProp first, prev, prop;
			int cmp;
			
			prev = null;
			first = props;
			
			while (props != null)
			{
				cmp = props.name.CompareTo(name);
				
				if (cmp == 0)
				{
					return first;
				}
				
				if (cmp > 0)
				{
					prop = new StyleProp(name, value_Renamed, props);
					
					if (prev != null)
						prev.next = prop;
					else
						first = prop;
					
					return first;
				}
				
				prev = props;
				props = props.next;
			}
			
			prop = new StyleProp(name, value_Renamed);
			
			if (prev != null)
				prev.next = prop;
			else
				first = prop;
			
			return first;
		}
		
		private StyleProp createProps(StyleProp prop, string style)
		{
			int name_end;
			int value_end;
			int value_start = 0;
			int name_start = 0;
			bool more;
			
			name_start = 0;
			while (name_start < style.Length)
			{
				while (name_start < style.Length && style[name_start] == ' ')
					++name_start;
				
				name_end = name_start;
				
				while (name_end < style.Length)
				{
					if (style[name_end] == ':')
					{
						value_start = name_end + 1;
						break;
					}
					
					++name_end;
				}
				
				if (name_end >= style.Length || style[name_end] != ':')
					break;
				
				while (value_start < style.Length && style[value_start] == ' ')
					++value_start;
				
				value_end = value_start;
				more = false;
				
				while (value_end < style.Length)
				{
					if (style[value_end] == ';')
					{
						more = true;
						break;
					}
					
					++value_end;
				}
				
				prop = insertProperty(prop, style.Substring(name_start, (name_end) - (name_start)), style.Substring(value_start, (value_end) - (value_start)));
				
				if (more)
				{
					name_start = value_end + 1;
					continue;
				}
				
				break;
			}
			return prop;
		}
		
		private string createPropString(StyleProp props)
		{
			string style = "";
			int len;
			StyleProp prop;
			
			for (len = 0, prop = props; prop != null; prop = prop.next)
			{
				len += prop.name.Length + 2;
				len += prop.value_Renamed.Length + 2;
			}
			
			for (prop = props; prop != null; prop = prop.next)
			{
				style = System.String.Concat(style, prop.name);
				style = System.String.Concat(style, ": ");
				
				style = System.String.Concat(style, prop.value_Renamed);
				
				if (prop.next == null)
					break;
				
				style = System.String.Concat(style, "; ");
			}
			
			return style;
		}
		
		private string addProperty(string style, string property)
		{
			StyleProp prop;
			
			prop = createProps(null, style);
			prop = createProps(prop, property);
			style = createPropString(prop);
			return style;
		}
		
		private string gensymClass(string tag)
		{
			string str;
			
			str = "c" + classNum;
			classNum++;
			return str;
		}
		
		private string findStyle(Lexer lexer, string tag, string properties)
		{
			Style style;
			
			for (style = lexer.styles; style != null; style = style.next)
			{
				if (style.tag.Equals(tag) && style.properties.Equals(properties))
					return style.tagClass;
			}
			
			style = new Style(tag, gensymClass(tag), properties, lexer.styles);
			lexer.styles = style;
			return style.tagClass;
		}
		
		private void style2Rule(Lexer lexer, Node node)
		{
			AttVal styleattr, classattr;
			string classname;
			
			styleattr = node.getAttrByName("style");
			
			if (styleattr != null)
			{
				classname = findStyle(lexer, node.element, styleattr.value_Renamed);
				classattr = node.getAttrByName("class");
				
				if (classattr != null)
				{
					classattr.value_Renamed = classattr.value_Renamed + " " + classname;
					node.removeAttribute(styleattr);
				}
				else
				{
					styleattr.attribute = "class";
					styleattr.value_Renamed = classname;
				}
			}
		}
		
		private void addColorRule(Lexer lexer, string selector, string color)
		{
			if ((System.Object) color != null)
			{
				lexer.addStringLiteral(selector);
				lexer.addStringLiteral(" { color: ");
				lexer.addStringLiteral(color);
				lexer.addStringLiteral(" }\n");
			}
		}
		
		private void cleanBodyAttrs(Lexer lexer, Node body)
		{
			AttVal attr;
			string bgurl = null;
			string bgcolor = null;
			string color = null;
			
			attr = body.getAttrByName("background");
			
			if (attr != null)
			{
				bgurl = attr.value_Renamed;
				attr.value_Renamed = null;
				body.removeAttribute(attr);
			}
			
			attr = body.getAttrByName("bgcolor");
			
			if (attr != null)
			{
				bgcolor = attr.value_Renamed;
				attr.value_Renamed = null;
				body.removeAttribute(attr);
			}
			
			attr = body.getAttrByName("text");
			
			if (attr != null)
			{
				color = attr.value_Renamed;
				attr.value_Renamed = null;
				body.removeAttribute(attr);
			}
			
			if ((System.Object) bgurl != null || (System.Object) bgcolor != null || (System.Object) color != null)
			{
				lexer.addStringLiteral(" body {\n");
				
				if ((System.Object) bgurl != null)
				{
					lexer.addStringLiteral("  background-image: url(");
					lexer.addStringLiteral(bgurl);
					lexer.addStringLiteral(");\n");
				}
				
				if ((System.Object) bgcolor != null)
				{
					lexer.addStringLiteral("  background-color: ");
					lexer.addStringLiteral(bgcolor);
					lexer.addStringLiteral(";\n");
				}
				
				if ((System.Object) color != null)
				{
					lexer.addStringLiteral("  color: ");
					lexer.addStringLiteral(color);
					lexer.addStringLiteral(";\n");
				}
				
				lexer.addStringLiteral(" }\n");
			}
			
			attr = body.getAttrByName("link");
			
			if (attr != null)
			{
				addColorRule(lexer, " :link", attr.value_Renamed);
				body.removeAttribute(attr);
			}
			
			attr = body.getAttrByName("vlink");
			
			if (attr != null)
			{
				addColorRule(lexer, " :visited", attr.value_Renamed);
				body.removeAttribute(attr);
			}
			
			attr = body.getAttrByName("alink");
			
			if (attr != null)
			{
				addColorRule(lexer, " :active", attr.value_Renamed);
				body.removeAttribute(attr);
			}
		}
		
		private bool niceBody(Lexer lexer, Node doc)
		{
			Node body = doc.findBody(lexer.configuration.tt);
			
			if (body != null)
			{
				if (body.getAttrByName("background") != null || body.getAttrByName("bgcolor") != null || body.getAttrByName("text") != null || body.getAttrByName("link") != null || body.getAttrByName("vlink") != null || body.getAttrByName("alink") != null)
				{
					lexer.badLayout = (short)((ushort)lexer.badLayout | (ushort)Report.USING_BODY);
					return false;
				}
			}
			
			return true;
		}
		
		private void createStyleElement(Lexer lexer, Node doc)
		{
			Node node, head, body;
			Style style;
			AttVal av;
			
			if (lexer.styles == null && niceBody(lexer, doc))
				return ;
			
			node = lexer.newNode(Node.StartTag, null, 0, 0, "style");
			node.implicit_Renamed = true;
			
			av = new AttVal(null, null, '"', "type", "text/css");
			av.dict = AttributeTable.DefaultAttributeTable.findAttribute(av);
			node.attributes = av;
			
			body = doc.findBody(lexer.configuration.tt);
			
			lexer.txtstart = lexer.lexsize;
			
			if (body != null)
				cleanBodyAttrs(lexer, body);
			
			for (style = lexer.styles; style != null; style = style.next)
			{
				lexer.addCharToLexer(' ');
				lexer.addStringLiteral(style.tag);
				lexer.addCharToLexer('.');
				lexer.addStringLiteral(style.tagClass);
				lexer.addCharToLexer(' ');
				lexer.addCharToLexer('{');
				lexer.addStringLiteral(style.properties);
				lexer.addCharToLexer('}');
				lexer.addCharToLexer('\n');
			}
			
			lexer.txtend = lexer.lexsize;
			
			Node.insertNodeAtEnd(node, lexer.newNode(Node.TextNode, lexer.lexbuf, lexer.txtstart, lexer.txtend));
			
			head = doc.findHEAD(lexer.configuration.tt);
			
			if (head != null)
				Node.insertNodeAtEnd(head, node);
		}
		
		private void fixNodeLinks(Node node)
		{
			Node child;
			
			if (node.prev != null)
				node.prev.next = node;
			else
				node.parent.content = node;
			
			if (node.next != null)
				node.next.prev = node;
			else
				node.parent.last = node;
			
			for (child = node.content; child != null; child = child.next)
				child.parent = node;
		}
		
		private void stripOnlyChild(Node node)
		{
			Node child;
			
			child = node.content;
			node.content = child.content;
			node.last = child.last;
			child.content = null;
			
			for (child = node.content; child != null; child = child.next)
				child.parent = node;
		}
		
		private void discardContainer(Node element, MutableObject pnode)
		{
			Node node;
			Node parent = element.parent;
			
			if (element.content != null)
			{
				element.last.next = element.next;
				
				if (element.next != null)
				{
					element.next.prev = element.last;
					element.last.next = element.next;
				}
				else
					parent.last = element.last;
				
				if (element.prev != null)
				{
					element.content.prev = element.prev;
					element.prev.next = element.content;
				}
				else
					parent.content = element.content;
				
				for (node = element.content; node != null; node = node.next)
					node.parent = parent;
				
				pnode.Object = element.content;				
			}
			else
			{
				if (element.next != null)
					element.next.prev = element.prev;
				else
					parent.last = element.prev;
				
				if (element.prev != null)
					element.prev.next = element.next;
				else
					parent.content = element.next;
				
				pnode.Object = element.next;
			}
			
			element.next = null;
			element.content = null;
		}
		
		private void addStyleProperty(Node node, System.String property)
		{
			AttVal av;
			
			for (av = node.attributes; av != null; av = av.next)
			{
				if (av.attribute.Equals("style"))
					break;
			}
			
			if (av != null)
			{
				System.String s;
				
				s = addProperty(av.value_Renamed, property);
				av.value_Renamed = s;
			}
			else
			{
				av = new AttVal(node.attributes, null, '"', "style", property);
				av.dict = AttributeTable.DefaultAttributeTable.findAttribute(av);
				node.attributes = av;
			}
		}
		
		private string mergeProperties(string s1, string s2)
		{
			string s;
			StyleProp prop;
			
			prop = createProps(null, s1);
			prop = createProps(prop, s2);
			s = createPropString(prop);
			return s;
		}
		
		private void  mergeStyles(Node node, Node child)
		{
			AttVal av;
			string s1, s2, style;
			
			for (s2 = null, av = child.attributes; av != null; av = av.next)
			{
				if (av.attribute.Equals("style"))
				{
					s2 = av.value_Renamed;
					break;
				}
			}
			
			for (s1 = null, av = node.attributes; av != null; av = av.next)
			{
				if (av.attribute.Equals("style"))
				{
					s1 = av.value_Renamed;
					break;
				}
			}
			
			if ((System.Object) s1 != null)
			{
				if ((System.Object) s2 != null)
				{
					style = mergeProperties(s1, s2);
					av.value_Renamed = style;
				}
			}
			else if ((System.Object) s2 != null)
			{
				av = new AttVal(node.attributes, null, '"', "style", s2);
				av.dict = AttributeTable.DefaultAttributeTable.findAttribute(av);
				node.attributes = av;
			}
		}
		
		private string fontSize2Name(string size)
		{
			string[] sizes = new string[]{"60%", "70%", "80%", null, "120%", "150%", "200%"};
			string buf;
			
			if (size.Length > 0 && '0' <= size[0] && size[0] <= '6')
			{
				int n = size[0] - '0';
				return sizes[n];
			}
			
			if (size.Length > 0 && size[0] == '-')
			{
				if (size.Length > 1 && '0' <= size[1] && size[1] <= '6')
				{
					int n = size[1] - '0';
					double x;
					
					for (x = 1.0; n > 0; --n)
						x *= 0.8;
					
					x *= 100.0;
					buf = "" + (int) x + "%";
					
					return buf;
				}
				
				return "smaller"; /*"70%"; */
			}
			
			if (size.Length > 1 && '0' <= size[1] && size[1] <= '6')
			{
				int n = size[1] - '0';
				double x;
				
				for (x = 1.0; n > 0; --n)
					x *= 1.2;
				
				x *= 100.0;
				buf = "" + (int) x + "%";
				
				return buf;
			}
			
			return "larger"; /* "140%" */
		}
		
		private void addFontFace(Node node, string face)
		{
			addStyleProperty(node, "font-family: " + face);
		}
		
		private void addFontSize(Node node, string size)
		{
			string value_Renamed;
			
			if (size.Equals("6") && node.tag == tt.tagP)
			{
				node.element = "h1";
				tt.findTag(node);
				return ;
			}
			
			if (size.Equals("5") && node.tag == tt.tagP)
			{
				node.element = "h2";
				tt.findTag(node);
				return ;
			}
			
			if (size.Equals("4") && node.tag == tt.tagP)
			{
				node.element = "h3";
				tt.findTag(node);
				return ;
			}
			
			value_Renamed = fontSize2Name(size);
			
			if ((System.Object) value_Renamed != null)
			{
				addStyleProperty(node, "font-size: " + value_Renamed);
			}
		}
		
		private void addFontColor(Node node, string color)
		{
			addStyleProperty(node, "color: " + color);
		}
		
		private void addAlign(Node node, string align)
		{
			addStyleProperty(node, "text-align: " + align.ToLower());
		}
		
		private void addFontStyles(Node node, AttVal av)
		{
			while (av != null)
			{
				if (av.attribute.Equals("face"))
					addFontFace(node, av.value_Renamed);
				else if (av.attribute.Equals("size"))
					addFontSize(node, av.value_Renamed);
				else if (av.attribute.Equals("color"))
					addFontColor(node, av.value_Renamed);
				
				av = av.next;
			}
		}
		
		private void textAlign(Lexer lexer, Node node)
		{
			AttVal av, prev;
			
			prev = null;
			
			for (av = node.attributes; av != null; av = av.next)
			{
				if (av.attribute.Equals("align"))
				{
					if (prev != null)
						prev.next = av.next;
					else
						node.attributes = av.next;
					
					if ((System.Object) av.value_Renamed != null)
					{
						addAlign(node, av.value_Renamed);
					}
					
					break;
				}
				
				prev = av;
			}
		}
		
		private bool dir2Div(Lexer lexer, Node node)
		{
			Node child;
			
			if (node.tag == tt.tagDir || node.tag == tt.tagUl || node.tag == tt.tagOl)
			{
				child = node.content;
				
				if (child == null)
					return false;
				
				/* check child has no peers */
				
				if (child.next != null)
					return false;
				
				if (child.tag != tt.tagLi)
					return false;
				
				if (!child.implicit_Renamed)
					return false;
				
				/* coerce dir to div */
				
				node.tag = tt.tagDiv;
				node.element = "div";
				addStyleProperty(node, "margin-left: 2em");
				stripOnlyChild(node);
				return true;
			}
			
			return false;
		}
		
		private bool center2Div(Lexer lexer, Node node, MutableObject pnode)
		{
			if (node.tag == tt.tagCenter)
			{
				if (lexer.configuration.DropFontTags)
				{
					if (node.content != null)
					{
						Node last = node.last;
						Node parent = node.parent;
						
						discardContainer(node, pnode);
						
						node = lexer.inferredTag("br");
						
						if (last.next != null)
							last.next.prev = node;
						
						node.next = last.next;
						last.next = node;
						node.prev = last;
						
						if (parent.last == last)
							parent.last = node;
						
						node.parent = parent;
					}
					else
					{
						Node prev = node.prev;
						Node next = node.next;
						Node parent = node.parent;
						discardContainer(node, pnode);
						
						node = lexer.inferredTag("br");
						node.next = next;
						node.prev = prev;
						node.parent = parent;
						
						if (next != null)
							next.prev = node;
						else
							parent.last = node;
						
						if (prev != null)
							prev.next = node;
						else
							parent.content = node;
					}
					
					return true;
				}
				node.tag = tt.tagDiv;
				node.element = "div";
				addStyleProperty(node, "text-align: center");
				return true;
			}
			
			return false;
		}
		
		private bool mergeDivs(Lexer lexer, Node node)
		{
			Node child;
			
			if (node.tag != tt.tagDiv)
				return false;
			
			child = node.content;
			
			if (child == null)
				return false;
			
			if (child.tag != tt.tagDiv)
				return false;
			
			if (child.next != null)
				return false;
			
			mergeStyles(node, child);
			stripOnlyChild(node);
			return true;
		}
		
		private bool nestedList(Lexer lexer, Node node)
		{
			Node child, list;
			
			if (node.tag == tt.tagUl || node.tag == tt.tagOl)
			{
				child = node.content;
				
				if (child == null)
					return false;
				
				if (child.next != null)
					return false;
				
				list = child.content;
				
				if (list == null)
					return false;
				
				if (list.tag != node.tag)
					return false;
				
				list.prev = node.prev;
				list.next = node.next;
				list.parent = node.parent;
				fixNodeLinks(list);
				
				child.content = null;
				node.content = null;
				node.next = null;
				
				if (list.prev != null)
				{
					node = list;
					list = node.prev;
					
					if (list.tag == tt.tagUl || list.tag == tt.tagOl)
					{
						list.next = node.next;
						
						if (list.next != null)
							list.next.prev = list;
						
						child = list.last; /* <li> */
						
						node.parent = child;
						node.next = null;
						node.prev = child.last;
						fixNodeLinks(node);
					}
				}
				
				cleanNode(lexer, node);
				return true;
			}
			
			return false;
		}
		
		private bool blockStyle(Lexer lexer, Node node)
		{
			Node child;
			
			if ((node.tag.model & (Dict.CM_BLOCK | Dict.CM_LIST | Dict.CM_DEFLIST | Dict.CM_TABLE)) != 0)
			{
				if (node.tag != tt.tagTable && node.tag != tt.tagTr && node.tag != tt.tagLi)
				{
					if (node.tag != tt.tagCaption)
						textAlign(lexer, node);
					
					child = node.content;
					
					if (child == null)
						return false;
					
					if (child.next != null)
						return false;
					
					if (child.tag == tt.tagB)
					{
						mergeStyles(node, child);
						addStyleProperty(node, "font-weight: bold");
						stripOnlyChild(node);
						return true;
					}
					
					if (child.tag == tt.tagI)
					{
						mergeStyles(node, child);
						addStyleProperty(node, "font-style: italic");
						stripOnlyChild(node);
						return true;
					}
					
					if (child.tag == tt.tagFont)
					{
						mergeStyles(node, child);
						addFontStyles(node, child.attributes);
						stripOnlyChild(node);
						return true;
					}
				}
			}
			
			return false;
		}
		
		private bool inlineStyle(Lexer lexer, Node node)
		{
			Node child;
			
			if (node.tag != tt.tagFont && (node.tag.model & (Dict.CM_INLINE | Dict.CM_ROW)) != 0)
			{
				child = node.content;
				
				if (child == null)
					return false;
				
				if (child.next != null)
					return false;
				
				if (child.tag == tt.tagB && lexer.configuration.LogicalEmphasis)
				{
					mergeStyles(node, child);
					addStyleProperty(node, "font-weight: bold");
					stripOnlyChild(node);
					return true;
				}
				
				if (child.tag == tt.tagI && lexer.configuration.LogicalEmphasis)
				{
					mergeStyles(node, child);
					addStyleProperty(node, "font-style: italic");
					stripOnlyChild(node);
					return true;
				}
				
				if (child.tag == tt.tagFont)
				{
					mergeStyles(node, child);
					addFontStyles(node, child.attributes);
					stripOnlyChild(node);
					return true;
				}
			}
			
			return false;
		}
		
		private bool font2Span(Lexer lexer, Node node, MutableObject pnode)
		{
			AttVal av, style, next;
			
			if (node.tag == tt.tagFont)
			{
				if (lexer.configuration.DropFontTags)
				{
					discardContainer(node, pnode);
					return false;
				}
				
				if (node.parent.content == node && node.next == null)
					return false;
				
				addFontStyles(node, node.attributes);
				
				av = node.attributes;
				style = null;
				
				while (av != null)
				{
					next = av.next;
					
					if (av.attribute.Equals("style"))
					{
						av.next = null;
						style = av;
					}
					
					av = next;
				}
				
				node.attributes = style;
				
				node.tag = tt.tagSpan;
				node.element = "span";
				
				return true;
			}
			
			return false;
		}
		
		private Node cleanNode(Lexer lexer, Node node)
		{
			Node next = null;
			MutableObject o = new MutableObject();
			bool b = false;
			
			for (next = node; node.Element; node = next)
			{
				b = dir2Div(lexer, node);
				if (b)
					continue;
				
				b = nestedList(lexer, node);
				if (b)
					continue;
				
				b = center2Div(lexer, node, o);
				if (b)
					continue;
				
				b = mergeDivs(lexer, node);
				if (b)
					continue;
				
				b = blockStyle(lexer, node);
				if (b)
					continue;
				
				b = inlineStyle(lexer, node);
				if (b)
					continue;
				
				b = font2Span(lexer, node, o);
				if (b)
					continue;
				
				break;
			}
			
			return next;
		}
		
		private Node createStyleProperties(Lexer lexer, Node node)
		{
			Node child;
			
			if (node.content != null)
			{
				for (child = node.content; child != null; child = child.next)
				{
					child = createStyleProperties(lexer, child);
				}
			}

			Node cleanerNode = cleanNode(lexer, node);
			return cleanerNode;
		}
		
		private void defineStyleRules(Lexer lexer, Node node)
		{
			Node child;
			
			if (node.content != null)
			{
				for (child = node.content; child != null; child = child.next)
				{
					defineStyleRules(lexer, child);
				}
			}
			
			style2Rule(lexer, node);
		}
		
		public virtual void cleanTree(Lexer lexer, Node doc)
		{
			doc = createStyleProperties(lexer, doc);
			
			defineStyleRules(lexer, doc);
			createStyleElement(lexer, doc);
		}
		
		public virtual void nestedEmphasis(Node node)
		{
			MutableObject o = new MutableObject();
			Node next;
			
			while (node != null)
			{
				next = node.next;
				
				if ((node.tag == tt.tagB || node.tag == tt.tagI) && node.parent != null && node.parent.tag == node.tag)
				{
					o.Object = next;
					discardContainer(node, o);
					next = (Node)o.Object;
					node = next;
					continue;
				}
				
				if (node.content != null)
					nestedEmphasis(node.content);
				
				node = next;
			}
		}
		
		public virtual void emFromI(Node node)
		{
			while (node != null)
			{
				if (node.tag == tt.tagI)
				{
					node.element = tt.tagEm.name;
					node.tag = tt.tagEm;
				}
				else if (node.tag == tt.tagB)
				{
					node.element = tt.tagStrong.name;
					node.tag = tt.tagStrong;
				}
				
				if (node.content != null)
					emFromI(node.content);
				
				node = node.next;
			}
		}
		
		public virtual void list2BQ(Node node)
		{
			while (node != null)
			{
				if (node.content != null)
					list2BQ(node.content);
				
				if (node.tag != null && node.tag.parser == ParserImpl.getParseList() && node.hasOneChild() && node.content.implicit_Renamed)
				{
					stripOnlyChild(node);
					node.element = tt.tagBlockquote.name;
					node.tag = tt.tagBlockquote;
					node.implicit_Renamed = true;
				}
				
				node = node.next;
			}
		}
		
		public virtual void bQ2Div(Node node)
		{
			int indent;
			System.String indent_buf;
			
			while (node != null)
			{
				if (node.tag == tt.tagBlockquote && node.implicit_Renamed)
				{
					indent = 1;
					
					while (node.hasOneChild() && node.content.tag == tt.tagBlockquote && node.implicit_Renamed)
					{
						++indent;
						stripOnlyChild(node);
					}
					
					if (node.content != null)
						bQ2Div(node.content);
					
					indent_buf = "margin-left: " + (2 * indent).ToString() + "em";
					
					node.element = tt.tagDiv.name;
					node.tag = tt.tagDiv;
					node.addAttribute("style", indent_buf);
				}
				else if (node.content != null)
					bQ2Div(node.content);
				
				
				node = node.next;
			}
		}
		
		public virtual Node pruneSection(Lexer lexer, Node node)
		{
			for (; ; )
			{
				node = Node.discardElement(node);
				
				if (node == null)
					return null;
				
				if (node.type == Node.SectionTag)
				{
					if ((Lexer.getString(node.textarray, node.start, 2)).Equals("if"))
					{
						node = pruneSection(lexer, node);
						continue;
					}
					
					if ((Lexer.getString(node.textarray, node.start, 5)).Equals("endif"))
					{
						node = Node.discardElement(node);
						break;
					}
				}
			}
			
			return node;
		}
		
		public virtual void dropSections(Lexer lexer, Node node)
		{
			while (node != null)
			{
				if (node.type == Node.SectionTag)
				{
					if ((Lexer.getString(node.textarray, node.start, 2)).Equals("if"))
					{
						node = pruneSection(lexer, node);
						continue;
					}
					
					node = Node.discardElement(node);
					continue;
				}
				
				if (node.content != null)
					dropSections(lexer, node.content);
				
				node = node.next;
			}
		}
		
		public virtual void purgeAttributes(Node node)
		{
			AttVal attr = node.attributes;
			AttVal next = null;
			AttVal prev = null;
			
			while (attr != null)
			{
				next = attr.next;
				
				if ((System.Object) attr.attribute != null && (System.Object) attr.value_Renamed != null && attr.attribute.Equals("class") && attr.value_Renamed.Equals("Code"))
				{
					prev = attr;
				}
				else if ((System.Object) attr.attribute != null && (attr.attribute.Equals("class") || attr.attribute.Equals("style") || attr.attribute.Equals("lang") || attr.attribute.StartsWith("x:") || ((attr.attribute.Equals("height") || attr.attribute.Equals("width")) && (node.tag == tt.tagTd || node.tag == tt.tagTr || node.tag == tt.tagTh))))
				{
					if (prev != null)
						prev.next = next;
					else
						node.attributes = next;
				}
				else
					prev = attr;
				
				attr = next;
			}
		}
		
		public virtual Node stripSpan(Lexer lexer, Node span)
		{
			Node node;
			Node prev = null;
			Node content;
			
			cleanWord2000(lexer, span.content);
			content = span.content;
			
			if (span.prev != null)
				prev = span.prev;
			else if (content != null)
			{
				node = content;
				content = content.next;
				Node.removeNode(node);
				Node.insertNodeBeforeElement(span, node);
				prev = node;
			}
			
			while (content != null)
			{
				node = content;
				content = content.next;
				Node.removeNode(node);
				Node.insertNodeAfterElement(prev, node);
				prev = node;
			}
			
			if (span.next == null)
				span.parent.last = prev;
			
			node = span.next;
			span.content = null;
			Node.discardElement(span);
			return node;
		}
		
		private void normalizeSpaces(Lexer lexer, Node node)
		{
			while (node != null)
			{
				if (node.content != null)
					normalizeSpaces(lexer, node.content);
				
				if (node.type == Node.TextNode)
				{
					int i;
					MutableInteger c = new MutableInteger();
					int p = node.start;
					
					for (i = node.start; i < node.end; ++i)
					{
						c.value_Renamed = (int) node.textarray[i];
						
						if (c.value_Renamed > 0x7F)
							i += PPrint.getUTF8(node.textarray, i, c);
						
						if (c.value_Renamed == 160)
							c.value_Renamed = ' ';
						
						p = PPrint.putUTF8(node.textarray, p, c.value_Renamed);
					}
				}
				
				node = node.next;
			}
		}
		
		public virtual void cleanWord2000(Lexer lexer, Node node)
		{
			Node list = null;
			
			while (node != null)
			{
				if (node.tag == tt.tagStyle || node.tag == tt.tagMeta || node.type == Node.CommentTag)
				{
					node = Node.discardElement(node);
					continue;
				}
				
				if (node.tag == tt.tagSpan)
				{
					node = stripSpan(lexer, node);
					continue;
				}
				
				if (node.tag == tt.tagHtml)
				{
					if (node.getAttrByName("xmlns:o") == null)
						return ;
				}
				
				if (node.tag == tt.tagLink)
				{
					AttVal attr = node.getAttrByName("rel");
					
					if (attr != null && (System.Object) attr.value_Renamed != null && attr.value_Renamed.Equals("File-List"))
					{
						node = Node.discardElement(node);
						continue;
					}
				}
				
				if (node.content == null && node.tag == tt.tagP)
				{
					node = Node.discardElement(node);
					continue;
				}
				
				if (node.tag == tt.tagP)
				{
					AttVal attr = node.getAttrByName("class");
					
					if (attr != null && (System.Object) attr.value_Renamed != null && attr.value_Renamed.Equals("MsoListBullet"))
					{
						Node.coerceNode(lexer, node, tt.tagLi);
						
						if (list == null || list.tag != tt.tagUl)
						{
							list = lexer.inferredTag("ul");
							Node.insertNodeBeforeElement(node, list);
						}
						
						purgeAttributes(node);
						
						if (node.content != null)
							cleanWord2000(lexer, node.content);
						
						Node.removeNode(node);
						Node.insertNodeAtEnd(list, node);
						node = list.next;
					}
					else if (attr != null && (System.Object) attr.value_Renamed != null && attr.value_Renamed.Equals("Code"))
					{
						Node br = lexer.newLineNode();
						normalizeSpaces(lexer, node);
						
						if (list == null || list.tag != tt.tagPre)
						{
							list = lexer.inferredTag("pre");
							Node.insertNodeBeforeElement(node, list);
						}
						
						Node.removeNode(node);
						Node.insertNodeAtEnd(list, node);
						stripSpan(lexer, node);
						Node.insertNodeAtEnd(list, br);
						node = list.next;
					}
					else
						list = null;
				}
				else
					list = null;
				
				if (node.type == Node.StartTag || node.type == Node.StartEndTag)
					purgeAttributes(node);
				
				if (node.content != null)
					cleanWord2000(lexer, node.content);
				
				node = node.next;
			}
		}

		/// <summary>
		/// This function discards any xml processing instructions that might be in a node or its children.
		/// </summary>
		/// <param name="node"></param>
		public virtual void cleanPIs(Node node) 
		{
			while (node != null)
			{
				if (node.type == Node.ProcInsTag)
				{
					node = Node.discardElement(node);
					continue;
				}
				if (node.content != null)
					cleanPIs(node.content);

				node = node.next;
			}
		}
		
		public virtual bool isWord2000(Node root, TagTable tt)
		{
			Node html = root.findHTML(tt);
			
			return (html != null && html.getAttrByName("xmlns:o") != null);
		}
	}
}