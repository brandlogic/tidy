using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>HTML ISO entity</summary>
	public class Entity
	{
		public Entity(string name, short code)
		{
			this.name = name;
			this.code = code;
		}
		
		public Entity(string name, int code)
		{
			this.name = name;
			this.code = (short)code;
		}
		
		public string name;
		public short code;
	}
}