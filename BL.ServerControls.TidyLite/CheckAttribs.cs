using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Check HTML attributes</summary>	
	public interface CheckAttribs
	{
		void  check(Lexer lexer, Node node);
	}
}