using System;
namespace BL.ServerControls.TidyLite
{
	/// <summary>Linked list of class names and styles</summary>
	public class Style
	{
		public Style(string tag, string tagClass, string properties, Style next)
		{
			this.tag = tag;
			this.tagClass = tagClass;
			this.properties = properties;
			this.next = next;
		}
		
		public Style(string tag, string tagClass, string properties):this(tag, tagClass, properties, null)
		{
		}
		
		public Style():this(null, null, null, null)
		{
		}
		
		public string tag;
		public string tagClass;
		public string properties;
		public Style next;
	}
}