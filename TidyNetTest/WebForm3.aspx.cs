using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using BL.ServerControls.TidyLite;
//using org.w3c.tidy02;

namespace TidyNetTest
{
	/// <summary>
	/// Summary description for WebForm3.
	/// </summary>
	public class WebForm3 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox TextBoxTidyIn;
		protected System.Web.UI.WebControls.Button ButtonTidy;
		protected System.Web.UI.WebControls.TextBox TextBoxTidyOut;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!Page.IsPostBack) 
			{
				string strIn = "", strOut = "", strErr = "Error";
				//strIn = TextBoxTidyIn.Text;
//				strIn = "hello <b>world</B>";
//				strIn = "<% dim vb as object %>";

				strIn = "\u70BA\u4F60\u5373\u6642\u638C\u63E1\u5929\u4E0B\u4E8B\u0097";
				strIn += "\n<a href=\"http://localhost/TidyNetTest/WebForm3.aspx\">one</a>";
				strIn += "\n<a href=\"/TidyNetTest/WebForm3.aspx\">two</a>";
				strIn += "\n<a href=\"./WebForm3.aspx\">three</a>";
				strIn += "\n<a href=\"../TidyNetTest/WebForm3.aspx\">four</a>";
				strIn += "\n<a href=\"http://localhost/TidyNetTest/WebForm2.aspx\">five</a>";
				strIn += "\n<a href=\"http://localhost/TidyNetTest/WebForm3.aspx?different=true\">six</a>";
				strIn += "\n<a href=\"http://localhost/TidyNetTest/WebForm3.aspx#aname\">seven</a>";
				strIn += "\n<a href=\"/TidyNetTest/WebForm3.aspx#aname\">eight</a>";
				strIn += "\n<a href=\"./WebForm3.aspx#aname\">nine</a>";
				strIn += "\n<a href=\"../TidyNetTest/WebForm3.aspx#aname\">ten</a>";
				strIn += "\n<a href=\"http://localhost/TidyNetTest/WebForm2.aspx#aname\">eleven</a>";
				strIn += "\n<a href=\"http://localhost/TidyNetTest/WebForm3.aspx?different=true#aname\">Twelve</a>";
				strIn += "\n<a href=\"http://localhost/TIDYNetTest/WebForm3.aspx#aname\">Thirteen</a>";
				strIn += "\n<a href=\"http://localhost/TidyNETTest/WebForm3.aspx?different=true#aname\">Fourteen</a>";
				strIn += "\n<a href=\"http://localhost/TidyNetTEST/WebForm3.aspx?different=TRUE#aname\">Fifteen</a>";
				strIn += @"<P class=MsoNormal style=""MARGIN: 0in 0in 0pt""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial"">asdf <SPAN class=head1><STRONG><FONT style=""BACKGROUND-COLOR: #cecece"">airliners fly at 30,000 feet and above. At that altitude, aircraft are flying through the ozone layer that protects us from the sun. To ensure that passengers</FONT></STRONG></SPAN> have fresh, clean air to breathe, air must be drawn in from the outside. Enter ozone, an unhealthy form of oxygen. <BR><BR><BR>The FAA requires that cabin ozone be controlled. The solution is to either remove the ozone or, better yet, convert it into breathable oxygen. <BR><BR><BR>Crew and passenger health � not to mention comfort � all depend on cabin air quality. That's why aircraft manufacturers from around the world come to Engelhard for solutions to the ozone problem.";
				strIn += @"<BR><BR><BR>We apply our catalyst technology and coatings expertise to create catalytic converters that cause harmful ozone molecules to react and recombine into healthy oxygen molecules. Today, major aircraft manufacturers such as Airbus and Boeing use our Deoxo ozone converters in the aircraft they sell. <BR><BR><BR>In the competitive marketplace of commercial air travel, airlines remain profitable by keeping planes in the air and controlling their costs. Major variables that impact cost and uptime are: <?xml:namespace prefix = o ns = ""urn:schemas-microsoft-com:office:office"" /><o:p></o:p></SPAN></P><UL type=disc><LI class=MsoNormal style=""MARGIN: 0in 0in 0pt; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; mso-list: l0 level1 lfo1; tab-stops: list .5in""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial"">Weight <o:p></o:p></SPAN></LI><LI class=MsoNormal style=""MARGIN: 0in 0in 0pt; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; mso-list: l0 level1 lfo1; tab-stops: list .5in""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial"">Size <o:p></o:p></SPAN></LI><LI class=MsoNormal style=""MARGIN: 0in 0in 0pt; mso-margin-top-alt: auto; mso-mar";
				strIn += @"gin-bottom-alt: auto; mso-list: l0 level1 lfo1; tab-stops: list .5in""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial"">Ease of installation <o:p></o:p></SPAN></LI><LI class=MsoNormal style=""MARGIN: 0in 0in 0pt; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; mso-list: l0 level1 lfo1; tab-stops: list .5in""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial"">Ease of maintenance <o:p></o:p></SPAN></LI><LI class=MsoNormal style=""MARGIN: 0in 0in 0pt; mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; mso-list: l0 level1 lfo1; tab-stops: list .5in""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial"">Service life <o:p></o:p></SPAN></LI></UL><P class=MsoNormal style=""MARGIN: 0in 0in 0pt""><SPAN style=""FONT-SIZE: 10.5pt; FONT-FAMILY: Arial""><BR>Engelhard technologies and services help in every way. <BR><BR><BR><BR>In the last 20 years, Engelhard people like Rudy Lechelt have improved and refined our Deoxo offerings. Weight and size have been reduced by two thirds. In-service hours per unit have been extended dramatically. Packaging improvements make replacement quick and easy. <BR><BR><BR>Our refurbishing services can clean a contaminated converter unit so that it can be quickly returned to service. This means that a single converter unit can continue to be used over and over again.</SPAN><SPAN style=""FONT-FAMILY: Arial""><o:p></o:p></SPAN></P>";

//				TextBoxTidyIn.Text = strIn;
				BL.ServerControls.TidyLite.Tidy tidy = new BL.ServerControls.TidyLite.Tidy();
				//org.w3c.tidy02.Tidy tidy = new org.w3c.tidy02.Tidy();
				tidy.Quiet = true;
				tidy.ShowWarnings = false;
//				tidy.Domain = "localhost";
				//tidy.CurrentURL = Request.Url.ToString();
				//tidy.CurrentURL = "http://localhost/TidyNetTest/WebForm3.aspx";
//				tidy.StripBaseHref = true;
//				tidy.StripRedundantURLBeforeHash = true;
				XslCompiledTransform xsl = (XslCompiledTransform)Cache["XslTransformObject"];
				if (xsl == null) 
				{
					xsl = new XslCompiledTransform();
					xsl.Load(Server.MapPath("xDoc.xsl"));
					Cache.Insert("XslTransformObject", xsl, new System.Web.Caching.CacheDependency(Server.MapPath("xDoc.xsl")));
				}

				tidy.XslTransformer = xsl;
				try 
				{
					strErr = "Internal Tidy error";
					strOut = tidy.parse(strIn);					
					//tempRTB.Text = xdoc.Replace("\uFEFF", "");
//					System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding();
//					byte[] bytes = utf8.GetBytes(strOut);
//					strOut = utf8.GetString(bytes);
					char[] chars = strOut.ToCharArray();
//					strOut = strOut.Replace("\uFEFF", "");				
					//xdoc = xdoc.Replace((Char.Parse("\u65279")), Char.MinValue);
					//chars = strOut.ToCharArray();
					//strOut = "";
					Response.Write(strOut);
				}
				catch (Exception tidye)
				{
					Response.Write("<pre>" + strErr + ": \n" + tidye.ToString() + "</pre>");
				}
				TextBoxTidyOut.Text = strOut;
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.ButtonTidy.Click += new System.EventHandler(this.ButtonTidy_Click);
			this.Load += new System.EventHandler(this.Page_Load);
		}
		#endregion

		private void ButtonTidy_Click(object sender, System.EventArgs e)
		{
			string strIn = "", strOut = "", strErr = "Error";
			strIn = TextBoxTidyIn.Text;
			//BL.ServerControls.TidyLite.Tidy tidy = new BL.ServerControls.TidyLite.Tidy();
//			org.w3c.tidy02.Tidy tidy = new org.w3c.tidy02.Tidy();
//			tidy.Quiet = true;
//			tidy.ShowWarnings = false;
			//tidy.Domain = "localhost";
			//tidy.CurrentURL = Request.Url.ToString();
			//tidy.CurrentURL = "http://localhost/TidyNetTest/WebForm3.aspx";
			//tidy.StripBaseHref = true;
			//tidy.StripRedundantURLBeforeHash = true;
			//XslTransform xsl = (XslTransform)Cache["XslTransformObject"];
			//if (xsl == null) 
			//{
				//xsl = new XslTransform();
				//xsl.Load(Server.MapPath("xDoc.xsl"));
				//Cache.Insert("XslTransformObject", xsl, new System.Web.Caching.CacheDependency(Server.MapPath("xDoc.xsl")));
			//}

			//tidy.XslTransformer = xsl;
			try 
			{
				strErr = "Internal Tidy error";
//				strOut = tidy.parse(strIn);
			}
			catch (Exception tidye)
			{
				Response.Write("<pre>" + strErr + ": \n" + tidye.ToString() + "</pre>");
			}
			TextBoxTidyOut.Text = strOut;
		}	
	}
}