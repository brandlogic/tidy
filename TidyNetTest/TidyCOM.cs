/*//
////
//// File:        Tidy.cs
//// Author:      Matthew Stanfield <mattstan@blueyonder.co.uk>
//// Updated:     2003-03-03
////
//// New version designed to run with Charles Reitzel's <creitzel@rcn.com>
//// LibTidyCom http://users.rcn.com/creitzel/tidy.html
////

    C# Tidy Class Copyright (c) 2002 Matthew Stanfield 

  The C# Tidy Class is specifically designed to use with 
  Charles Reitzel's LibTidyCom. This is a COM/ATL wrapper for the 
  HTML Tidy library. See web and email address details above.

  Matthew Stanfield gratefully acknowledges the work gone into
  HTML Tidy by Dave Raggett <dsr@w3.org> and the developers who
  have continued the project since August 2000, especially Charlie 
  Reitzel, developer of LibTidyCom, without whose work this class
  would not exist.

  The copyright of HTML Tidy is with the following organizations:

  Copyright (c) 1998-2001 World Wide Web Consortium
  (Massachusetts Institute of Technology, Institut National de
  Recherche en Informatique et en Automatique, Keio University).
  All Rights Reserved.

  COPYRIGHT NOTICE HTML Tidy and C# Tidy Class:
 
  This source code and documentation is provided "as is," and
  the copyright holders and contributing author(s) make no
  representations or warranties, express or implied, including
  but not limited to, warranties of merchantability or fitness
  for any particular purpose or that the use of the software or
  documentation will not infringe any third party patents,
  copyrights, trademarks or other rights. 

  The copyright holders and contributing author(s) will not be held
  liable for any direct, indirect, special or consequential damages
  arising out of any use of the software or documentation, even if
  advised of the possibility of such damage.

  Permission is hereby granted to use, copy, modify, and distribute
  this source code, or portions hereof, documentation and executables,
  for any purpose, without fee, subject to the following restrictions:

  1. The origin of this source code must not be misrepresented.
  2. Altered versions must be plainly marked as such and must
     not be misrepresented as being the original source.
  3. This Copyright notice may not be removed or altered from any
     source or altered source distribution.
 
  The copyright holders and contributing author(s) specifically
  permit, without fee, and encourage the use of this source code
  as a component for supporting the Hypertext Markup Language in
  commercial products. If you use this source code in a product,
  acknowledgment is not required but would be appreciated.

////
*///


using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

using Tidy;  // Needed to use DotNetTidy.dll, dll must be specified on the compile line too.
using TidyATL;

// Compile line example:
// csc /o+ /out:run.exe /r:DotNetTidy.dll Tidy.cs /doc:Tidy.xml


// Namespace is for the creation of the XMLClasses.dll.
namespace TidyNetTest
{


/// <summary> The Tidy class facilitates the 'tidying' of html files and strings. </summary>
/// <remarks> It uses a dll called Tidy.dll which is part of Charlie Reitzel's LibTidyCom, 
/// see: http://users.rcn.com/creitzel/tidy.html For more details about HTML Tidy 
/// see: http://tidy.sourceforge.net </remarks>
public class TidyCOM
{
    // Class variables.

    /// <summary> Contains the name of the input file. </summary>
    /// <remarks> Either this or the string 'input' must be set depending on
    /// which TidyMode is used. </remarks>
    private string inputFilename;

    /// <summary> Contains the name of the output file. </summary>
    /// <remarks> This will hold the tidied output if one of the XXXToFile TidyModes 
    /// is used. </remarks>
    private string outputFilename;

    /// <summary> Contains the name of the options file. </summary>
    /// <remarks> If the string is not set, the file doesn't exist, is empty or is an 
    /// invalid options file Tidy will use the default settings for ALL options. </remarks>
    private string optionsFilename;

    /// <summary> Contains the name of the error file. </summary>
    /// <remarks> The error file is used in conjunction with the tidy options of show-errors 
    /// and show-warnings. If either of these 2 options are set to show errors or warnings by 
    /// the options file it is highly recommended that errorFilename is set to a valid filename. 
    /// If errors and warnings are set to occur by the options file and errorFilename is left 
    /// blank (IE. "", the empty string) then all errors and warnings will be sent to stdout. 
    /// Note that this is regardless of whether the error-file option is set by the options file. 
    /// The error-file option is always ignored. </remarks>
    private string errorFilename;

    /// <summary> Contains the text of the input. </summary>
    /// <remarks> Either this or the string 'inputFilename' must be set depending on
    /// which TidyMode is used. </remarks>
    private string input;

    /// <summary> Contains the text of the tidied output. </summary>
    /// <remarks> This will hold the tidied output if one of the XXXToString TidyModes 
    /// is used. </remarks>
    public string output;

    /// <summary> Holds the last error to occur. </summary>
    /// <remarks> If any method returns with errors or warnings this will hold the reason why. </remarks>
    public string error;


    /// <summary> This enumeration allows which 'Tidying' type to be specified. </summary>
    /// <remarks> It is used as a paramater when calling HandleTidying which
    /// initiates the tidying process. </remarks>
    public enum TidyMode : int 
    {
        /// <summary> Transformation: input file, output file. </summary>
        FileToFile      = 1, 
        /// <summary> Transformation: input string, output string. </summary>
        StringToString  = 2, 
        /// <summary> Transformation: input file, output string. </summary>
        FileToString    = 3, 
        /// <summary> Transformation: input string, output file. </summary>
        StringToFile    = 4
    }


    /// <summary> This enumeration is for error and warning values. </summary>
    /// <remarks> It facilitates the testing of values returned from the methods in 
    /// the TidyLibCom dll and the actual tidying routines return this type. </remarks>
    public enum TidyStatus : int 
    {
        /// <summary> Status: success. </summary>
        TidySuccess       = 0, 
        /// <summary> Status: warning(s) occurred. </summary>
        TidyWarning       = 1, 
        /// <summary> Status: error(s) occurred. </summary>
        TidyError         = 2, 
        /// <summary> Status: severe error(s) occurred. A severe error will be less 
        /// than 0 so result less than TidySevereError should be used with this. </summary>
        TidySevereError   = -1
    }


    /// <summary> With this constructor no class variables are set. </summary>
    /// <remarks> If this is used then either the SetAllValues method or the set 
    /// value properties must be called, as appropriate, to set essential variables 
    /// before HandleTidying can be called. </remarks>
    public TidyCOM()
    {
        inputFilename = "";
        outputFilename = "";
        optionsFilename = "";
        errorFilename = "";
        input = "";
        output = "";
        error = "";
    }


    /// <summary> This constructor allows all class variables to be set. </summary>
    /// <remarks> Any parameter may be an empty string as long as the values needed
    /// by the DoTidyingXXX methods are set to appropriate values. </remarks>
    public TidyCOM(string InputFilename, string OutputFilename, 
                string OptionsFilename, string ErrorFilename, string Input)
    {
        inputFilename = InputFilename;
        outputFilename = OutputFilename;
        optionsFilename = OptionsFilename;
        errorFilename = ErrorFilename;
        input = Input;
        output = "";
        error = "";
    }


    /// <summary> This method facilitates setting the class variables. </summary>
    /// <remarks> Any parameter may be an empty string as long as the values needed
    /// by the DoTidyingXXX methods are set to appropriate values. </remarks>
    public void SetAllValues(string InputFilename, string OutputFilename, 
                             string OptionsFilename, string ErrorFilename, string Input)
    {
        inputFilename = InputFilename;
        outputFilename = OutputFilename;
        optionsFilename = OptionsFilename;
        errorFilename = ErrorFilename;
        input = Input;
        output = "";
        error = "";
    }


    /// <summary> A property to get/set the inputFilename. </summary>
    public string InputFilenameValue
    {
        get {  return inputFilename;  }
        set {  inputFilename = value; }
    }


    /// <summary> A property to get/set the outputFilename. </summary>
    public string OutputFilenameValue
    {
        get {  return outputFilename;  }
        set {  outputFilename = value; }
    }


    /// <summary> A property to get/set the optionsFilename. </summary>
    public string OptionsFilenameValue
    {
        get {  return optionsFilename;  }
        set {  optionsFilename = value; }
    }


    /// <summary> A property to get/set the errorFilename. </summary>
    public string ErrorFilenameValue
    {
        get {  return errorFilename;  }
        set {  errorFilename = value; }
    }


    /// <summary> A property to get/set the input string. </summary>
    public string InputValue
    {
        get {  return input;  }
        set {  input = value; }
    }


    /// <summary> A property to get the output string. </summary>
    public string OutputValue
    {
        get {  return output;  }
    }


    /// <summary> A property to get the error string. </summary>
    public string ErrorValue
    {
        get {  return error;  }
    }


    /// <summary> Calls the appropriate DoTidyingXXX method. </summary>
    /// <remarks> This is the method that is called to start the tidying. </remarks>
    /// <param name="mode"> Specifies which DoTidyingXXX method to call. </param>
    /// <returns> Returns TidyStatus as to the success of the DoTidyingXXX method. </returns>
    public TidyStatus HandleTidying(TidyMode mode)
    {
        switch (mode)
        {
            case TidyMode.FileToFile:
                return DoTidyingFileToFile();

            case TidyMode.StringToString:
                return DoTidyingStringToString();

            case TidyMode.FileToString:
                return DoTidyingFileToString();

            case TidyMode.StringToFile:
                return DoTidyingStringToFile();

            default:
                error = "Invalid TidyMode.";
                return TidyStatus.TidyError;

        }  // End switch
        // return true; Not needed because all paths through switch return a value
        // Compiler does not need this method to return as all paths return a value.
    }


    /// <summary> A tidying routine. Input is a file, output to a file. </summary>
    /// <remarks> It uses the data held in the file inputFilename as input and outputs 
    /// to the file outputFilename. outputFilename will be overwritten if it already 
    /// exists. It will use the options and error files. </remarks>
    /// <returns> A TidyStatus value. </returns>
    private TidyStatus DoTidyingFileToFile()
    {
        // Check input filename is set
        if (inputFilename.Length < 1)
        {
            error = "Input filename is not set.";
            return TidyStatus.TidyError;
        }

        // Check output filename is set
        if (outputFilename.Length < 1)
        {
            error = "Output filename is not set.";
            return TidyStatus.TidyError;
        }

        // Check input file exists
        FileInfo fileInfo = new FileInfo(inputFilename);
        if (fileInfo.Exists == false)
        {
            error = "Input file does not exist: " + inputFilename;
            return TidyStatus.TidyError;
        }

        // Used to hold return values for many of the following method calls.
//        int returnValue;

        // Create a TidyDocument object, set options file, set error file, 
        // parse, clean and save file.

//        Tidy.Document tidyDocument;
//        try
//        {
//            tidyDocument = new Tidy.Document();
//        }
//        catch (Exception)
//        {
//            error = "Could not create a TidyDocument object.";
//            return TidyStatus.TidyError;
//        }
//
//        if (tidyDocument == null)
//        {
//            error = "Could not create a TidyDocument object.";
//            return TidyStatus.TidyError;
//        }
//
//        returnValue = tidyDocument.LoadConfig(optionsFilename);
//        if (returnValue != (int) TidyStatus.TidySuccess)
//        {
//            error = "Error loading config file: " + optionsFilename;
//            return TidyStatus.TidyError;
//        }
//
//        if (errorFilename.Length > 0)
//        {
//            returnValue = tidyDocument.SetErrorFile(errorFilename);
//            if (returnValue != (int) TidyStatus.TidySuccess)
//            {
//                error = "Error setting error filename: " + errorFilename;
//                return TidyStatus.TidyError;
//            }
//        }
//
//        int parseReturnValue = tidyDocument.ParseFile(inputFilename);
//        if ( (parseReturnValue == (int) TidyStatus.TidyError) || 
//             (parseReturnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error parsing file: " + inputFilename;
//            return TidyStatus.TidyError;
//        }
//
//        int cleanReturnValue = tidyDocument.CleanAndRepair();
//        if ( (cleanReturnValue == (int) TidyStatus.TidyError) || 
//             (cleanReturnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error cleaning or repairing file: " + inputFilename;
//            return TidyStatus.TidyError;
//        }
//
//        // SaveFile() returns 0 on success or 1 on success if there were any Tidy warnings during
//        // the tidy parsing, cleaning and repairing process.
//        returnValue = tidyDocument.SaveFile(outputFilename);
//        if ( (returnValue == (int) TidyStatus.TidyError) || 
//             (returnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error saving output file: " + outputFilename;
//            return TidyStatus.TidyError;
//        }
//
//        // Handle return values
//
//        if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
//             (cleanReturnValue == (int) TidyStatus.TidySuccess))
//        {
//            return TidyStatus.TidySuccess;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
//                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
//        {
//            error = "Parsing and Cleaning issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
//                  (cleanReturnValue == (int) TidyStatus.TidySuccess))
//        {
//            error = "Parsing issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
//                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
//        {
//            error = "Cleaning issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else
//        {
//            // Any other value returns errors but this should never happen.
//            error = "Invalid return.";
            return TidyStatus.TidyError;
//        }
    }


    /// <summary> A tidying routine. Input is a string, output to a string. </summary>
    /// <remarks> The class variable input must hold the input string. When cleaning 
    /// and repairing has been done the class variable output will hold the tidied string. 
    /// It will use the options and error files. </remarks>
    /// <returns> A TidyStatus value. </returns>
    private TidyStatus DoTidyingStringToString()
    {
        // Check input string has something to tidy.
        if (input.Length < 1)
        {
            error = "No input to tidy.";
            return TidyStatus.TidyError;
        }

        // Used to hold return values for many of the following method calls.
        int returnValue;

        // Create a TidyDocument object, set options file, set error file, 
        // parse, clean and get string.
        
        Tidy.DocumentClass tidyDocument;		
        try
        {
            tidyDocument = new Tidy.DocumentClass();
        }
        catch (Exception)
        {
            error = "Could not create a TidyDocument object.";
            return TidyStatus.TidyError;
        }

        if (tidyDocument == null)
        {
            error = "Could not create a TidyDocument object.";
            return TidyStatus.TidyError;
        }


        // LoadConfig() returns 0 for success
        returnValue = tidyDocument.LoadConfig(optionsFilename);
        if (returnValue != (int) TidyStatus.TidySuccess)
        {
            error = "Error loading config file: " + optionsFilename;
            return TidyStatus.TidyError;
        }

        if (errorFilename.Length > 0)
        {
            returnValue = tidyDocument.SetErrorFile(errorFilename);
            if (returnValue != (int) TidyStatus.TidySuccess)
            {
                error = "Error setting error filename: " + errorFilename;
                return TidyStatus.TidyError;
            }
        }

        int parseReturnValue = tidyDocument.ParseString(input);
        if ( (parseReturnValue == (int) TidyStatus.TidyError) || 
             (parseReturnValue <= (int) TidyStatus.TidySevereError))
        {
            error = "Error parsing string.";
            return TidyStatus.TidyError;
        }

        int cleanReturnValue = tidyDocument.CleanAndRepair();
        if ( (cleanReturnValue == (int) TidyStatus.TidyError) || 
             (cleanReturnValue <= (int) TidyStatus.TidySevereError))
        {
            error = "Error cleaning or repairing string.";
            return TidyStatus.TidyError;
        }

        output = tidyDocument.SaveString();
        if (output.Length < 1)
        {
            error = "Output string is empty.";
            return TidyStatus.TidyError;
        }

        // Handle return values

        if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
             (cleanReturnValue == (int) TidyStatus.TidySuccess))
        {
            return TidyStatus.TidySuccess;
        }

        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
        {
            error = "Parsing and Cleaning issued warnings.";
            return TidyStatus.TidyWarning;
        }

        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
                  (cleanReturnValue == (int) TidyStatus.TidySuccess))
        {
            error = "Parsing issued warnings.";
            return TidyStatus.TidyWarning;
        }

        else if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
        {
            error = "Cleaning issued warnings.";
            return TidyStatus.TidyWarning;
        }

        else
        {
            // Any other value returns errors but this should never happen.
            error = "Invalid return.";
            return TidyStatus.TidyError;
        }
    }


    /// <summary> A tidying routine. Input is a file, output to a string. </summary>
    /// <remarks> The file inputFilename holds the input and the output will be held in the 
    /// class variable output when the cleaning and repairing has been done. 
    /// It will use the options and error files. </remarks>
    /// <returns> A TidyStatus value. </returns>
    private TidyStatus DoTidyingFileToString()
    {
        // Check input filename is set
        if (inputFilename.Length < 1)
        {
            error = "Input filename is not set.";
            return TidyStatus.TidyError;
        }

        // Check input file exists
        FileInfo fileInfo = new FileInfo(inputFilename);
        if (fileInfo.Exists == false)
        {
            error = "Input file does not exist: " + inputFilename;
            return TidyStatus.TidyError;
        }

        // Used to hold return values for many of the following method calls.
//        int returnValue;

        // Create a TidyDocument object, set options file, set error file, 
        // parse, clean and save file.
        
//        TidyDocument tidyDocument;
//        try
//        {
//            tidyDocument = new TidyDocument();
//        }
//        catch (Exception)
//        {
//            error = "Could not create a TidyDocument object.";
//            return TidyStatus.TidyError;
//        }
//
//        if (tidyDocument == null)
//        {
//            error = "Could not create a TidyDocument object.";
//            return TidyStatus.TidyError;
//        }
//
//
//        // LoadConfig() returns 0 for success
//        returnValue = tidyDocument.LoadConfig(optionsFilename);
//        if (returnValue != (int) TidyStatus.TidySuccess)
//        {
//            error = "Error loading config file: " + optionsFilename;
//            return TidyStatus.TidyError;
//        }
//
//        if (errorFilename.Length > 0)
//        {
//            returnValue = tidyDocument.SetErrorFile(errorFilename);
//            if (returnValue != (int) TidyStatus.TidySuccess)
//            {
//                error = "Error setting error filename: " + errorFilename;
//                return TidyStatus.TidyError;
//            }
//        }
//
//        int parseReturnValue = tidyDocument.ParseFile(inputFilename);
//        if ( (parseReturnValue == (int) TidyStatus.TidyError) || 
//             (parseReturnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error parsing file: " + inputFilename;
//            return TidyStatus.TidyError;
//        }
//
//        int cleanReturnValue = tidyDocument.CleanAndRepair();
//        if ( (cleanReturnValue == (int) TidyStatus.TidyError) || 
//             (cleanReturnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error cleaning or repairing file: " + inputFilename;
//            return TidyStatus.TidyError;
//        }
//
//        output = tidyDocument.SaveString();
//        if (output.Length < 1)
//        {
//            error = "Output string is empty.";
//            return TidyStatus.TidyError;
//        }
//
//        // Handle return values
//
//        if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
//             (cleanReturnValue == (int) TidyStatus.TidySuccess))
//        {
//            return TidyStatus.TidySuccess;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
//                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
//        {
//            error = "Parsing and Cleaning issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
//                  (cleanReturnValue == (int) TidyStatus.TidySuccess))
//        {
//            error = "Parsing issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
//                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
//        {
//            error = "Cleaning issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else
//        {
//            // Any other value returns errors but this should never happen.
//            error = "Invalid return.";
            return TidyStatus.TidyError;
//        }
    }


    /// <summary> A tidying routine. Input is a string, output to a file. </summary>
    /// <remarks> The class variable input holds the input and the output will be saved
    /// in the file outputFilename when the cleaning and repairing has been done. 
    /// outputFilename will be overwritten if it already exists. It will use the 
    /// options and error files. </remarks>
    /// <returns> A TidyStatus value. </returns>
    private TidyStatus DoTidyingStringToFile()
    {
        // Check input string has something to tidy.
        if (input.Length < 1)
        {
            error = "No input to tidy.";
            return TidyStatus.TidyError;
        }

        // Check output filename is set
        if (outputFilename.Length < 1)
        {
            error = "Output filename is not set.";
            return TidyStatus.TidyError;
        }

        // Used to hold return values for many of the following method calls.
//        int returnValue;

        // Create a TidyDocument object, set options file, set error file, 
        // parse, clean and save file.
        
//        TidyDocument tidyDocument;
//        try
//        {
//            tidyDocument = new TidyDocument();
//        }
//        catch (Exception)
//        {
//            error = "Could not create a TidyDocument object.";
//            return TidyStatus.TidyError;
//        }
//
//        if (tidyDocument == null)
//        {
//            error = "Could not create a TidyDocument object.";
//            return TidyStatus.TidyError;
//        }
//
//
//        // LoadConfig() returns 0 for success
//        returnValue = tidyDocument.LoadConfig(optionsFilename);
//        if (returnValue != (int) TidyStatus.TidySuccess)
//        {
//            error = "Error loading config file: " + optionsFilename;
//            return TidyStatus.TidyError;
//        }
//
//        if (errorFilename.Length > 0)
//        {
//            returnValue = tidyDocument.SetErrorFile(errorFilename);
//            if (returnValue != (int) TidyStatus.TidySuccess)
//            {
//                error = "Error setting error filename: " + errorFilename;
//                return TidyStatus.TidyError;
//            }
//        }
//
//        int parseReturnValue = tidyDocument.ParseString(input);
//        if ( (parseReturnValue == (int) TidyStatus.TidyError) || 
//             (parseReturnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error parsing string.";
//            return TidyStatus.TidyError;
//        }
//
//        int cleanReturnValue = tidyDocument.CleanAndRepair();
//        if ( (cleanReturnValue == (int) TidyStatus.TidyError) || 
//             (cleanReturnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error cleaning or repairing string.";
//            return TidyStatus.TidyError;
//        }
//
//        // SaveFile() returns 0 on success or 1 on success if there were any Tidy warnings during
//        // the tidy parsing, cleaning and repairing process.
//        returnValue = tidyDocument.SaveFile(outputFilename);
//        if ( (returnValue == (int) TidyStatus.TidyError) || 
//             (returnValue <= (int) TidyStatus.TidySevereError))
//        {
//            error = "Error saving output file: " + outputFilename;
//            return TidyStatus.TidyError;
//        }
//
//        // Handle return values
//
//        if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
//             (cleanReturnValue == (int) TidyStatus.TidySuccess))
//        {
//            return TidyStatus.TidySuccess;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
//                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
//        {
//            error = "Parsing and Cleaning issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidyWarning) && 
//                  (cleanReturnValue == (int) TidyStatus.TidySuccess))
//        {
//            error = "Parsing issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else if ( (parseReturnValue == (int) TidyStatus.TidySuccess) && 
//                  (cleanReturnValue == (int) TidyStatus.TidyWarning))
//        {
//            error = "Cleaning issued warnings.";
//            return TidyStatus.TidyWarning;
//        }
//
//        else
//        {
//            // Any other value returns errors but this should never happen.
//            error = "Invalid return.";
            return TidyStatus.TidyError;
//        }
    }


    /// <summary> Loads the text contained in the file into the loadtext 
    /// reference parameter. </summary>
    /// <remarks> My standard load text from file routine. </remarks>
    /// <param name="filename"> File to load text from. </param>
    /// <param name="loadText"> String reference to place the loaded text in. </param>
    /// <returns> Returns bool on sucess/failure. </returns>
    private bool LoadTextFromFile(string filename, ref string loadText)
    {
        loadText = "";

        FileStream fsIn;

        try
        {
            fsIn = new FileStream(filename, FileMode.Open);
        }
        catch (Exception)
        {
            return false;
        }

        try
        {
            if (fsIn.CanRead == false)
            {
                fsIn.Close();
                return false;
            }
        }
        catch (Exception)
        {
            fsIn.Close();
            return false;
        }

        StreamReader sr;

        try
        {
            sr = new StreamReader(fsIn);
        }
        catch (Exception)
        {
            fsIn.Close();
            return false;
        }

        try
        {
            loadText = sr.ReadToEnd();
        }
        catch (Exception)
        {
            sr.Close();
            fsIn.Close();
            return false;
        }

        sr.Close();
        fsIn.Close();

        return true;
    }


    /// <summary> Saves text to a file. </summary>
    /// <remarks> My standard save text to file routine. The file 'filename'
    /// will be overwritten, if it exists, without warning. </remarks>
    /// <param name="filename"> File to save text in. </param>
    /// <param name="saveText"> String of text to save. </param>
    /// <returns> Returns bool on sucess/failure. </returns>
    private bool SaveTextToFile(string filename, string saveText)
    {
        FileStream fsOut;

        try
        {
            fsOut = new FileStream(filename, FileMode.Create);
        }
        catch (Exception)
        {
            return false;
        }

        try
        {
            if (fsOut.CanWrite == false)
            {
                fsOut.Close();
                return false;
            }
        }
        catch (Exception)
        {
            fsOut.Close();
            return false;
        }

        StreamWriter sw;

        try
        {
            sw = new StreamWriter(fsOut);
        }
        catch (Exception)
        {
            fsOut.Close();
            return false;
        }

        try
        {
            sw.Write(saveText);
        }
        catch (Exception)
        {
            sw.Close();
            fsOut.Close();
            return false;
        }

        sw.Close();
        fsOut.Close();

        return true;
    }


	public string ConvertSpecialChars(string body) 
	{
//		string xdoc = body;			
//		for (int x = 0; x < xdoc.Length; x++) 
//		{
//			string s = xdoc.Substring(x, 1);
//			int charInt = (int)char.Parse((xdoc.Substring(x, 1)));
//			if (charInt > 127) 
//			{
//				xdoc = xdoc.Replace(s, "&#" + charInt + ";");
//			}
//		}
//		return xdoc;

		System.Text.UTF8Encoding utf8 = new UTF8Encoding();
		System.Text.ASCIIEncoding ascii = new ASCIIEncoding();
		//byte[] bytes = new byte[body.Length];
		
		byte[] bytes = utf8.GetBytes(body);
		//utf8.GetBytes(body, 0, body.Length, bytes, 0);		

		//char[] ascChars = new char[bytes.Length];
		char[] utf8Chars = utf8.GetChars(bytes);
		char[] ascChars = ascii.GetChars(bytes);
		//utf8.GetChars(bytes, 0, bytes.Length, ascChars, 0);
		//System.Text.Encoding ascii = System.Text.Encoding.ASCII;	
		//ascii.GetChars((byte[])body.ToCharArray(), 0, body.Length, ascChars, 0);
		return new string(ascChars);
	}

	public string RemoveNamespaces(string body)
	{
		string xdoc = body;
		if (xdoc.Length != 0)
		{			
			Regex r = new Regex(@"</?\w*:\w*>|<\?.*?>",RegexOptions.IgnoreCase | RegexOptions.Compiled);
			Match m;

			try 
			{					
				m = r.Match(xdoc);
				if (m.Success)
				{
					xdoc = r.Replace(xdoc,String.Empty);
				}
			}
			catch 
			{
				return body;
			}
		}
		return xdoc;
	}

	public string CleanCode(string body, string xslFilePath)
	{
		string xdoc = body;

		try 
		{
			xdoc = RemoveNamespaces(xdoc);
			//xdoc = ConvertSpecialChars(xdoc);

			//this.InputValue = xdoc.Replace("<xdoc>",String.Empty);
			this.InputValue = xdoc;
			this.HandleTidying(TidyMode.StringToString);
			//xdoc = "<xdoc>" + ConvertSpecialChars(this.OutputValue).Replace("&#65279;", String.Empty) + "</xdoc>";
			//xdoc = "<xdoc>" + this.OutputValue + "</xdoc>";
			//xdoc = CleanWXsl(xdoc, xslFilePath);
			//return xdoc;
			return this.OutputValue;
		}
		catch 
		{
			return body;
		}
	}

	private string CleanWXsl(string body, string xslFilePath)
	{		
		string xdoc = body;

		if (xdoc.Length != 0) 
		{
			XslTransform xsl = new XslTransform();
			xsl.Load(xslFilePath);
			StringBuilder sb = new StringBuilder();
			StringWriter w = new StringWriter(sb);

//			try 
//			{
				XPathDocument xpath = new XPathDocument(new StringReader(xdoc));
				xsl.Transform(xpath, null, w, null);
				xdoc = sb.ToString();
				if (xdoc.Length == 0) 
				{					
					return body;
				}
//			}
//			catch 
//			{
//				return body;
//			}
		}
		return xdoc;
	}

}  // End of Tidy Class

}  // End of XMLClasses namespace.
