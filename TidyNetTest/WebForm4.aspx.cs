using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace TidyNetTest
{
	/// <summary>
	/// Summary description for WebForm4.
	/// </summary>
	public class WebForm4 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox TextBoxTidyIn;
		protected System.Web.UI.WebControls.Button ButtonTidy;
		protected System.Web.UI.WebControls.TextBox TextBoxTidyOut;

		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new System.EventHandler(this.Page_Load);
			this.ButtonTidy.Click += new System.EventHandler(this.ButtonTidy_Click);
		}
		#endregion

		private void ButtonTidy_Click(object sender, System.EventArgs e)
		{
			string strIn = String.Empty, strOut = String.Empty, strErr = "Error";
			strIn = TextBoxTidyIn.Text;
			BL.ServerControls.TidyLite.Tidy tidy = new BL.ServerControls.TidyLite.Tidy();
			tidy.Quiet = true;
			tidy.ShowWarnings = false;
			tidy.Domain = Request.Url.Host;
			tidy.StripBaseHref = true;
			tidy.CurrentURL = Request.Url.ToString();
			tidy.StripRedundantURLBeforeHash = true;
			//			org.w3c.tidy02.Tidy tidy = new org.w3c.tidy02.Tidy();
			//			tidy.Quiet = true;
			//			tidy.ShowWarnings = false;
			//tidy.Domain = "localhost";
			//tidy.CurrentURL = Request.Url.ToString();
			//tidy.CurrentURL = "http://localhost/TidyNetTest/WebForm3.aspx";
			//tidy.StripBaseHref = true;
			//tidy.StripRedundantURLBeforeHash = true;

			XslCompiledTransform xsl = (XslCompiledTransform)Cache["XslTransformObject"];
			if (xsl == null) 
			{
				xsl = new XslCompiledTransform();
				xsl.Load(Server.MapPath("xDoc.xsl"));
				Cache.Insert("XslTransformObject", xsl, new System.Web.Caching.CacheDependency(Server.MapPath("xDoc.xsl")));
			}
			tidy.XslTransformer = xsl;

			try 
			{
				strErr = "Internal Tidy error";
				strOut = tidy.parse(strIn);
			}
			catch (Exception tidye)
			{
				Response.Write("<pre>" + strErr + ": \n" + tidye.ToString() + "</pre>");
			}
			TextBoxTidyOut.Text = strOut;
		}	
	}
}
