<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:be="http://www.brandensemble.com" version="1.0" exclude-result-prefixes="be">
	<!--<xsl:output method="xml" media-type="text/xml" omit-xml-declaration="yes" encoding="UTF-8"/>-->
	<xsl:output method="xml" />
	<!--<xsl:output method="xml" indent="yes" encoding="utf-8" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd" omit-xml-declaration="no"/>-->
	<xsl:template match="xdoc">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="div">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="html">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="body">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="style">
		<style type="text/css">
			<xsl:apply-templates/>
		</style>
	</xsl:template>
	<xsl:template match="iframe">
		<xsl:copy>
			<xsl:copy-of select="@*|b/@*" />
			<xsl:value-of select="." disable-output-escaping="yes" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="script[not(@src)]">
		<xsl:copy>
			<xsl:value-of select="." disable-output-escaping="yes" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="title"></xsl:template>
	<xsl:template match="head">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="u">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="img[not(@src)]|Image[not(@src)]"></xsl:template>
	<xsl:template match="pre"></xsl:template>
	<!--<xsl:template match="form"></xsl:template>-->
	<!--<xsl:template match="span[not(@class) and not(@id)]"><xsl:apply-templates/></xsl:template>-->
	<xsl:template match="span[not(@class)]">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="ul[not(@class)]">
		<ul>
			<xsl:apply-templates/>
		</ul>
	</xsl:template>
	<xsl:template match="ul[@class]">
		<ul>
			<xsl:attribute name="class">
				<xsl:value-of select="@class"/>
			</xsl:attribute>
			<xsl:apply-templates/>
		</ul>
	</xsl:template>
	<xsl:template match="br">
		<br/>
		<xsl:apply-templates/>
	</xsl:template>
	<!--<xsl:template match="p"><p><xsl:apply-templates/></p></xsl:template>-->
	<xsl:template match="b">
		<strong>
			<xsl:apply-templates/>
		</strong>
	</xsl:template>
	<xsl:template match="i">
		<em>
			<xsl:apply-templates/>
		</em>
	</xsl:template>

	<!--<xsl:template match="em"><i><xsl:apply-templates/></i></xsl:template>
	<xsl:template match="strong"><b><xsl:apply-templates/></b></xsl:template>-->

	<xsl:template match="font[not(@class)]">
		<xsl:apply-templates/>
	</xsl:template>
	<xsl:template match="font[@class]">
		<span>
			<xsl:attribute name="class">
				<xsl:value-of select="@class"/>
			</xsl:attribute>
			<xsl:apply-templates/>
		</span>
	</xsl:template>

	<xsl:template match="a[@name]">
		<a>
			<xsl:attribute name="id">
				<xsl:value-of select="@name"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="@name"/>
			</xsl:attribute>
		</a>
	</xsl:template>

	<!-- this is to allow flash movies -->
	<!-- 
	<xsl:template match="object">
		<object>
			<xsl:if test="@classid">
				<xsl:attribute name="classid">
					<xsl:value-of select="@classid"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@codebase">
				<xsl:attribute name="codebase">
					<xsl:value-of select="@codebase"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@width">
				<xsl:attribute name="width">
					<xsl:value-of select="@width"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@height">
				<xsl:attribute name="height">
					<xsl:value-of select="@height"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@id">
				<xsl:attribute name="id">
					<xsl:value-of select="@id"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@align">
				<xsl:attribute name="align">
					<xsl:value-of select="@align"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:value-of select="."/>
			<xsl:apply-templates/>
		</object>
	</xsl:template>
	<xsl:template match="param">
		<param>
			<xsl:if test="@name">
				<xsl:attribute name="name">
					<xsl:value-of select="@name"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@value">
				<xsl:attribute name="value">
					<xsl:value-of select="@value"/>
				</xsl:attribute>
			</xsl:if>
		</param>
	</xsl:template>
	 -->

	<!--<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates />
		</xsl:copy>
	</xsl:template>-->

	<xsl:template match="*">
		<xsl:element name="{name(.)}" namespace="{namespace-uri(.)}">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates/>
		</xsl:element>
	</xsl:template>

	<xsl:template name="vReplace">
		<xsl:param name="text"/>
		<xsl:param name="replace"/>
		<xsl:param name="by"/>
		<xsl:choose>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text, $replace)"/>
				<xsl:value-of select="$by"/>
				<xsl:call-template name="vReplace">
					<xsl:with-param name="text" select="substring-after($text, $replace)"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>